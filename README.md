# On finding optimal dynamic arborescences

Given a directed weighted and connected graph G, we address the problem of finding an optimal arborescence on G. Although there are known algorithms for this problem, with the one proposed by Edmonds being the most well known, there are several proposed algorithmic ideas around this problem that have not been used in practical implementations. In this paper we gather and explore those ideas, providing practical implementations and experimental results. We address also the problem of maintaining an optimal arborescence on a dynamic graph G.


# Evaluations


The **release** folder contains the jars used for testing this work.
In the following sections we will specify each type of test and how they can be performed.  
The tests ran on real graphs and randomly generated homogenous graphs. The former were generated using phylogenetic datasets from [EnteroBase](https://enterobase.warwick.ac.uk/), which details are shown in fowlling Table. The latter were produced by creating a defined number of nodes and adding the same number of random edges of random weight (between 0 and 100) incident to each node, i.e. each node has the same degree. As the tests runs on strongly connected graphs, an additional dummy node is added with edges of infinite weight (maximum integer) from (resp. incident to) this node to (resp. from) each other node. The graphs were made homogenous to quickly find an average computation time.


| Data sets                  |\|V\| | \|E\|    
|----------------------------|------|----------|
| clostridium.Griffiths      | 440  | 193600   |
| Moraxella.Achtman7GeneMLST | 773  | 597529   |
| Salmonella.Achtman7GeneMLST| 5464 | 29855296 |
| Yersinia.McNally           | 369  | 136161   | 

All the datasets used in the tests can be found in the folder: [`data/phylogenetic_dataset`](https://gitlab.com/ana.correia/on-finding-optimal-dynamic-arborescences/-/tree/main/data/phylogenetic_dataset)

**ATTENTION** when running the tests(.jar) please enter the datasets folder([`data/phylogenetic_dataset`](https://gitlab.com/ana.correia/on-finding-optimal-dynamic-arborescences/-/tree/main/data/phylogenetic_dataset)) in the same path where the **jar** you are running is located. 



## Prim algorithm vs Edmonds algorithm

In this test, we intend to compare algorithm *Prim* with algorithm *Edmonds* using **real** and **random** data.

1. For real data  
`java -jar EdmondsEvalutation.jar primVSedmondsReal`

2. For random data  
`java -jar EdmondsEvalutation.jar primVSedmondsRand`

## Memory computation

In this test, we intent measure the memory usage of both the *static* and *dynamic* algorithm.
The data that use in this test are real data shown in the previous table.

1. For static algorithm - runs all real datasets  
`java -jar MemoryEval.jar memoryStaticUpdate`  


    1. For static algorithm - for clostridium.Griffiths dataset  
    `java -jar MemoryEval.jar memoryStaticUpdate c`
    1. For static algorithm - for Moraxella.Achtman7GeneMLST dataset  
    `java -jar MemoryEval.jar memoryStaticUpdate m`
    1. For static algorithm - for Salmonella.Achtman7GeneMLST dataset  
    `java -jar MemoryEval.jar memoryStaticUpdate s`
    1. For static algorithm - for Yersinia.McNally dataset  
    `java -jar MemoryEval.jar memoryStaticUpdate y`

2. For dynamic algorithm - runs all real datasets  
`java -jar MemoryEval.jar memoryDynamicUpdate`  

    1. For static algorithm - for clostridium.Griffiths dataset  
    `java -jar MemoryEval.jar memoryDynamicUpdate c`
    1. For static algorithm - for Moraxella.Achtman7GeneMLST dataset  
    `java -jar MemoryEval.jar memoryDynamicUpdate m`
    1. For static algorithm - for Salmonella.Achtman7GeneMLST dataset  
    `java -jar MemoryEval.jar memoryDynamicUpdate s`
    1. For static algorithm - for Yersinia.McNally dataset  
    `java -jar MemoryEval.jar memoryDynamicUpdate y`


### Dynamic arborescence vs static Edmonds' algorithm

In this test, we intent compare our dynamic optimum arborescence to our static implementation of Edmonds' algorithm.  
For this test we use the more complex operation: the *UPDATE* operation as it is composed of a *DELETE* and a *ADD* operation.

1. runs all random datasets  
`java -jar DynamicEval.jar rand`  

2. runs all real datasets  
`java -jar DynamicEval.jar real`  

    1. For only clostridium.Griffiths dataset  
    `java -jar DynamicEval.jar real c`
    1. For only Moraxella.Achtman7GeneMLST dataset  
    `java -jar DynamicEval.jar real m`
    1. For only Salmonella.Achtman7GeneMLST dataset  
    `java -jar DynamicEval.jar real s`
    1. For only Yersinia.McNally dataset  
    `java -jar DynamicEval.jar real y`

