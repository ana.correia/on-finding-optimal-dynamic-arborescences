package net.phyloviz.io;

public class Directory {
    public static String SCALE_FREE = "directed_scale_free/";
    public static String DIRECTED_GRAPH_DIR = "directed/";
    public static String CONNECTED_GRAPH_DIR = "strongly_connected_graph/";
    public static String DIRECTED_COMPLETE = "directed_complete/";
    public static String SMALL_DIRECTED_COMPLETE = "small_directed_complete/";
    public static String CONNECTED_GRAPH_ARBO = "strongly_connected_graph_sol/";
    public static String INNEFICIENT_CONNECTED_ARBO = "strongly_connected_inefficient/";
    public static String EFFICIENT_CONNECTED_ARBO = "strongly_connected_efficient/";
    public static String SPARSE_GRAPH = "sparse_graphs/";
    public static String SPARSE_ARBO = "sparse_arbo/";
    public static String EFF_SPARSE_SOL = "eff_sparse_sol/";
    public static String UNDIRECTED_GRAPH = "undirected/";
    public static String UNDIRECTED_COMPLETE = "undirected_complete/";
    public static String PRIM_EVAL = "prim_eval/";
    public static String EDMONDS_EVAL = "edmonds_eval/";
    public static String DYNAMIC_EVAL = "dynamic_eval/";
    public static String PHYLOGENETIC_DATASET = "phylogenetic_dataset/";
    public static String PRIM_EVAL_RAND = "prim_eval_rand/";
    public static String EDMONDS_COMP_RAND = "edmonds_compare_rand/";
}
