package net.phyloviz.io;

import net.phyloviz.graph.*;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class IO {

    /**
     * Reads a file and builds a graph
     *
     * @param fileName - file containing all edges
     * @return graph
     * @throws IOException
     */
    public static DirectedGraph readDirectedGraph(String fileName) throws IOException {

        FileInputStream fStream = new FileInputStream(fileName);
        BufferedReader br = new BufferedReader(new InputStreamReader(fStream));
        String strLine;

        int i = 0;
        DirectedGraph graph = null;

        while ((strLine = br.readLine()) != null) {

            if (i == 0) {

                String[] strings = strLine.split(" ");
                int size = Integer.parseInt(strings[0]);
                graph = new DirectedGraph(size);
                i++;

            } else {

                readGraphUtil(strLine, graph);
            }
        }

        fStream.close();
        return graph;
    }

    /**
     * Reads a file and builds a graph
     *
     * @param fileName - file containing all edges
     * @return graph
     * @throws IOException
     */
    public static DirectedGraph readDirectedGraph2(String fileName) throws IOException {

        FileInputStream fStream = new FileInputStream(fileName);
        BufferedReader br = new BufferedReader(new InputStreamReader(fStream));
        String strLine;
        DirectedGraph graph = new DirectedGraph();

        while ((strLine = br.readLine()) != null) {

            readGraphUtil(strLine, graph);
        }

        fStream.close();
        return graph;
    }

    /**
     * Reads a file with an arboresncece
     * @param fileName - file name
     * @return List of edges
     * @throws IOException
     */
    public static List<WeightedEdge> readSol(String fileName) throws IOException {

        FileInputStream fStream = new FileInputStream(fileName);
        BufferedReader br = new BufferedReader(new InputStreamReader(fStream));
        String strLine;

        List<WeightedEdge> edges = new LinkedList<>();
        DirectedGraph graph = new DirectedGraph();

        while ((strLine = br.readLine()) != null) {

            WeightedEdge edge = readGraphUtil(strLine, graph);
            edges.add(edge);
        }

        fStream.close();
        return edges;
    }


    /**
     * Reads a file and builds a graph
     *
     * @param fileName - file containing all edges
     * @return graph
     * @throws IOException
     */
    public static UndirectedGraph readUndirectedGraph(String fileName) throws IOException {

        FileInputStream fStream = new FileInputStream(fileName);
        BufferedReader br = new BufferedReader(new InputStreamReader(fStream));
        String strLine;

        int i = 0;
        UndirectedGraph graph = null;

        while ((strLine = br.readLine()) != null) {

            if (i == 0) {
                String[] strArr = strLine.split(" ");

                int size = Integer.parseInt(strArr[0]);
                graph = new UndirectedGraph(size);
                i++;

            } else {

                readGraphUtil(strLine, graph);
            }
        }

        fStream.close();
        return graph;
    }

    /**
     * Utility function that parses a line
     * @param strLine - line containing edge information
     * @param graph - graph to add a new edge
     * @return Weigtehd edge object filled with the information in strLine
     */
    private static WeightedEdge readGraphUtil(String strLine, GraphInterface graph) {

        String[] strArr = strLine.split(" ");
        int u = Integer.parseInt(strArr[0]);
        int v = Integer.parseInt(strArr[1]);
        int w = Integer.parseInt(strArr[2]);

        WeightedEdge e = new WeightedEdge(u, v, w);
        graph.addEdge(e);
        return e;
    }

    /**
     * Get the name of files on a directory
     * https://www.baeldung.com/java-list-directory-files
     *
     * @param dir - directory
     * @return Set of strings
     */
    public static Set<String> listFiles(String dir) {
        System.out.println("lecture dir: " + dir);
        return Stream.of(Objects.requireNonNull(new File(dir).listFiles()))
                .filter(file -> !file.isDirectory())
                .map(File::getName)
                .collect(Collectors.toSet());
    }


    /**
     * Writes data to a file
     * @param treeMap - tree map holding as key the number of nodes and average time
     * @param fileName - file name
     * @throws IOException
     */
    public static void writeMapIntegerDouble(TreeMap<Integer, Double> treeMap, String fileName) throws IOException {

        File fout = new File(fileName);
        if(!fout.exists())
        {
            if(fout.getParentFile() != null)
            {
                fout.getParentFile().mkdir();
            }
            fout.createNewFile();
        }
        FileOutputStream fos = new FileOutputStream(fout);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

        for (Map.Entry<Integer, Double> entry : treeMap.entrySet()) {
            Double value = entry.getValue();
            Integer key = entry.getKey();
            String s = key + " " + value;
            bw.write(s);
            bw.newLine();
        }

        bw.close();
    }

    /**
     * Write data to file
     * @param treeMap - tree map holding as key E log V and averaged time
     * @param fileName - filename
     * @throws IOException
     */
    public static void writeMapDoubleDouble(Map<Double, Double> treeMap, String fileName) throws IOException {

        File fout = new File(fileName);
        if(!fout.exists())
        {
            if(fout.getParentFile() != null)
            {
                fout.getParentFile().mkdir();
            }
            fout.createNewFile();
        }
        FileOutputStream fos = new FileOutputStream(fout, true);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

        for (Map.Entry<Double, Double> entry : treeMap.entrySet()) {
            Double value = entry.getValue();
            Double key = entry.getKey();
            String s = key + " " + value;
            bw.write(s);
            bw.newLine();
        }

        bw.close();
    }

    /**
     * Write data to file
     * @param fileName - filename
     * @throws IOException
     */
    public static void writeMapDoubleDouble(List<Double> times, List<Double> logs, String fileName) throws IOException {

        assert times.size() == logs.size();

        File fout = new File(fileName);
        if(!fout.exists())
        {
            if(fout.getParentFile() != null)
            {
                fout.getParentFile().mkdir();
            }
            fout.createNewFile();
        }
        FileOutputStream fos = new FileOutputStream(fout);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

        for (int i = 0 ; i < times.size(); i++) {

            Double time = times.get(i);
            Double log = logs.get(i);

            String s = log + " " + time;
            bw.write(s);
            bw.newLine();
        }

        bw.close();
    }

    /**
     * Write a arboresncence to file
     * @param arboresncence - List of edges belonging to the arborescence
     * @param fileName - file name
     * @throws IOException
     */
    public static void writeArborescencToFile(List<WeightedEdge> arboresncence, String fileName) throws IOException
    {

        File fout = new File(fileName);
        if(!fout.exists())
        {
            if(fout.getParentFile() != null)
            {
                fout.getParentFile().mkdir();
            }
            fout.createNewFile();
        }
        FileOutputStream fos = new FileOutputStream(fout);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

        for (WeightedEdge w : arboresncence) {

            String line = w.getSrc() + " " + w.getDest() + " " + w.getWeight();
            bw.write(line);
            bw.newLine();
        }

        bw.close();
    }
}
