package net.phyloviz.io;

import net.phyloviz.phylogenetic.otu.OTU;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class PhylogeneticIO {

    /**
     * Reads a file containing philogenetic information
     * @param fileName - file name
     * @return List of Otus
     * @throws IOException
     */
    public static List<OTU> parseFile(String fileName) throws IOException {

        List<OTU> otus = new ArrayList<>();
        FileInputStream fStream = new FileInputStream(fileName);

        BufferedReader br = new BufferedReader(new InputStreamReader(fStream));
        String strLine;
        int n = 0;
        int len = 0;

        while ((strLine = br.readLine()) != null) {

            String[] strings = strLine.split("\t");

            if (n == 0) {
                len = strings.length;
                n++;
                continue;
            }

            int uID = Integer.parseInt(strings[0]);
            OTU otu = new OTU(uID, len - 1);

            for (int i = 1; i < len; i++) {
                 int profileID = Integer.parseInt(strings[i]);
                 otu.addLociID(profileID);
            }
            otus.add(otu);
        }

        fStream.close();
        return otus;
    }
}
