package net.phyloviz.phylogenetic.otu;

import java.util.*;

public class OTU {

    /**
     * This class represents Operational taxonomic unit
     *
     */

    // class attributes
    private int uID; // unique identifier
    private int[] profile; // genetic profile
    private int profileSize; // profile size
    private int currentProfilePosition; // current profile position

    /**
     * Class constructor
     * The profile array is empty
     *
     * @param uID         - unique identifier
     * @param profileSize - size of the profile
     */
    public OTU(int uID, int profileSize) {

        this.uID = uID;
        this.profileSize = profileSize;
        this.profile = new int[this.profileSize];
        this.currentProfilePosition = 0;
    }


    /**
     * Add a profileID to our profile array
     * @param profileID - profile identifier
     * @throws IllegalStateException - if
     */
    public void addLociID(int profileID) {

        if (this.currentProfilePosition == this.profileSize) {
            throw new IllegalStateException("Can not add more data to profile");
        }

        this.setLociIDToPosition(this.currentProfilePosition, profileID);
        this.currentProfilePosition++;
    }

    /**
     * Simple setter for profile id
     * @param position - position
     * @param loci - profile ID
     */
    public void setLociIDToPosition(int position, int loci) {

        this.profile[position] = loci;
    }

    /**
     * Getter for unique identifier parameter
     *
     * @return uID
     */
    public int getuID() {
        return this.uID;
    }

    /**
     * Getter for the profile
     *
     * @return profile
     */
    public int[] getProfile() {
        return this.profile;
    }

    /**
     * Getter for profile size
     *
     * @return profile size
     */
    public int getProfileSize() {
        return this.profileSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        OTU otu = (OTU) o;
        return uID == otu.uID &&
                profileSize == otu.profileSize &&
                Arrays.equals(profile, otu.profile);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(uID, profileSize);
        result = 31 * result + Arrays.hashCode(profile);
        return result;
    }

    @Override
    public String toString() {

        return "unique id: " + this.uID + " profile: " + Arrays.toString(this.profile);
    }
}
