package net.phyloviz.phylogenetic.matrix;

import net.phyloviz.phylogenetic.otu.OTU;

import java.util.List;

public class PairWiseMatrix {

    public List<OTU> otuList;
    public int size;

    /**
     * Class constructor
     * @param otuList - Otu list
     */
    public PairWiseMatrix(List<OTU> otuList) {

        if (otuList == null || otuList.size() == 0)
            throw new IllegalArgumentException("otuList is null or empty");

        this.otuList = otuList;
        this.size = this.otuList.size();
    }
 }
