package net.phyloviz.phylogenetic.matrix;

import net.phyloviz.graph.DirectedGraph;
import net.phyloviz.graph.UndirectedGraph;

public interface PairWiseMatrixInterface {

    DirectedGraph matrixToDirectedGraph();
    UndirectedGraph matrixToUndirectedGraph();
}
