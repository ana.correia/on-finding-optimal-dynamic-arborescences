package net.phyloviz.phylogenetic.matrix;

import net.phyloviz.graph.DirectedGraph;
import net.phyloviz.graph.UndirectedGraph;
import net.phyloviz.phylogenetic.otu.OTU;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class DirectedPairWiseMatrix extends PairWiseMatrix implements PairWiseMatrixInterface {


    /**
     * Private class representing a matrix entry
     * holds a source, destination and weight
     */
    private class MatrixEntry {

        public int u;
        public int v;
        public float w;

        /**
         * Private Class constructor
         *
         * @param u - source
         * @param v - dest
         * @param w - asymmetric hamming distance
         */
        MatrixEntry(int u, int v, float w) {
            this.u = u;
            this.v = v;
            this.w = w;
        }
    }

    // attributes
    private List<MatrixEntry> matrixEntryList; // list of matrix entries

    /**
     * Class constructor
     *
     * @param otuList - List containing all Otus of a bacteria
     */
    public DirectedPairWiseMatrix(List<OTU> otuList) {

        super(otuList);
        this.otuList = otuList;
        this.matrixEntryList = new ArrayList<>();
        this.size = otuList.size();
        this.buildMatrix();
    }

    /**
     * Build list containing matrix entry object
     */
    private void buildMatrix() {

        for (int i = 0; i < this.size; i++) {
            OTU otuI = this.otuList.get(i);
            for (int j = 0; j < this.size; j++) {

                OTU otuJ = this.otuList.get(j);
                // self-loops not considered
                if (i != j) {
                    float hammingDistance = this.asymmetricHammingDistance(otuI, otuJ);
                    this.matrixEntryList.add(new MatrixEntry(i, j, hammingDistance));
                }
            }
        }
        // sort the list
        Comparator<MatrixEntry> tupleComparator = (t1, t2) -> Float.compare(t1.w, t2.w);
        this.matrixEntryList.sort(tupleComparator);
    }

    /**
     * Parse matrix to directed graph
     *
     * @return directed graph
     */
    @Override
    public DirectedGraph matrixToDirectedGraph() {

        DirectedGraph directedGraph = new DirectedGraph();
        int w = 0;
        float oldEntry = -1.0F;

        for (MatrixEntry matrixEntry : this.matrixEntryList) {

            float currentEntry = matrixEntry.w;

            if (oldEntry != currentEntry) {
                directedGraph.addEdge(matrixEntry.u, matrixEntry.v, w);
                w++;
                oldEntry = currentEntry;

            } else {
                directedGraph.addEdge(matrixEntry.u, matrixEntry.v, w);
            }
        }
        return directedGraph;
    }

    @Override
    public UndirectedGraph matrixToUndirectedGraph() {
        throw new UnsupportedOperationException("Fast Ranked Relaxed does not support union yet");

    }

    /**
     * Calculate asymmetric hamming distance
     *
     * @param u - OTU u
     * @param v - OTU v
     * @return - asymmetric hamming distance
     */
    private float asymmetricHammingDistance(OTU u, OTU v) {

        float asymmetricHamming;
        int size = u.getProfileSize();
        int[] profileU = u.getProfile();
        int[] profileV = v.getProfile();
        float Nv = 0.0F;
        float pred = 0.0F;

        for (int i = 0; i < size; i++) {

            int piU = profileU[i];
            int piV = profileV[i];

            if ((piU != piV) && (piV != 0)) {
                pred++;
            }

            if (piV != 0) {
                Nv++;
            }
        }
        asymmetricHamming = pred / Nv;
        return asymmetricHamming;
    }
}
