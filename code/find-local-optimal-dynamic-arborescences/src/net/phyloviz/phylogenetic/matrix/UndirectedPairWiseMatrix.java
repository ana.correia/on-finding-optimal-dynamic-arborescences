package net.phyloviz.phylogenetic.matrix;

import net.phyloviz.graph.DirectedGraph;
import net.phyloviz.graph.GraphInterface;
import net.phyloviz.graph.UndirectedGraph;
import net.phyloviz.phylogenetic.otu.OTU;

import java.util.List;

public class UndirectedPairWiseMatrix extends PairWiseMatrix implements PairWiseMatrixInterface {

    public UndirectedPairWiseMatrix(List<OTU> otuList) {
        super(otuList);
    }

    /**
     * Calculate the hamming distance between two OTUs
     * @param otuI - otu i
     * @param otuJ - otu j
     * @return - hamming distance between OTus
     */
    private int hammingDistance(OTU otuI, OTU otuJ) {
        int hamming = 0;
        int profileSize = otuI.getProfileSize();
        int[] profileI = otuI.getProfile();
        int[] profileJ = otuJ.getProfile();

        for (int i = 0; i < profileSize; i++) {

            if (profileI[i] != profileJ[i]){

                hamming++;
            }
        }
        return hamming;
    }

    /**
     * Parses to a directed graph
     * @return - Directed graph
     */
    @Override
    public DirectedGraph matrixToDirectedGraph() {

        DirectedGraph graph = new DirectedGraph();
        this.matrixToGraphUtil(graph);
        return graph;
    }

    /**
     * Parses to a directed graph
     * @return - Directed graph
     */
    @Override
    public UndirectedGraph matrixToUndirectedGraph() {
        UndirectedGraph graph = new UndirectedGraph();
        this.matrixToGraphUtil(graph);
        return graph;
    }

    /**
     * Util function
     * @param graph - graph
     */
    private void matrixToGraphUtil(GraphInterface graph){
        for (int i = 0; i < this.size; i++) {
            OTU otuI = this.otuList.get(i);
            for (int j = 0; j < this.size; j++) {
                OTU otuJ = this.otuList.get(j);
                // self-loops not considered
                if (i != j) {
                    int hammingDistance = this.hammingDistance(otuI, otuJ);
                    graph.addEdge(i,j,hammingDistance);
                }
            }
        }
    }
}
