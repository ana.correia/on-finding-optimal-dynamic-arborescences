package net.phyloviz.graph;

import java.util.*;

public class DirectedGraph extends BaseGraph implements GraphInterface {

    private boolean reversed;

    /**
     * Class constructor
     *
     * @param v - number of nodes
     */
    public DirectedGraph(int v) {

        super(v);
        this.reversed = false;
    }

    /**
     * Constructor used to copyGraph graph
     *
     * @param graph - input graph
     */
    public DirectedGraph(DirectedGraph graph) {

        super(graph);
        this.reversed = false;
    }

    /**
     * Constructor
     */
    public DirectedGraph() {

        super();
        this.reversed = false;
    }

    /**
     * Find zero degree nodes in graph
     *
     * @return List containing zero degree
     */
    public List<Integer> findZeroDegreeNode() {

        List<Integer> zeroDegree = new LinkedList<>();

        Set<Integer> nodeSet = getNodeSet();

        for (Integer u : nodeSet) {

            List<WeightedEdge> neighbors = getNeighbors(u);

            if (neighbors.size() == 0) {

                zeroDegree.add(u);

            }
        }

        return zeroDegree;
    }

    /**
     * Add an edge to the graph
     *
     * @param src - source
     * @param dst - destination
     * @param w   - weight
     */
    @Override
    public void addEdge(int src, int dst, int w) {
        WeightedEdge weightedEdge = new WeightedEdge(src, dst, w);
        this.addEdgeUtil(src, dst, weightedEdge);
    }

    /**
     * Add an edge to the graph
     *
     * @param edge - weighted edge
     */
    @Override
    public void addEdge(WeightedEdge edge) {

        int src = edge.getSrc();
        int dst = edge.getDest();
        this.addEdgeUtil(src, dst, edge);
    }

    /**
     * Auxiliary function that adds an edge to the graph
     *
     * @param src  - edge source
     * @param dst  - edge destination
     * @param edge - edge object
     */
    private void addEdgeUtil(int src, int dst, WeightedEdge edge) {

        if (this.adjList.containsKey(src)) {
            LinkedList<WeightedEdge> tmp = this.adjList.get(src);
            tmp.add(edge);
        } else {

            LinkedList<WeightedEdge> tmp = new LinkedList<>();
            tmp.add(edge);
            this.adjList.put(src, tmp);
        }

        if (!this.adjList.containsKey(dst)) {

            this.adjList.put(dst, new LinkedList<>());
        }
    }

    /**
     * Finds all edged on the graph and return them
     *
     * @return List of all edges on the graph
     */
    @Override
    public List<WeightedEdge> getAllEdges() {

        List<WeightedEdge> weightedEdgeList = new LinkedList<>();
        Set<Integer> nodeSet = this.getNodeSet();

        for (Integer u : nodeSet) {

            weightedEdgeList.addAll(this.getNeighbors(u));
        }

        return weightedEdgeList;
    }

    /**
     * Find edge in the graph
     *
     * @param src - edge source
     * @param dst - edge destination
     * @return edge if it exists other null
     */
    public List<WeightedEdge> findEdge(int src, int dst) {
        return findEdgeUtil(src, dst, src);

    }

    /**
     * Find edge in the reversed graph
     *
     * @param src - edge source
     * @param dst - edge destination
     * @return edge if it exists other null
     */
    public List<WeightedEdge> findEdgeReversed(int src, int dst) {

        return findEdgeUtil(src, dst, dst);
    }

    /**
     * Find all edges with src and dst
     *
     * @param src - source node
     * @param dst - destination node
     * @param n   - incident
     * @return List of WeightedEdge object
     */
    private List<WeightedEdge> findEdgeUtil(int src, int dst, int n) {

        LinkedList<WeightedEdge> linkedList = new LinkedList<>();
        LinkedList<WeightedEdge> edges = this.getNeighbors(n);

        for (WeightedEdge e : edges) {

            if (e.getDest() == dst && e.getSrc() == src) {
                linkedList.add(e);
            }
        }

        return linkedList;
    }

    /**
     * Check if exists an edge with src and dst
     *
     * @param src - source node
     * @param dst - destination node
     * @return true if exists, false otherwise
     */
    public boolean findEdgeEndpoints(int src, int dst) {
        List<WeightedEdge> edgeList = this.adjList.get(src);
        for (WeightedEdge weightedEdge : edgeList) {
            if (src == weightedEdge.getSrc() && weightedEdge.getDest() == dst) {
                return true;
            }
        }

        return false;
    }

    /**
     * Add a list of edges
     *
     * @param src              - node
     * @param weightedEdgeList - edge list
     */
    public void addEdges(int src, List<WeightedEdge> weightedEdgeList) {
        List<WeightedEdge> weightedEdges = this.adjList.get(src);
        weightedEdges.addAll(weightedEdgeList);
    }

    /**
     * Returns the neighbors of a node
     *
     * @param src - node
     * @return Linked list of edges
     */
    public LinkedList<WeightedEdge> getNeighbors(int src) {
        return this.adjList.get(src);
    }

    /**
     * Getter for the node set
     *
     * @return node set
     */
    public Set<Integer> getNodeSet() {

        return this.adjList.keySet();
    }

    /**
     * Removes a node from the graph
     *
     * @param src - node id
     */
    public void removeNode(int src) {
        this.adjList.remove(src);
    }

    /**
     * This function removes an edge from the graph given an source and destination
     *
     * @param src - edge source
     * @param dst - edge destination
     */
    @Override
    public void removeEdge(int src, int dst) {

        this.removeEdgeUtil(src, dst, src);
    }

    public void removeWeightedEdge(WeightedEdge edge) {

        List<WeightedEdge> list = this.adjList.get(edge.getSrc());
        list.remove(edge);
    }

    /**
     * This function removes an edge from the graph given an source and destination
     *
     * @param edge - edge object
     */
    public void removeEdge(WeightedEdge edge) {
        int src = edge.getSrc();
        int dst = edge.getDest();
        this.removeEdgeUtil(src, dst, src);
    }

    /**
     * This function removes an edge from the  reversed graph given an source and destination
     *
     * @param src - edge source
     * @param dst - edge destination
     */
    public void removeEdgeReversed(int src, int dst) {

        this.removeEdgeUtil(src, dst, dst);
    }

    /**
     * Util function to remove the nod
     *
     * @param src - edge source
     * @param dst - edge destination
     * @param n   -
     */
    private void removeEdgeUtil(int src, int dst, int n) {

        List<WeightedEdge> weightedEdges = this.findEdgeUtil(src, dst, n);
        LinkedList<WeightedEdge> edgeLinkedList = this.getNeighbors(n);

        for (WeightedEdge weightedEdge : weightedEdges) {
            edgeLinkedList.remove(weightedEdge);
        }
    }

    /**
     * Adds a new node to the graph
     */
    public void addNode(int src) {
        LinkedList<WeightedEdge> e = new LinkedList<>();
        this.adjList.put(src, e);
    }

    /**
     * Remove all neighbors of a given node
     *
     * @param src - node id
     */
    public void clearNeighbors(int src) {
        this.adjList.get(src).clear();
    }

    /**
     * Returns number of nodes
     *
     * @return number of nodes
     */
    public int size() {
        return this.getNodeSet().size();
    }

    @Override
    public int nEdges() {

        int res = 0;
        Set<Integer> nodeSet = this.getNodeSet();

        for (Integer node : nodeSet) {

            res += this.getNeighbors(node).size();
        }

        return res;
    }

    /**
     * This function reverses the graph
     * Worst case is O(|V| + |E|)
     */
    public void reverseGraph() {

        HashMap<Integer, LinkedList<WeightedEdge>> newAdjList = new HashMap<>();
        Set<Integer> nodeSet = this.getNodeSet();

        for (Integer i : nodeSet) {
            newAdjList.put(i, new LinkedList<>());
        }


        for (Integer src : nodeSet) {

            LinkedList<WeightedEdge> current = this.adjList.get(src);

            for (WeightedEdge e : current) {

                int v = this.reverseEndpoint(e);
                LinkedList<WeightedEdge> reverse = newAdjList.get(v);
                reverse.add(e);
            }
        }

        this.adjList = newAdjList;
        this.reversed = !this.reversed;
    }

    private int reverseEndpoint(WeightedEdge edge) {

        if (this.reversed)
            return edge.getSrc();
        else
            return edge.getDest();
    }

    /**
     * Override of toString method
     *
     * @return String with all edges of the graph
     */
    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder();
        for (Map.Entry<Integer, LinkedList<WeightedEdge>> entry : this.adjList.entrySet()) {

            List<WeightedEdge> weightedEdges = entry.getValue();
            Integer node = entry.getKey();

            builder.append(node);
            builder.append(": ");
            builder.append(weightedEdges.toString());
            builder.append("\n");
        }

        return builder.toString();
    }

    /**
     * Override of equals method
     *
     * @param other - object
     * @return true if both graphs are equal, false otherwise
     */
    @Override
    public boolean equals(Object other) {

        if (other == null)
            return false;

        if (!(other instanceof DirectedGraph))
            return false;

        DirectedGraph otherGraph = (DirectedGraph) other;

        if (this.size() != otherGraph.size())
            return false;

        Set<Integer> nodeSet = this.getNodeSet();
        Set<Integer> otherNodeSet = otherGraph.getNodeSet();

        if (!nodeSet.equals(otherNodeSet))
            return false;

        for (Integer i : nodeSet) {

            LinkedList<WeightedEdge> e1 = this.getNeighbors(i);
            LinkedList<WeightedEdge> e2 = otherGraph.getNeighbors(i);

            if (e1.size() != e2.size())
                return false;

            if (!e1.equals(e2))
                return false;
        }

        return true;
    }
}
