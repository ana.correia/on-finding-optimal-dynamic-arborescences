package net.phyloviz.graph;

import java.io.Serializable;
import java.util.Objects;

public class Edge implements Serializable {

    static final long serialVersionUID = 50312312L;

    protected int src;
    protected int dest;

    /**
     * Class constructor
     * @param src - edge source
     * @param dest - edge destination
     */
    public Edge(int src, int dest) {

        this.src = src;
        this.dest = dest;
    }

    /**
     * Class constructor
     * @param e - edge
     */
    public Edge(Edge e) {
        this.src = e.src;
        this.dest = e.dest;
    }


    /**
     * Getter for source of the edge
     *
     * @return source of the edge
     */
    public int getSrc() {
        return this.src;
    }

    /**
     * Getter for destination of the edge
     *
     * @return destination of the edge
     */
    public int getDest() {
        return this.dest;
    }


    /**
     * Setter for src
     *
     * @param src - edge source
     */
    public void setSrc(int src) {

        this.src = src;
    }

    /**
     * Setter for dest
     *
     * @param dest - edge destination
     */
    public void setDest(int dest) {

        this.dest = dest;
    }


    /**
     * Override of to string method
     *
     * @return string containing edge information
     */
    @Override
    public String toString() {

        return "src: " + this.src + " dest: " + this.dest;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edge edge = (Edge) o;
        return src == edge.src &&
                dest == edge.dest;
    }

    @Override
    public int hashCode() {
        return Objects.hash(src, dest);
    }
}
