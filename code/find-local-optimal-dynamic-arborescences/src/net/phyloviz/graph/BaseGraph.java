package net.phyloviz.graph;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

public class BaseGraph {

    HashMap<Integer, LinkedList<WeightedEdge>> adjList;

    /**
     * Class constructor
     *
     * @param v - number of nodes
     */
    public BaseGraph(int v) {

        this.adjList = new HashMap<>();

        for (int i = 0; i < v; i++) {
            this.adjList.put(i, new LinkedList<>());
        }
    }


    /**
     * Constructor used to copyGraph graph
     *
     * @param graph - input graph
     */
    public BaseGraph(BaseGraph graph) {

        this.adjList = this.copyGraph(graph.adjList);
    }

    public BaseGraph() {
        this.adjList = new HashMap<>();
    }


    /**
     * This function performs an explicit copy of the adjacency list represented by an HashMap
     *
     * @param original - original graph
     * @return copy of the original graph
     */
    public HashMap<Integer, LinkedList<WeightedEdge>> copyGraph(HashMap<Integer, LinkedList<WeightedEdge>> original) {
        HashMap<Integer, LinkedList<WeightedEdge>> copy = new HashMap<>();

        for (Map.Entry<Integer, LinkedList<WeightedEdge>> entry : original.entrySet()) {

            LinkedList<WeightedEdge> copyLinkedList = new LinkedList<>();

            for (WeightedEdge e : entry.getValue()) {

                WeightedEdge copyWeightedEdge = new WeightedEdge(e);
                copyLinkedList.add(copyWeightedEdge);

            }

            copy.put(entry.getKey(), copyLinkedList);
        }

        return copy;
    }

    /**
     * Getter for the node set
     *
     * @return node set
     */
    public Set<Integer> getNodeSet() {

        return this.adjList.keySet();
    }


}
