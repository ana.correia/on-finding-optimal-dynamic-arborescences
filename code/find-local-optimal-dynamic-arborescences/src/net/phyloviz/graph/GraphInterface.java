package net.phyloviz.graph;

import java.util.List;
import java.util.Set;

public interface GraphInterface {

    Set<Integer> getNodeSet();

    List<WeightedEdge> getNeighbors(int src);

    List<WeightedEdge> getAllEdges();

    void addEdge(int src, int dst, int w);

    void addEdge(WeightedEdge edge);

    void removeEdge(int src, int dst);

    int size();

    int nEdges();
}
