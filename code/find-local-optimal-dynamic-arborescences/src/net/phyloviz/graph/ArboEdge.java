package net.phyloviz.graph;

public class ArboEdge extends WeightedEdge {

    int originalWeight;

    public ArboEdge(int u, int v, int w) {
        super(u, v, w);
    }

    public ArboEdge(WeightedEdge e) {
        super(e);
    }
}
