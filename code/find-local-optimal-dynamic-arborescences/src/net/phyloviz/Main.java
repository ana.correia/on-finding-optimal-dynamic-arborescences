package net.phyloviz;

import net.phyloviz.comparators.Comparators;
import net.phyloviz.dynamic.algo.arborescence.ATree;
import net.phyloviz.dynamic.algo.arborescence.BlackBox;
import net.phyloviz.dynamic.algo.arborescence.DynamicArborescence;
import net.phyloviz.graph.DirectedGraph;
import net.phyloviz.graph.UndirectedGraph;
import net.phyloviz.graph.WeightedEdge;
import net.phyloviz.immutable.algo.arborescence.*;
import net.phyloviz.immutable.algo.arborescence.camerini.SolutionForest;
import net.phyloviz.immutable.algo.connectivity.KosarajuSCC;
import net.phyloviz.immutable.algo.connectivity.StronglyConnected;
import net.phyloviz.immutable.algo.tree.MSTAlgorithm;
import net.phyloviz.immutable.queue.HeapType;
import net.phyloviz.io.Directory;
import net.phyloviz.io.IO;
import net.phyloviz.serializable.Serializer;
import net.phyloviz.util.ArboEnum;
import net.phyloviz.validation.Validation;

import java.io.IOException;
import java.util.*;


public class Main {

    @SuppressWarnings("Duplicates")
    public static void main(String[] args) throws Exception {

        /*
        Set<String> dir = IO.listFiles(Directory.CONNECTED_GRAPH_DIR);
        for (String name: dir) {
            DirectedGraph g = IO.readDirectedGraph(Directory.CONNECTED_GRAPH_DIR+ name);
            testAdd(g);
            Runtime.getRuntime().exec("clear");
        }

         */

        /*
        BlackBox blackBox = new BlackBox(new DirectedGraph(g), Comparators.totalOrderCmp());
        blackBox.getArborescence();
        blackBox.getATree().printTreeTransversal();
        blackBox.getATree().getSolutionForest().resetSolutionForest();
        blackBox.getATree().getSolutionForest().printTreeTransversal();

         */
        //testDelete(IO.readDirectedGraph(stronglyConnectedDir));

        Set<String> dir = IO.listFiles(Directory.CONNECTED_GRAPH_DIR);
        for (String name: dir) {
            DirectedGraph g = IO.readDirectedGraph(Directory.CONNECTED_GRAPH_DIR+ name);
            testDelete(g);
            Runtime.getRuntime().exec("clear");
        }
    }

    private static void expand() throws IOException {

        Set<String> dir = IO.listFiles(Directory.CONNECTED_GRAPH_DIR);
        //LinkedList<String> dir = new LinkedList<>();
        //dir.add("small_tree_dynamic.txt");
        for (String s : dir) {
            debugExpansion(Directory.CONNECTED_GRAPH_DIR + s);
        }
    }

    @SuppressWarnings("Duplicates")
    private static void debugExpansion(String gName) throws IOException {

        DirectedGraph directedG = IO.readDirectedGraph(gName);
        DirectedGraph directedG2 = new DirectedGraph(directedG);

        Arborescence edmondsFastReduction = new EdmondsSlowReduction(new DirectedGraph(directedG2), Comparators.totalOrderCmp(), HeapType.BINOMIAL, true, true, ArboEnum.FASTER_ARBO_DISJOINT_SET);
        List<WeightedEdge> l3 = edmondsFastReduction.getArborescence();
        l3.sort(Comparators.getSolutionComparator());
        HashSet<WeightedEdge> s3 = new HashSet<>(l3);


        //Arborescence blackBox = new CameriniExpansion(new DirectedGraph(directedG2), Comparators.totalOrderCmp(), HeapType.BINOMIAL, true, true, ArboEnum.FASTER_ARBO_DISJOINT_SET);
        Arborescence blackBox = new BlackBox(new DirectedGraph(directedG2),  Comparators.totalOrderCmp());
        List<WeightedEdge> edgeList = blackBox.getArborescence();

        //System.out.println("FAST REDU:");
        //ArboChecker arboChecker = new ArboChecker(l3);

        //List<WeightedEdge> edgeList = blackBox.getSolutionForest().expansion();
        edgeList.sort(Comparators.getSolutionComparator());

        HashSet<WeightedEdge> s2 = new HashSet<>(edgeList);

        System.out.println("BAD: " + edgeList);
        System.out.println("GOD: " + l3);

        if (edmondsFastReduction.computeWeight(l3) != blackBox.computeWeight(edgeList)) {

            System.out.println("diff w " + gName);
            System.out.println("O: " + edmondsFastReduction.computeWeight(l3) + " new: " + blackBox.computeWeight(edgeList));
            HashSet<WeightedEdge> ss3 = new HashSet<>(s3);
            ss3.removeAll(s2);
            System.out.println("DIFF: " + ss3);
            HashSet<WeightedEdge> ss2 = new HashSet<>(s2);
            ss2.removeAll(s3);
            System.out.println("DIFF: " + ss2);


            ArboChecker arboChecker = new ArboChecker(l3);
            ArboChecker arboChecker1 = new ArboChecker(edgeList);
        }
        System.out.println("#############################################");
    }

    private static void runEfficientEdmonds() throws IOException {

        String gName = "directed_graph3.txt";
        String g1Name = "strongly_connected_graph/50_cc_graph.txt";
        String g2Name = "paper.txt";
        String g3Name = "sparse_graph_2800_0.txt";
        String g4Name = "small_tree_dynamic.txt";
        String g5Name = "fully_dynamic_tree.txt";

        DirectedGraph G = IO.readDirectedGraph(gName);

        WeightedEdge[] removeArray = new WeightedEdge[]{
                new WeightedEdge(0,1,1),
                new WeightedEdge(3,2,1),
                new WeightedEdge(0,3,2),
                new WeightedEdge(1,3,10)};


        removeArray = new WeightedEdge[]{
                new WeightedEdge(0,2,5)
                //new WeightedEdge(2,0,15),
                //new WeightedEdge(3,1,10),
                //new WeightedEdge(4,6,12),
                //new WeightedEdge(6,7,1)
        };


        for (WeightedEdge remove: G.getAllEdges()) {

            System.out.println("REMOVE: " + remove);
            BlackBox blackBox = new BlackBox(new DirectedGraph(G), Comparators.totalOrderCmp());
            List<WeightedEdge> arbo = blackBox.getArborescence();
            ATree aTree = blackBox.getATree();
            //aTree.printTreeTransversal();

            DirectedGraph copyOfG = new DirectedGraph(G);
            copyOfG.removeEdge(remove);
            System.out.println("*#######################################################################*");

            /*
            Arborescence cameriniExpansion = new CameriniExpansion(new DirectedGraph(copyOfG),
                                                                    Comparators.totalOrderCmp(),
                                                                    HeapType.BINOMIAL,
                                                                    true,
                                                                    true,
                                                                    ArboEnum.FASTER_ARBO_DISJOINT_SET);

             */

            BlackBox cameriniExpansion =  new BlackBox(new DirectedGraph(copyOfG), Comparators.totalOrderCmp());
            List<WeightedEdge> cameriniSol = cameriniExpansion.getArborescence();
            HashSet<WeightedEdge> setCameriniSol = new HashSet<>(cameriniSol);

            System.out.println("*#######################################################################*");
            System.out.println("REMOVE: " + remove);

            DynamicArborescence dynamicArbo = new DynamicArborescence(aTree, Comparators.totalOrderCmp(), new DirectedGraph(G));
            List<WeightedEdge> newSolution = dynamicArbo.removeEdge(remove);
            HashSet<WeightedEdge> newSolutionSet = new HashSet<>(newSolution);

            if (!newSolutionSet.equals(setCameriniSol)) {
                cameriniSol.sort(Comparators.totalOrderCmp());
                newSolution.sort(Comparators.totalOrderCmp());
                System.out.println("CORRECT: " + cameriniSol + " W: " + cameriniExpansion.computeWeight(cameriniSol) +
                        " ROOT: " + cameriniExpansion.getATree().getSolutionForest().R);

                System.out.println("DYNAMIC: " + newSolution + " W: " + cameriniExpansion.computeWeight(newSolution) +
                        " ROOT: " + dynamicArbo.getAtree().getSolutionForest().R);

                SolutionForest f = dynamicArbo.getAtree().getSolutionForest();
                f.resetSolutionForest();
                f.printTreeTransversal();

            }
        }

    }

    private static void testDelete(DirectedGraph G) throws Exception {

        DirectedGraph GG = new DirectedGraph(G);
        BlackBox blackBox = new BlackBox(new DirectedGraph(G), Comparators.totalOrderCmp());
        blackBox.getArborescence();
        List<WeightedEdge> edgeList = new LinkedList<>(blackBox.getATree().getEdgesInTree());

        for (int i = 0; i < 2; i++){
            List<WeightedEdge> lst = new ArrayList<>(edgeList);
            Collections.shuffle(lst);
            testDeleteUtil(GG,lst);
            System.out.println("#####################################");
        }

    }

    @SuppressWarnings("Duplicates")
    private static void testDeleteUtil(DirectedGraph G, List<WeightedEdge> removeArr) throws Exception {

        DirectedGraph G1 = new DirectedGraph(G);
        DirectedGraph G2 = new DirectedGraph(G);

        BlackBox blackBox = new BlackBox(new DirectedGraph(G), Comparators.totalOrderCmp());
        blackBox.getArborescence();

        ATree aTree = blackBox.getATree();
        DynamicArborescence dynamicArborescence = new DynamicArborescence(aTree, Comparators.totalOrderCmp(), G1);
        List<WeightedEdge> removedList = new LinkedList<>();
        boolean breakConnectivity = false;

        for (WeightedEdge edge: removeArr) {

            if (edge == null)
                continue;

            if (breakConnectivity)
                break;

            System.out.println("===================================================================================");
            System.out.println("REMOVE: " + edge);
            G2.removeEdge(edge);

            StronglyConnected stronglyConnected = new KosarajuSCC(new DirectedGraph(G2), 0);
            if (!stronglyConnected.isStronglyConnected()){
                System.out.println("Breaks connectivity: " + edge);
                breakConnectivity = true;
                G2.addEdge(edge);
            }

            removedList.add(edge);

            CameriniExpansion cameriniExpansion = new CameriniExpansion(new DirectedGraph(G2), Comparators.totalOrderCmp(), HeapType.BINOMIAL,
                    true, true, ArboEnum.FASTER_ARBO_DISJOINT_SET);
            List<WeightedEdge> solution = cameriniExpansion.getArborescence();


            List<WeightedEdge> dynamicSol = dynamicArborescence.removeEdge(edge);
            dynamicArborescence.getAtree().getSolutionForest().resetSolutionForest();

            if (!new HashSet<>(solution).equals(new HashSet<>(dynamicSol))){

                System.err.println("PROBLEM");

                System.out.println("CORRECT: " + solution + " W: " + cameriniExpansion.computeWeight(solution) + " ROOT: "  + cameriniExpansion.getSolution().R);

                System.out.println("+++++++++++++++++++++++++++++++++++");
                SolutionForest camerini = cameriniExpansion.getSolution();
                camerini.resetSolutionForest();
                camerini.printTreeTransversal();
                System.out.println("+++++++++++++++++++++++++++++++++++");

                System.out.println("DYNAMIC: " + dynamicSol + " W: " + cameriniExpansion.computeWeight(dynamicSol) +
                        " ROOT: " + dynamicArborescence.getAtree().getSolutionForest().R);

                System.out.println("******************************************");
                SolutionForest d = blackBox.getATree().getSolutionForest();
                d.resetSolutionForest();
                d.printTreeTransversal();
                System.out.println(";;;;;;;;");
                aTree.printTreeTransversal();
                System.out.println("******************************************");

                throw new Exception("DYNAMIC IS NOT FULLY WORKING");
            }

            System.out.println("===================================================================================");

        }
        System.out.println("REMOVED SEQUENCE : " + removedList);
    }

    @SuppressWarnings("Duplicates")
    private static void testAdd(DirectedGraph G) throws Exception {


        int n = Collections.max(G.getNodeSet());

        DirectedGraph G1 = new DirectedGraph(G);
        DirectedGraph G2 = new DirectedGraph(G);

        BlackBox blackBox = new BlackBox(new DirectedGraph(G1), Comparators.totalOrderCmp());
        List<WeightedEdge> arbo = blackBox.getArborescence();
        ATree tree = blackBox.getATree();
        DynamicArborescence dynamicArborescence = new DynamicArborescence(tree, Comparators.totalOrderCmp(), G1);

        Random random = new Random(System.currentTimeMillis());
        int maxW = Collections.max(G.getAllEdges(), Comparators.totalOrderCmp()).getWeight() / 2;
        Serializer<List<WeightedEdge>> s = new Serializer<List<WeightedEdge>>();


        List<WeightedEdge> createEdges = Validation.createRandomEdges(random, n, maxW, G);
        //List<WeightedEdge> createRandomEdges = s.deserialize("adding_seq.ser");

        for (WeightedEdge newEdge: createEdges) {

            System.out.println("################################################################################");
            System.out.println("ADDING : " + newEdge);

            List<WeightedEdge> dynamicSolution = dynamicArborescence.addNewEdge(new WeightedEdge(newEdge));

            G2.addEdge(new WeightedEdge(newEdge));


            CameriniExpansion blackBox1 = new CameriniExpansion(new DirectedGraph(G2), Comparators.totalOrderCmp(),
                    HeapType.BINOMIAL,
                    true, true, ArboEnum.FASTER_ARBO_DISJOINT_SET);
            List<WeightedEdge> solution = blackBox1.getArborescence();

            /*
            System.out.println("################################################");
            System.out.println("DYNAMIC TREE: ");
            dynamicArborescence.getAtree().getSolutionForest().resetSolutionForest();
            dynamicArborescence.getAtree().getSolutionForest().printTreeTransversal();

            dynamicArborescence.getAtree().printTreeTransversal();

            System.out.println("****************************************************");
            System.out.println("CORRECT TREE: ");
            blackBox1.getSolution().resetSolutionForest();
            blackBox1.getSolution().printTreeTransversal();
            System.out.println("****************************************************");

             */


            //!(new HashSet<>(solution).equals(new HashSet<>(dynamicSolution)))

            if ( blackBox.computeWeight(solution) != blackBox.computeWeight(dynamicSolution) )
            {

                solution.sort(Comparators.totalOrderCmp());
                dynamicSolution.sort(Comparators.totalOrderCmp());

                System.out.println("CORRECT: " + " W: " + blackBox1.computeWeight(solution) + " ROOT: " + blackBox1.getSolution().R + solution );
                System.out.println("DYNAMIC: " + " W: " + blackBox.computeWeight(dynamicSolution) + " ROOT: " + dynamicArborescence.getAtree().getSolutionForest().R + dynamicSolution);

                try {

                    throw new Exception("DYNAMIC IS NOT FULLY WORKING");

                } catch (Exception e) {

                    //Serializer<List<WeightedEdge>> serializable = new Serializer<List<WeightedEdge>>();
                    //serializable.serialize("adding_seq.ser", createRandomEdges);

                    e.printStackTrace();
                    System.exit(-1);
                }

            }

            System.out.println("################################################################################");
        }

    }

    private static void runEfficientEdmondsUtil(Arborescence ed) {
        long start;
        List<WeightedEdge> arbo;
        long end;
        double elapsed;
        start = System.currentTimeMillis();
        arbo = ed.getArborescence();
        if (arbo != null)
            arbo.sort(Comparators.totalOrderCmp());
        end = System.currentTimeMillis();
        elapsed = (end - start) / 1000F;
        if (arbo != null)
            System.out.println("W: " + ed.computeWeight(arbo));
        System.out.println("Sol: " + arbo);
        System.out.println("Elapsed: " + elapsed);
        System.out.println(ed.getBenchMark());
        System.out.println("#####################################################################");
    }

    private static void runPrim() throws IOException {

        String undirectedGraphName = "undirected_complete/complete1000_0.txt";
        UndirectedGraph undirectedGraph = IO.readUndirectedGraph(undirectedGraphName);
        MSTAlgorithm prim = new MSTAlgorithm(undirectedGraph, Comparators.getSimpleComparator(), HeapType.FAST_RANK_RELAXED);

        long s = System.currentTimeMillis();
        List<WeightedEdge> tree = prim.getTree();
        long e = System.currentTimeMillis();
        float elapTime = (e - s) / 1000F;
        System.out.println("Tree: " + tree);
        System.out.println("Tree W: " + prim.computeWeight(tree));
        System.out.println("Elapsed time: " + elapTime + " s");
    }

    private static void runBatchEfficeint(String graphDir, String resultsDir) throws IOException {

        Set<String> stringsGraphName = IO.listFiles(graphDir);

        List<String> mainList = new ArrayList<>(stringsGraphName);
        mainList.sort(String::compareTo);

        for (String gName : mainList) {
            System.out.println(gName);

            DirectedGraph g = IO.readDirectedGraph(graphDir + gName);


            Arborescence efficientArboresncence = new EdmondsJavaHeap(g, Comparators.getSimpleComparator());
            List<WeightedEdge> arbo = efficientArboresncence.getArborescence();

            DirectedGraph arboGraph = new DirectedGraph();
            for (WeightedEdge edge : arbo) {
                arboGraph.addEdge(edge);
            }

            ArboChecker arborescenceChecker = new ArboChecker(arboGraph);


            if (!arborescenceChecker.checkArbo()) {

                System.err.println(gName + " not an arborescence");
            }


            IO.writeArborescencToFile(arbo, resultsDir + gName);
            System.out.println("#######");
        }
    }

    @SuppressWarnings("Duplicates")
    private static void runBatchOriginal(String graphDir, String resultsDir) throws IOException {

        Set<String> stringsGraphName = IO.listFiles(graphDir);

        List<String> mainList = new ArrayList<>(stringsGraphName);
        mainList.sort(String::compareTo);

        for (String gName : mainList) {
            System.out.println("G: " + gName);
            DirectedGraph g = IO.readDirectedGraph(graphDir + gName);
            Arborescence original = new OriginalImpl(g, Comparators.getSimpleComparator());
            List<WeightedEdge> arbo = original.getArborescence();
            IO.writeArborescencToFile(arbo, resultsDir + gName);
            System.out.println("#######");
        }
    }
}
