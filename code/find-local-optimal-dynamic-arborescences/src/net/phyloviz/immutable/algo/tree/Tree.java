package net.phyloviz.immutable.algo.tree;

import net.phyloviz.graph.WeightedEdge;

import java.util.List;

public interface Tree {

    List<WeightedEdge> getTree();
    int computeWeight(List<WeightedEdge> lst);
}
