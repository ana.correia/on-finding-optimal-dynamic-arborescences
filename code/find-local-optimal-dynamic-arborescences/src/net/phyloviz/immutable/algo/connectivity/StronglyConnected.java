package net.phyloviz.immutable.algo.connectivity;

public interface StronglyConnected {

    public boolean isStronglyConnected();
}
