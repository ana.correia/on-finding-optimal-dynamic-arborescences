package net.phyloviz.immutable.algo.connectivity;


import net.phyloviz.graph.DirectedGraph;
import net.phyloviz.graph.WeightedEdge;

import java.util.HashMap;
import java.util.Set;

public class DirectedDfs {

    private HashMap<Integer, Boolean> visited;
    private DirectedGraph G;
    private int root;

    public DirectedDfs(DirectedGraph G, int root) {

        this.G = G;
        this.root = root;
        this.visited = new HashMap<>();

        Set<Integer> nodeSet = this.G.getNodeSet();

        for (Integer u : nodeSet) {
            this.visited.put(u, false);
        }

        this.dfs(this.root);
    }

    private void dfs(int v) {

        this.visited.put(v, true);
        for (WeightedEdge edge : this.G.getNeighbors(v)) {
            int u = edge.getDest();

            if (!this.visited.get(u))
                this.dfs(u);
        }
    }

    public boolean marked(int u) {
        return this.visited.get(u);
    }

    public boolean allNodesReachableFromRoot() {

        boolean result = true;
        Set<Integer> nodeSet = this.G.getNodeSet();

        for (Integer u : nodeSet) {
            if (u != this.root) {
                if (!this.marked(u))
                    result = false;
            }
        }

        return result;
    }


}
