package net.phyloviz.immutable.algo.connectivity;

import net.phyloviz.graph.DirectedGraph;
import net.phyloviz.graph.WeightedEdge;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class KosarajuSCC implements StronglyConnected {

    private DirectedGraph G;
    private HashMap<Integer, Boolean> visited;
    private int root;

    /**
     * Class constructor
     *
     * @param G - input graph
     */
    public KosarajuSCC(DirectedGraph G, int root) {

        this.G = G;
        this.root = root;
        this.visited = new HashMap<>();
    }


    /**
     * This function performs a dfs and marks the visited hash map
     *
     * @param G - input graph
     * @param v - root
     */
    private void dfs(DirectedGraph G, int v) {

        // Mark the current node as visited
        this.visited.put(v, true);

        for (WeightedEdge edge : G.getNeighbors(v)) {
            int u = edge.getDest();

            if (!this.visited.get(u)) {
                this.dfs(G, u);
            }
        }
    }

    /**
     * This function performs a dfs and marks the visited hash map
     *
     * @param G - input graph
     * @param v - root
     */
    private void reverseDfs(DirectedGraph G, int v) {

        // Mark the current node as visited and print it
        this.visited.put(v, true);

        for (WeightedEdge edge : G.getNeighbors(v)) {

            int u = edge.getSrc();

            if (!this.visited.get(u)) {
                this.reverseDfs(G, u);
            }
        }
    }

    /**
     * Utility function that initializes the hash with false at every entry
     */
    private void initMapUtil() {
        Set<Integer> nodeSet = this.G.getNodeSet();
        for (Integer u : nodeSet)
            this.visited.put(u, false);
    }

    /**
     * This function checks if any false entry exists on visited hashmap
     *
     * @return true if false entry exists, false otherwise
     */
    private boolean mapHasFalseEntry() {

        for (Map.Entry<Integer, Boolean> entry : this.visited.entrySet()) {
            if (!entry.getValue()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Kosaraju Strongly Connected algorithm
     * Algorithm steps:
     * 1 - Initialize the visited map to false
     * 2 - Perform a dfs marking visited nodes
     * 3 - Check if visited nodes as any false entry , it it has return false
     * 4 - reverse the graph
     * 5 - Reset marking on visited nodes
     * 5 - Perform a dfs marking visited nodes
     * 6 - Check if visited nodes as any false entry , it it has return false
     * 7 - return true - the graph is strongly connected
     *
     * @return true if the graph is strongly connect, false otherwise
     */
    @Override
    public boolean isStronglyConnected() {

        this.initMapUtil();
        this.dfs(this.G, this.root);

        boolean falseEntry = this.mapHasFalseEntry();

        if (falseEntry)
            return false;

        DirectedGraph reversed = new DirectedGraph(this.G);
        reversed.reverseGraph();

        this.initMapUtil();

        this.reverseDfs(reversed, this.root);

        falseEntry = this.mapHasFalseEntry();

        if (falseEntry)
            return false;

        return true;
    }
}

