package net.phyloviz.immutable.algo.arborescence;


import net.phyloviz.Arborescence;
import net.phyloviz.util.ArboEnum;
import net.phyloviz.graph.DirectedGraph;
import net.phyloviz.graph.WeightedEdge;
import net.phyloviz.immutable.queue.HeapType;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class EdmondsSlowReduction extends AbstractArbo implements Arborescence {

    public EdmondsSlowReduction(DirectedGraph G, Comparator<WeightedEdge> cmp, HeapType heapType, boolean smartStorage, ArboEnum arboEnum) {

        super(G, cmp, heapType, smartStorage, arboEnum);
    }

    public EdmondsSlowReduction(DirectedGraph G, Comparator<WeightedEdge> cmp, HeapType heapType, boolean smartStorage, boolean floydHeapConstruction, ArboEnum arboEnum) {

        super(G,cmp,heapType,smartStorage, floydHeapConstruction, arboEnum);
    }

    @Override
    protected Comparator<Integer> getHeapComparator() {

        Comparator<Integer> icmp = new Comparator<Integer>() {

            @Override
            public int compare(Integer integer1, Integer integer2) {

                WeightedEdge edge1 = weightedEdges[integer1];
                WeightedEdge edge2 = weightedEdges[integer2];
                return cmp.compare(edge1, edge2);
            }
        };

        return icmp;
    }

    @Override
    protected Comparator<WeightedEdge> getMaxDisjointCmp() {
        return this.cmp;
    }

    @Override
    protected WeightedEdge getAdjustedWeightedEdge(WeightedEdge edge) {
        return edge;
    }

    @Override
    protected int getAdjustedWeight(WeightedEdge edge) {
        return edge.getWeight();
    }

    @Override
    protected void computeReducedCost(List<Integer> contractionSet, Map<Integer, WeightedEdge> map, int maxW) {

        for (Integer node : contractionSet) {

            int incidentW = map.get(node).getWeight();
            Set<Integer> keySet = this.queues[node].getKeySet();

            for (Integer key : keySet) {
                WeightedEdge edge = this.weightedEdges[key];
                int newW = edge.getWeight() + maxW - incidentW ;
                edge.setWeight(newW);
                this.nReductions++;
            }
        }
    }

    @Override
    public List<WeightedEdge> getArborescence() {
        return this.getArborescenceUtil();
    }
}
