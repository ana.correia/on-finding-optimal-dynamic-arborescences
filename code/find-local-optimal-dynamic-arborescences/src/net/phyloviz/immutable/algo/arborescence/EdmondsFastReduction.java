package net.phyloviz.immutable.algo.arborescence;

import net.phyloviz.Arborescence;
import net.phyloviz.util.ArboEnum;
import net.phyloviz.graph.DirectedGraph;
import net.phyloviz.graph.WeightedEdge;
import net.phyloviz.immutable.queue.HeapType;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class EdmondsFastReduction extends AbstractArbo implements Arborescence {

    public EdmondsFastReduction(DirectedGraph G,  Comparator<WeightedEdge> cmp)  {
        super(G, cmp);
    }

    public EdmondsFastReduction(DirectedGraph G, Comparator<WeightedEdge> cmp, HeapType heapType, boolean smartStorage, ArboEnum arboEnum) {

        super(G, cmp, heapType, smartStorage, arboEnum);
    }

    public EdmondsFastReduction(DirectedGraph G, Comparator<WeightedEdge> cmp, HeapType heapType, boolean smartStorage, boolean floydHeapConstruction, ArboEnum arboEnum) {

        super(G,cmp,heapType,smartStorage, floydHeapConstruction, arboEnum);
    }


    @Override
    public List<WeightedEdge> getArborescence() {
        return this.getArborescenceUtil();
    }

    @Override
    protected Comparator<Integer> getHeapComparator() {

        Comparator<Integer> icmp = new Comparator<Integer>() {

            @Override
            public int compare(Integer integer1, Integer integer2) {

                WeightedEdge edge1 = weightedEdges[integer1];
                WeightedEdge edge2 = weightedEdges[integer2];
                return cmp.compare(getAdjustedWeightedEdge(edge1), getAdjustedWeightedEdge(edge2));
            }
        };

        return icmp;
    }

    @Override
    protected Comparator<WeightedEdge> getMaxDisjointCmp() {

        Comparator<WeightedEdge> maxDisjointCmp = new Comparator<WeightedEdge>() {
            @Override
            public int compare(WeightedEdge edge1, WeightedEdge edge2) {
                return cmp.compare(getAdjustedWeightedEdge(edge1), getAdjustedWeightedEdge(edge2));
            }
        };
        return maxDisjointCmp;
    }

    @Override
    protected WeightedEdge getAdjustedWeightedEdge(WeightedEdge edge) {
        int w = this.getAdjustedWeight(edge);
        return new WeightedEdge(edge.getSrc(), edge.getDest(), w);
    }

    @Override
    protected int getAdjustedWeight(WeightedEdge edge) {
        int w = edge.getOriginalWeight() + this.stronglyConnected.findWeight(edge.getDest());
        return w;
    }

    @Override
    protected void computeReducedCost(List<Integer> contractionSet, Map<Integer, WeightedEdge> map, int maxW) {
        for (Integer node : contractionSet) {
            int incidentW = this.getAdjustedWeight(map.get(node));
            int reducedCost = maxW - incidentW;
            this.stronglyConnected.addWeight(node, reducedCost);
        }
    }
}

