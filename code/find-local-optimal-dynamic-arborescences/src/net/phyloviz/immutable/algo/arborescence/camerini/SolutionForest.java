package net.phyloviz.immutable.algo.arborescence.camerini;

import net.phyloviz.comparators.Comparators;
import net.phyloviz.graph.WeightedEdge;

import java.io.Serializable;
import java.util.*;

public class SolutionForest implements Serializable {

    /**
     * This class represents the camerini proposed in
     * A Note on Finding Optimum Branching, 1979 by: P.M. Camerini, L. Fratta, solutionForest. Maffioli
     * Some changes were made to the contraction phase namely the construction of camerini solutionForest
     */

    static final long serialVersionUID = 5417492L;

    public HashMap<WeightedEdge, EdgeNode> map; // map a WeightedEdge object to EdgeNode
    private EdgeNode[] pi; // pi array holds the leafs of the tree
    private Set<Integer> rset; // no entering edges
    private int[] maxArr; // hold the destination of the maximum weighted edge in a strongly connected component
    private int nNodes; // number of nodes
    private List<WeightedEdge> solution; // list of weighted edges resultant of processing the forest

    public Set<Integer> R;

    /**
     * Class constructor
     *
     * @param nNodes - number of nodes the size of the pi array
     * @param nEdges - number of edges that its the number of nodes
     */
    public SolutionForest(int nNodes, int nEdges) {

        this.map = new HashMap<>(nEdges);
        this.pi = new EdgeNode[nNodes];
        this.rset = new HashSet<>();
        this.maxArr = new int[nNodes];

        for (int i = 0; i < nNodes; i++) {
            this.maxArr[i] = i;
        }

        this.rset = new HashSet<>();
        this.nNodes = nNodes;
    }

    /**
     * Remove elements for rset
     */
    public void clearRset(){
        this.rset.clear();
    }

    /**
     * Reset max array
     */
    public void resetMaxArr(){
        for (int i = 0; i < this.nNodes; i++) {
            this.maxArr[i] = i;
        }
    }

    /**
     * Add new node
     * @param edge - edge
     * @return EdgeNode related with edge
     */
    public EdgeNode add(WeightedEdge edge) {

        if (this.map.containsKey(edge)) {
            EdgeNode n = this.map.get(edge);
            n.setInitialParent(null);
            n.setParent(null);
            return n;
        }

        EdgeNode node = new EdgeNode(edge);
        this.map.put(edge, node);
        return node;
    }

    /**
     * Add an entry to the pi array
     *
     * @param p    - position
     * @param node - EdgeNode
     */
    public void addPi(int p, EdgeNode node) {
        this.pi[p] = node;
    }

    /**
     * Getter for EdgeNode list
     *
     * @return List of EdgeNodes
     */
    public List<EdgeNode> getEdgeNodeList() {
        return new LinkedList<>(this.map.values());
    }

    /**
     * Getter for pi array
     *
     * @return Pi array
     */
    public EdgeNode[] getPi() {
        return this.pi;
    }

    /**
     * Get edgeNode
     *
     * @param edge - edge
     * @return EdgeNode
     */
    public EdgeNode getEdgeNode(WeightedEdge edge) {

      return this.map.get(edge);
    }

    /**
     * Add an element to rset
     *
     * @param root - element
     */
    public void addEntryToRset(int root) {
        this.rset.add(root);
    }


    /**
     * Set an entry to a given position of MaxArr
     *
     * @param position - position
     * @param entry    - entry
     */
    public void setMaxArrPosition(int position, int entry) {
        this.maxArr[position] = entry;
    }

    /**
     * Update Max array
     *
     * @param p1 - position 1
     * @param p2 - position 2
     */
    public void updateMaxArray(int p1, int p2) {

        this.maxArr[p1] = this.maxArr[p2];
    }

    /**
     * Reset the camerini forest
     * Worst-case(O(n))
     */
    public void resetSolutionForest() {
        // reset solution Camerini
        // whenever solution is used to build the MSA the attributes change
        for (EdgeNode node : this.map.values()) {
            node.setRemoveF(false);
            node.resetParent();
        }
    }

    /**
     * Compute set of Roots
     *
     * @return Set of roots
     */
    private Set<Integer> setR() {

        Set<Integer> R = new HashSet<>();
        for (Integer node : this.rset) {
            R.add(this.maxArr[node]);
        }
        return R;
    }

    /**
     * Remove node from edgeNodeList
     *
     * @param edgeNode - edgeNode object
     */
    public void removeNodeFromNodeList(EdgeNode edgeNode) {

        this.map.remove(edgeNode.getWeightedEdge());
    }

    /**
     * Remove node from edgeNodeList
     *
     * @param edge - edgeNode object
     */
    public void removeNodeFromNodeList(WeightedEdge edge) {

        this.map.remove(edge);
    }

    /**
     * Check if an Edge is in our final solution
     *
     * @param edge - edge object
     * @return true if edge is member of final solution, otherwise return false
     */
    public boolean edgeInForest(WeightedEdge edge) {
        return this.map.containsKey(edge);
    }

    /**
     * Find the root of camerini solutionForest
     *
     * @return Set of roots of attribute solutionForest
     */
    public LinkedList<EdgeNode> rootsF() {
        // store here the roots
        LinkedList<EdgeNode> N = new LinkedList<>();
        for (EdgeNode edgeNode : this.map.values()) {
            // edge node is a root if parent == null and isRemoved == false
            if (edgeNode.isRoot()) {
                N.add(edgeNode);
            }
        }

        return N;
    }

    /**
     * Given a edgeNode follow the parent pointer
     * until a root is found
     *
     * @param edgeNode       - edgeNode
     * @param edgeNodesRoots - List of edgeNode
     */
    private void removeFromRoots(EdgeNode edgeNode, List<EdgeNode> edgeNodesRoots) {
        // follow the parent
        for (; edgeNode != null; edgeNode = edgeNode.getParent()) {
            edgeNode.setRemoveF(true);
            for (EdgeNode childEdgeNode : edgeNode.getChildren()) {
                childEdgeNode.setParent(null);
                edgeNodesRoots.add(childEdgeNode);
            }
        }
    }

    /**
     * Implementation  of Algorithm LEAF described in Camerini
     *
     * @return List containing minimum spanning arborescence
     */

    public List<WeightedEdge> expansion() {

        Set<Integer> R = this.setR();
        this.R = R;
        List<WeightedEdge> B = new ArrayList<>(this.nNodes - 1);
        LinkedList<EdgeNode> N = this.rootsF();

        // process R
        for (Integer node : R) {
            if (this.pi[node] != null) {
                this.removeFromRoots(this.pi[node], N);
            }
        }

        // process N
        while (!N.isEmpty()) {
            EdgeNode edgeNode = N.pop();
            if (edgeNode.isRemoveF()) {
                continue;
            }

            WeightedEdge edge = edgeNode.getWeightedEdge();
            B.add(edge);
            int dst = edge.getDest();
            this.removeFromRoots(this.pi[dst], N);
        }

        this.solution = B;
        return B;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (EdgeNode edgeNode : this.map.values()  ) {
            builder.append(edgeNode.toString());
            builder.append("\n");
        }

        return builder.toString();
    }

    /**
     * Getter for solution attribute
     * @return List of weighed edges
     */
    public List<WeightedEdge> getSolution() {
        return this.solution;
    }

    /**
     * Decompose Camerini Forest
     * @param edge - edge where start the decomposition
     */
    public void decomposeTree(WeightedEdge edge) {

        EdgeNode edgeNode = this.getEdgeNode(edge);
        EdgeNode parent = edgeNode.getParent();

        while (parent != null) {

            this.removeNodeFromNodeList(parent);
            List<EdgeNode> children = parent.getChildren();
            for (EdgeNode child : children) {
                child.setInitialParent(null);
                child.setParent(null);
            }

            children.clear();
            parent = parent.getParent();
        }
    }

    /**
     * Getter for maxArr
     * @return maxArr
     */
    public int[] getMaxArr() {
        return this.maxArr;
    }

    /**
     * Print in a Transversal order the tree
     */
    public void printTreeTransversal() {

        Comparator<WeightedEdge> comparator = Comparators.totalOrderCmp();
        Comparator<EdgeNode> edgeNodeComparator = new Comparator<EdgeNode>() {
            @Override
            public int compare(EdgeNode o1, EdgeNode o2) {

                WeightedEdge e1 = o1.getWeightedEdge();
                WeightedEdge e2 = o2.getWeightedEdge();

                return comparator.compare(e1,e2);
            }
        };

        for (EdgeNode root: this.rootsF()) {

            List<List<EdgeNode>> transversal = this.printTreeTransversalUtil(root);

            for (List<EdgeNode> edgeNodes: transversal) {
                edgeNodes.sort(edgeNodeComparator);

                for (EdgeNode node: edgeNodes) {

                    if (node.getParent() == null) {
                        System.out.print(node.getWeightedEdge() + " (Parent: " + null + ") " + "\t");
                    }

                    else {

                        System.out.print(node.getWeightedEdge() + " (Parent: " + node.getParent().getWeightedEdge() + ") " + "\t");
                    }
                }

                System.out.println();
            }

            System.out.println("---------------------------------------------------");
        }
    }

    /**
     * Util function
     * @param root - root node
     * @return List<List<EdgeNode>>
     */
    private List<List<EdgeNode>> printTreeTransversalUtil(EdgeNode root){

        List<List<EdgeNode>> ans = new ArrayList<>();

        if(root == null)
            return ans;

        Queue<EdgeNode> q = new LinkedList<>();
        q.offer(root);

        while(!q.isEmpty()) {

            int size = q.size();
            List<EdgeNode> list = new ArrayList<>();

            for(int i = 0; i < size; i++) {

                EdgeNode node = q.poll();

                list.add(node);

                for(EdgeNode child : node.getChildren()) {
                    q.offer(child);
                }
            }

            ans.add(list);
        }

        return ans;
    }
}
