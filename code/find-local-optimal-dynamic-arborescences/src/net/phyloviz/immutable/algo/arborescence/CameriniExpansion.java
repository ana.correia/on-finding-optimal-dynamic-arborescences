package net.phyloviz.immutable.algo.arborescence;

import net.phyloviz.Arborescence;
import net.phyloviz.immutable.algo.arborescence.camerini.EdgeNode;
import net.phyloviz.immutable.algo.arborescence.camerini.SolutionForest;
import net.phyloviz.util.ArboEnum;
import net.phyloviz.graph.DirectedGraph;
import net.phyloviz.graph.WeightedEdge;
import net.phyloviz.immutable.queue.Heap;
import net.phyloviz.immutable.queue.HeapType;

import java.util.*;

public class CameriniExpansion extends EdmondsFastReduction implements Arborescence {

    /**
     * This class performs the expansion phase as described
     * A Note on Finding Optimum Branching, 1979 by: P.M. Camerini, L. Fratta, solutionForest. Maffioli
     * Some changes were made to the contraction phase namely the construction of camerini solutionForest
     */

    protected List<List<EdgeNode>> edgeNodeCycle;
    protected SolutionForest solutionForest;
    protected EdgeNode[] inEdgeNode;

    public CameriniExpansion(DirectedGraph G, Comparator<WeightedEdge> cmp, SolutionForest solutionForest) {
        super(G, cmp);
        this.solutionForest = solutionForest;
        this.edgeNodeCycle = new ArrayList<>(this.G.size());
        this.inEdgeNode = new EdgeNode[this.G.size()];

        for (Integer node : this.G.getNodeSet()) {
            this.edgeNodeCycle.add(node, null);
        }
    }


    public CameriniExpansion(DirectedGraph G, Comparator<WeightedEdge> cmp, HeapType heapType, boolean smartStorage, boolean floydHeapConstruction, ArboEnum arboEnum) {
        super(G, cmp, heapType, smartStorage, floydHeapConstruction, arboEnum);
        this.initializerUtil();
    }

    public CameriniExpansion(DirectedGraph G, Comparator<WeightedEdge> cmp, HeapType heapType, boolean smartStorage, ArboEnum arboEnum) {
        super(G, cmp, heapType, smartStorage, arboEnum);
        this.initializerUtil();
    }

    /**
     * Util function which initializes all the structures
     */
    private void initializerUtil() {

        this.edgeNodeCycle = new ArrayList<>(this.G.size());
        this.inEdgeNode = new EdgeNode[this.G.size()];
        this.solutionForest = new SolutionForest(this.G.size(), this.G.nEdges());

        Set<Integer> nodeSet = this.G.getNodeSet();
        for (Integer node : nodeSet) {
            this.solutionForest.setMaxArrPosition(node, node);
            this.edgeNodeCycle.add(node, null);
        }
    }

    @Override
    public List<WeightedEdge> getArborescence() {
        return this.getArborescenceUtil();
    }

    @Override
    @SuppressWarnings("Duplicates")
    protected List<WeightedEdge> getArborescenceUtil() {

        long startContraction = System.currentTimeMillis();
        while (!this.roots.isEmpty()) {
            int root = this.roots.pop();
            long popStart = System.currentTimeMillis();
            // evaluate next root
            Heap<Integer> PQ = this.queues[root];
            if (PQ.isEmpty()) {
                this.solutionForest.addEntryToRset(root);
                continue;
            }

            // popping self loops
            int minPos = PQ.pop();
            WeightedEdge min = this.weightedEdges[minPos];
            while (PQ.size() > 0 && this.stronglyConnected.sameSet(min.getSrc(), min.getDest())) {
                minPos = PQ.pop();
                min = this.weightedEdges[minPos];
            }

            long popEnd = System.currentTimeMillis();
            this.popMinOperations += (popEnd - popStart);
            int u = min.getSrc();
            int v = min.getDest();

            // this component does not have any edge that is not a self loop so we must check another component
            if (this.stronglyConnected.sameSet(u, v)) {
                this.solutionForest.addEntryToRset(root);
                continue;
            }

            // adding a critical edge to camerini solutionForest
            EdgeNode minEdgeNode = this.solutionForest.add(min);

            // if a component is
            if (this.edgeNodeCycle.get(root) == null) {
                this.solutionForest.addPi(root, minEdgeNode);
            } else {
                for (EdgeNode node : this.edgeNodeCycle.get(root)) {
                    node.setParent(minEdgeNode);
                    minEdgeNode.addChild(node);
                }
            }

            // check if edge min is safe to add to the arborescence
            if (this.weaklyConnected.findSet(u) != this.weaklyConnected.findSet(v)) {
                this.in[root] = min;
                this.inEdgeNode[root] = minEdgeNode;
                this.weaklyConnected.unionSet(u, v);
            } else {

                // we have a cycle lets process it
                // store edges in the cycle
                HashSet<WeightedEdge> cycleEdges = new HashSet<>();
                cycleEdges.add(min);
                List<EdgeNode> edgeNodeCycle = new ArrayList<>();
                edgeNodeCycle.add(minEdgeNode);

                // store nodes in the cycle
                List<Integer> contractionSet = new ArrayList<>();

                contractionSet.add(this.stronglyConnected.findSet(v));
                this.in[root] = null;
                this.inEdgeNode[root] = null;

                // find the cycle
                HashMap<Integer, WeightedEdge> map = new HashMap<>();
                map.put(this.stronglyConnected.findSet(v), min);

                long startFindCycle = System.currentTimeMillis();

                for (int i = this.stronglyConnected.findSet(min.getSrc());
                     this.in[i] != null;
                     i = this.stronglyConnected.findSet(this.in[i].getSrc())) {
                    map.put(i, this.in[i]);
                    cycleEdges.add(this.in[i]);
                    edgeNodeCycle.add(this.inEdgeNode[i]);
                    contractionSet.add(i);
                }

                long endFindCycle = System.currentTimeMillis();
                this.findCycleOperations += (endFindCycle - startFindCycle);

                // find maximum adjustedWeights edge
                WeightedEdge maxEdgeCycle = Collections.max(cycleEdges, this.maxDisjointCmp);
                int dst = stronglyConnected.findSet(maxEdgeCycle.getDest());
                int maxW = this.getAdjustedWeight(maxEdgeCycle);

                // compute reduced costs
                long startReduced = System.currentTimeMillis();
                this.computeReducedCost(contractionSet, map, maxW);
                long endReduced = System.currentTimeMillis();
                this.reducedCostOperations += (endReduced - startReduced);

                // perform unionSet
                for (WeightedEdge weightedEdge : cycleEdges) {
                    this.stronglyConnected.unionSet(weightedEdge.getSrc(), weightedEdge.getDest());
                }

                // find cycle representative
                int rep = this.stronglyConnected.findSet(maxEdgeCycle.getDest());

                // the representative will be the next node to be evaluated
                this.roots.add(0, rep);
                Heap<Integer> heap = this.queues[rep];
                long unionStart = System.currentTimeMillis();
                // perform heap union
                for (Integer node : contractionSet) {
                    if (rep != node) {
                        heap.union(this.queues[node]);
                        this.nUnions++;
                    }
                }

                long unionEnd = System.currentTimeMillis();
                this.unionOperations += (unionEnd - unionStart);

                this.solutionForest.updateMaxArray(rep, dst);
                this.edgeNodeCycle.set(rep, edgeNodeCycle);
            }
        }

        long endContraction = System.currentTimeMillis();

        long solStart = System.currentTimeMillis();
        List<WeightedEdge> solution = this.expansion();
        long solEnd = System.currentTimeMillis();

        this.benchMark = new BenchMark(this.unionOperations,
                this.popMinOperations,
                this.reducedCostOperations,
                this.findCycleOperations,
                this.nReductions,
                this.nCycles,
                this.nUnions,
                endContraction - startContraction,
                solEnd - solStart,
                this.historic);

        return solution;
    }

    /**
     * Algorithm LEAF described in Camerini
     *
     * @return List containing minimum spanning arborescence
     */
    @Override
    public List<WeightedEdge> expansion() {

        return this.solutionForest.expansion();
    }

    public SolutionForest getSolution() {
        return this.solutionForest;
    }
}
