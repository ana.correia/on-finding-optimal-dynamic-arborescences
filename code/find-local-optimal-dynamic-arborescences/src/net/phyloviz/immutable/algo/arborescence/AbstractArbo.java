package net.phyloviz.immutable.algo.arborescence;

import net.phyloviz.immutable.queue.Heap;
import net.phyloviz.graph.DirectedGraph;
import net.phyloviz.graph.WeightedEdge;
import net.phyloviz.immutable.queue.BinaryHeap;
import net.phyloviz.immutable.queue.BinomialHeap;
import net.phyloviz.immutable.queue.HeapType;
import net.phyloviz.util.*;

import java.util.*;

abstract class AbstractArbo {

    protected DirectedGraph G;
    protected Comparator<WeightedEdge> cmp;
    protected Comparator<Integer> icmp;
    protected Heap<Integer>[] queues;
    protected WeightedEdge[] in;
    protected WeightedEdge[] weightedEdges;

    protected LinkedList<Integer> roots;

    protected DisjointSet weaklyConnected;
    protected ArboDisjointSet stronglyConnected;

    protected List<Set<WeightedEdge>> B;
    protected List<Set<WeightedEdge>> Q;
    protected List<WeightedEdge> QMax;
    protected List<ArboDisjointSet> stronglyConnectSets;
    protected int level;
    protected List<Integer> contractedNodes;

    protected Comparator<WeightedEdge> maxDisjointCmp;
    protected boolean smartStorage;
    protected boolean floydHeapConstruction;

    protected long unionOperations;
    protected long popMinOperations;
    protected long reducedCostOperations;
    protected long findCycleOperations;
    protected long nReductions;
    protected long nCycles;
    protected long nUnions;
    protected BenchMark benchMark;
    protected long historic;

    @SuppressWarnings("unchecked")
    public AbstractArbo(DirectedGraph G, Comparator<WeightedEdge> cmp) {

        this.G = G;
        this.G.reverseGraph();

        this.cmp = cmp;
        this.icmp = this.getHeapComparator();
        this.maxDisjointCmp = this.getMaxDisjointCmp();

        this.in = new WeightedEdge[this.G.size()];
        this.weightedEdges = new WeightedEdge[this.G.nEdges()];

        this.stronglyConnected = new FasterArboDisjointSet(this.G.size());
        this.weaklyConnected = new DisjointSet(this.G.size());
        this.queues = new Heap[this.G.size()];

        List<WeightedEdge> allEdges = this.G.getAllEdges();

        int i = 0;
        for (WeightedEdge edge : allEdges) {
            WeightedEdge newEdge = new WeightedEdge(edge);
            this.weightedEdges[i] = newEdge;
            i++;
        }

        Set<Integer> nodeSet = this.G.getNodeSet();
        this.instantiateHeapObjectUtil(nodeSet, HeapType.BINOMIAL);
    }

    public AbstractArbo(DirectedGraph G, Comparator<WeightedEdge> cmp, HeapType heapType, boolean smartStorage, ArboEnum arboEnum) {

        this(G, cmp, heapType, smartStorage, false, arboEnum);
    }


    /**
     * Class Constructor
     *
     * @param G        - Input graph
     * @param cmp      - Edge comparator object
     * @param heapType - Heap type Binary, Binomial
     */
    @SuppressWarnings("unchecked")
    public AbstractArbo(DirectedGraph G, Comparator<WeightedEdge> cmp, HeapType heapType, boolean smartStorage, boolean floydHeapConstruction, ArboEnum arboEnum) {

        this.G = G;
        this.G.reverseGraph();

        this.cmp = cmp;
        this.smartStorage = smartStorage;
        this.queues = new Heap[this.G.size()];
        this.in = new WeightedEdge[this.G.size()];
        this.weaklyConnected = new DisjointSet(this.G.size());
        this.floydHeapConstruction = floydHeapConstruction;
        this.instantiateDisjointSet(arboEnum);

        this.icmp = this.getHeapComparator();
        this.maxDisjointCmp = this.getMaxDisjointCmp();

        this.B = new ArrayList<>(this.G.size());
        this.Q = new ArrayList<>(this.G.size());
        this.QMax = new ArrayList<>(this.G.size());
        this.stronglyConnectSets = new ArrayList<>(this.G.size());
        this.stronglyConnectSets.add(this.stronglyConnected.clone());
        this.contractedNodes = new ArrayList<>(this.G.size());
        this.level = 0;

        Set<Integer> nodeSet = this.G.getNodeSet();
        this.roots = new LinkedList<>(nodeSet);

        this.instantiateHeapObjectUtil(nodeSet, heapType);
        this.edgeStore(nodeSet);
        this.initBenchmarkInfo();
    }

    private void instantiateDisjointSet(ArboEnum arboEnum) {

        switch (arboEnum) {
            case FASTER_ARBO_DISJOINT_SET:
                this.stronglyConnected = new FasterArboDisjointSet(this.G.size());
                break;
            case LINEAR_ARBO_DISJOINT_SET:
                this.stronglyConnected = new LinearArboDisjointSet(this.G.size());
                break;
            default:
                throw new IllegalStateException("Invalid Arbo disjoint set type");
        }
    }

    // abstract methods
    protected abstract Comparator<Integer> getHeapComparator();

    protected abstract Comparator<WeightedEdge> getMaxDisjointCmp();

    protected abstract WeightedEdge getAdjustedWeightedEdge(WeightedEdge edge);

    protected abstract int getAdjustedWeight(WeightedEdge edge);

    protected abstract void computeReducedCost(List<Integer> contractionSet, Map<Integer, WeightedEdge> map, int maxW);

    /**
     * Instantiate heap object
     *
     * @param nodeSet  - node set
     * @param heapType - selected heap
     */
    private void instantiateHeapObjectUtil(Set<Integer> nodeSet, HeapType heapType) {
        // initialize priority queue
        for (Integer node : nodeSet) {
            this.in[node] = null;
            switch (heapType) {
                case BINOMIAL:
                    this.queues[node] = new BinomialHeap(this.icmp);
                    break;
                case BINARY:
                    int n = this.G.getNeighbors(node).size();
                    if (n == 0) {
                        n++;
                    }
                    this.queues[node] = new BinaryHeap(n, this.icmp, this.floydHeapConstruction);
                    break;
                case FAST_RANK_RELAXED:
                    throw new UnsupportedOperationException("Fast Ranked Relaxed does not support union yet");
                default:
                    throw new IllegalStateException("Invalid Heap type");
            }
        }
    }

    private void edgeStore(Set<Integer> nodeSet) {

        if (!this.smartStorage) {
            this.G.reverseGraph();
        }

        this.weightedEdges = new WeightedEdge[this.G.nEdges()];
        int key = 0;

        for (Integer node : nodeSet) {

            List<WeightedEdge> weightedEdges = this.G.getNeighbors(node);

            for (WeightedEdge edge : weightedEdges) {

                this.weightedEdges[key] = new WeightedEdge(edge);
                this.queues[edge.getDest()].push(key);
                key++;
            }

        }
    }

    private void initBenchmarkInfo() {

        this.unionOperations = 0;
        this.popMinOperations = 0;
        this.reducedCostOperations = 0;
        this.findCycleOperations = 0;
        this.historic = 0;

        this.nReductions = 0;
        this.nCycles = 0;
        this.nUnions = 0;
    }


    @SuppressWarnings("Duplicates")
    protected List<WeightedEdge> getArborescenceUtil() {

        // contraction phase
        // store the current branching
        LinkedList<WeightedEdge> branching = new LinkedList<>();
        long startContraction = System.currentTimeMillis();
        while (!this.roots.isEmpty()) {

            int root = this.roots.pop();

            long popStart = System.currentTimeMillis();
            // evaluate next root
            Heap<Integer> PQ = this.queues[root];
            if (PQ.isEmpty()) {
                continue;
            }

            // popping self loops
            int minPos = PQ.pop();
            WeightedEdge min = this.weightedEdges[minPos];
            this.weightedEdges[minPos] = null;
            while (PQ.size() > 0 && this.stronglyConnected.sameSet(min.getSrc(), min.getDest())) {
                minPos = PQ.pop();
                min = this.weightedEdges[minPos];
                this.weightedEdges[minPos] = null;
            }

            long popEnd = System.currentTimeMillis();
            this.popMinOperations += (popEnd - popStart);
            int u = min.getSrc();
            int v = min.getDest();

            // this component does not have any edge that is not a self loop so we must check another component
            if (this.stronglyConnected.sameSet(u, v)) {
                continue;
            }

            // check if edge min is safe to add to the arborescence
            if (this.weaklyConnected.findSet(u) != this.weaklyConnected.findSet(v)) {
                this.in[root] = min;
                branching.add(min);
                this.weaklyConnected.unionSet(u, v);
            } else {

                this.nCycles++;
                // we have a cycle lets process it
                // store edges in the cycle
                HashSet<WeightedEdge> cycleEdges = new HashSet<>();
                cycleEdges.add(min);

                // store nodes in the cycle
                List<Integer> contractionSet = new ArrayList<>();

                contractionSet.add(this.stronglyConnected.findSet(v));
                this.in[root] = null;

                // find the cycle
                HashMap<Integer, WeightedEdge> map = new HashMap<>();
                map.put(this.stronglyConnected.findSet(v), min);

                long startFindCycle = System.currentTimeMillis();

                for (int i = this.stronglyConnected.findSet(min.getSrc());
                     this.in[i] != null;
                     i = this.stronglyConnected.findSet(this.in[i].getSrc())) {

                    map.put(i, this.in[i]);
                    cycleEdges.add(this.in[i]);
                    contractionSet.add(i);
                }

                long endFindCycle = System.currentTimeMillis();
                this.findCycleOperations += (endFindCycle - startFindCycle);

                // store the cycle
                this.Q.add(new HashSet<>(cycleEdges));

                // find maximum adjustedWeights edge
                WeightedEdge maxEdgeCycle = Collections.max(cycleEdges, this.maxDisjointCmp);
                int maxW = this.getAdjustedWeight(maxEdgeCycle);

                // compute reduced costs
                long startReduced = System.currentTimeMillis();
                this.computeReducedCost(contractionSet, map, maxW);
                long endReduced = System.currentTimeMillis();
                this.reducedCostOperations += (endReduced - startReduced);

                // perform unionSet
                for (WeightedEdge weightedEdge : cycleEdges) {

                    this.stronglyConnected.unionSet(weightedEdge.getSrc(), weightedEdge.getDest());
                }

                // find cycle representative
                int rep = this.stronglyConnected.findSet(maxEdgeCycle.getSrc());

                // the representative will be the next node to be evaluated
                roots.add(0, rep);
                Heap heap = this.queues[rep];

                long unionStart = System.currentTimeMillis();
                // perform heap unionSet
                for (Integer node : contractionSet) {
                    if (rep != node) {
                        heap.union(this.queues[node]);
                        this.queues[node] = null;
                        this.nUnions++;
                    }
                }

                long unionEnd = System.currentTimeMillis();
                this.unionOperations += (unionEnd - unionStart);
                long startHistoric = System.currentTimeMillis();
                // store maximum adjustedWeights edge
                this.QMax.add(maxEdgeCycle);
                // add to the contracted nodes the cycle representative
                this.contractedNodes.add(rep);
                // store disjoint set data structure
                this.stronglyConnectSets.add(this.stronglyConnected.clone());
                // store current branching
                this.B.add(new HashSet<>(branching));
                // clear the branching for the next iteration
                branching.removeAll(cycleEdges);
                long endHistoric = System.currentTimeMillis();
                this.historic += (endHistoric - startHistoric);
                // increment contraction level
                this.level++;
            }
        }

        long endContraction = System.currentTimeMillis();
        this.B.add(new HashSet<>(branching));
        long solStart = System.currentTimeMillis();
        List<WeightedEdge> solution = this.expansion();
        long solEnd = System.currentTimeMillis();

        this.benchMark = new BenchMark(this.unionOperations,
                this.popMinOperations,
                this.reducedCostOperations,
                this.findCycleOperations,
                this.nReductions,
                this.nCycles,
                this.nUnions,
                endContraction - startContraction,
                solEnd - solStart,
                this.historic);

        return solution;
    }


    /**
     * This function handles the expansion phase of the algorithm
     *
     * @return minimum spanning arborescence
     */
    @SuppressWarnings("Duplicates")
    public List<WeightedEdge> expansion() {

        List<WeightedEdge> edges = new ArrayList<>(this.B.get(this.level));
        while (this.level > 0) {

            this.level--;

            int mergedNode = this.contractedNodes.get(this.level);
            Set<WeightedEdge> circuit = this.Q.get(this.level);

            ArboDisjointSet uf = this.stronglyConnectSets.get(this.level + 1);
            boolean isRoot = this.isRoot(edges, mergedNode, uf);

            if (isRoot) {
                edges.addAll(circuit);
                WeightedEdge max = this.QMax.get(this.level);
                edges.remove(max);

            } else {

                ArboDisjointSet _uf = this.stronglyConnectSets.get(this.level);
                HashSet<Integer> endpoints = new HashSet<>();

                for (WeightedEdge edge : edges) {
                    int dst = _uf.findSet(edge.getDest());
                    endpoints.add(dst);
                }

                for (WeightedEdge cycleEdge : circuit) {

                    int dst = _uf.findSet(cycleEdge.getDest());

                    if (!endpoints.contains(dst)) {
                        edges.add(cycleEdge);
                    }
                }
            }
        }

        List<WeightedEdge> sol = new ArrayList<>(edges.size());
        for (WeightedEdge edge : edges) {
            edge.setWeight(edge.getOriginalWeight());
            sol.add(edge);
        }
        return sol;
    }

    /**
     * Check if mergedNode is root or not
     *
     * @param edges      - partial solution
     * @param mergedNode - super node
     * @param uf         - binaryHeapUnion find
     * @return true if merged node is root, false otherwise
     */
    @SuppressWarnings("Duplicates")
    private boolean isRoot(List<WeightedEdge> edges, int mergedNode, ArboDisjointSet uf) {
        boolean result = true;

        for (WeightedEdge weightedEdge : edges) {

            int dst = weightedEdge.getDest();
            dst = uf.findSet(dst);

            if (dst == mergedNode) {
                result = false;
                break;
            }
        }

        return result;
    }

    /**
     * Calculate arborescence weight
     *
     * @param lst - List with arborescence
     * @return arborescene weight
     */
    public int computeWeight(List<WeightedEdge> lst) {

        int w = 0;
        for (WeightedEdge weightedEdge : lst) {
            w += weightedEdge.getWeight();
        }

        return w;
    }

    /**
     * Return benchmark
     *
     * @return benchmark
     */
    public BenchMark getBenchMark() {
        return this.benchMark;
    }
}
