package net.phyloviz.immutable.algo.arborescence;

import net.phyloviz.Arborescence;
import net.phyloviz.graph.DirectedGraph;
import net.phyloviz.graph.WeightedEdge;
import net.phyloviz.util.DisjointSet;
import net.phyloviz.immutable.algo.cycles.DynamicDirectedCycle;

import java.util.*;

public class OriginalImpl implements Arborescence {

    /*
    * Edmond's algorithm implementation based On Finding Optimum Branching 1966 by Jack Edmond's
    * Here we try to find the minimum spanning arborescence instead the maximum has described on the
    * original paper.
    *
    * The main differences are:
    * - Instead of looking for the maximum weight edge incident on each node we find the minimum one
    * - When computing the reduced costs instead of using the weight of the lightest edge on the cycle
    * we use the heaviest
    * */

    private DirectedGraph graph;
    private Comparator<WeightedEdge> cmp;
    private int MAX_ID;

    // buckets described in the paper
    private List<DirectedGraph> G;
    private List<List<Integer>> D;
    private List<List<WeightedEdge>> E;
    private List<List<WeightedEdge>> B;
    private List<List<WeightedEdge>> Q;
    // store here maximum weighted edge in each cycle
    private List<WeightedEdge> QMax;
    // store the ID of the contracted nodes
    private List<Integer> contractedNodes;
    // used to keep track of the number of contractions and get the graphs
    private int level;


    public OriginalImpl(DirectedGraph G, Comparator<WeightedEdge> cmp) {

        this.graph = G;
        this.cmp = cmp;
        int size = this.graph.size();
        this.MAX_ID = size;
        this.level = 0;

        this.G = new ArrayList<>(size);
        this.D = new ArrayList<>(size);
        this.E = new ArrayList<>(size);
        this.B = new ArrayList<>(size);
        this.Q = new ArrayList<>(size);
        this.QMax = new ArrayList<>(size);
        this.contractedNodes = new ArrayList<>(size);

        // perform a copy of the original graph
        DirectedGraph copy = new DirectedGraph(this.graph);
        copy.reverseGraph();
        this.G.add(copy);
    }

    /**
     * Perform the contraction phase of the algorithm
     */
    private void contraction() {

        while (true) {

            DirectedGraph current = this.G.get(this.level);
            int currentSize = current.size();
            List<Integer> D = new ArrayList<>(currentSize);
            List<WeightedEdge> E = new ArrayList<>(currentSize);
            List<WeightedEdge> B = new ArrayList<>(currentSize);
            Set<Integer> nodeSet = current.getNodeSet();
            DisjointSet uf = new DisjointSet(this.MAX_ID);

            // for every node find minimum incident edge
            for (Integer n : nodeSet) {

                // save visited node
                D.add(n);

                WeightedEdge min = this.getIncidentEdgeOnNode(current, n);

                // the graph is only a super-node end the algorithm usually happens when graph is strongly connected
                if (min == null && D.size() == currentSize) {
                    this.B.add(B);
                    this.E.add(E);
                    this.D.add(D);
                    return;
                }

                // no minimum incident edge on node n get next node
                if (min == null)
                    continue;

                int src = min.getSrc();
                int dst = min.getDest();

                // B stores partial solutions and E stores all edges incident in nodes before cycle
                // Check if the addition of min forms a cycle
                if (uf.sameSet(src, dst)) {
                    // the addition of this edge will make a cycle
                    // E stores all edges
                    E.add(new WeightedEdge(min));
                    break;

                } else {
                    // add weighted edge to B
                    B.add(new WeightedEdge(min));
                    // add weightedEdge to E
                    E.add(new WeightedEdge(min));
                    uf.unionSet(src, dst);
                }

                // stopping condition for graphs that are not strongly connected
                if (D.size() == currentSize) {
                    this.B.add(B);
                    this.E.add(E);
                    this.D.add(D);
                    return;
                }
            }

            // store partial branching
            this.B.add(B);
            // store all min weight edges
            this.E.add(E);
            // store visited vertices
            this.D.add(D);

            // find the cycle
            List<WeightedEdge> cycle = this.getCycle(E);
            WeightedEdge maxCycleEdge = Collections.max(cycle, this.cmp);

            // add store cycle
            this.Q.add(cycle);

            // store min edge in cycle
            this.QMax.add(maxCycleEdge);

            DirectedGraph contractGraph = this.contractGraph(current, cycle, maxCycleEdge);
            this.G.add(contractGraph);

            // depth or contraction level increment
            this.level++;
        }
    }

    /**
     * Perform the expansion phase of the algorithm
     * @return
     */
    private List<WeightedEdge> expansion() {

        // here we will store the solution
        HashSet<WeightedEdge> edges = new HashSet<>(this.B.get(this.level));

        while (this.level > 0) {

            this.level--;
            List<WeightedEdge> circuit = this.Q.get(this.level);
            int mergedNode = this.contractedNodes.get(this.level);

            // check if merged node is a root
            Boolean isRoot = this.rootCheck(mergedNode, edges);

            // convert the current solution to the previous mapping
            HashSet<WeightedEdge> tmp = new HashSet<>();
            for (WeightedEdge weightedEdge : edges) {
                if (weightedEdge.getParent() != null) {
                    tmp.add(weightedEdge.getParent());
                } else {
                    tmp.add(weightedEdge);
                }
            }

            edges = tmp;

            if (isRoot) {
                // add all edges in the cycle unless the max
                WeightedEdge max = this.QMax.get(this.level);
                circuit.remove(max);
                edges.addAll(circuit);

            } else {

                // determine which edges from the cycle are safe to edge
                HashSet<Integer> endpoint = new HashSet<>();
                for (WeightedEdge edge: edges) {
                    endpoint.add(edge.getDest());
                }

                for (WeightedEdge edge: circuit) {
                    if (!endpoint.contains(edge.getDest())) {
                        edges.add(edge);
                    }
                }
            }
        }

        // obtain the final solution
        return new ArrayList<>(edges);
    }

    /**
     * Check if the merged node is a root considering the current
     * @param mergedNode - new super-vertex
     * @param edges - current solution
     * @return true if the merged node is root of the current solution, false otherwise
     */
    private Boolean rootCheck(int mergedNode, HashSet<WeightedEdge> edges) {

        boolean r = true;

        for (WeightedEdge w : edges) {

            if (mergedNode == w.getDest()) {
                r = false;
                break;
            }
        }

        return r;
    }

    /**
     * Compute the weight of the arborescence
     *
     * @param edges - list of edges
     * @return weight of the arborescence
     */
    public int computeWeight(List<WeightedEdge> edges) {

        int res = 0;

        for (WeightedEdge w : edges) {

            res = res + w.getWeight();
        }

        return res;
    }

    @Override
    public BenchMark getBenchMark() {
        throw new UnsupportedOperationException();
    }

    /**
     * Follow the parent attribute until no more parent
     *
     * @param edge - edge
     * @return - maximum level parent
     */
    private WeightedEdge getOriginal(WeightedEdge edge) {

        while (edge.getParent() != null) {

            edge = edge.getParent();
        }

        return edge;
    }

    /**
     * Build the contracted graph
     *
     * @param current      -  current graph
     * @param cycle        - cycle in current graph
     * @param maxEdgeCycle - maximum weight edge on cycle
     * @return Graph with contracted cycle
     */
    private DirectedGraph contractGraph(DirectedGraph current, List<WeightedEdge> cycle, WeightedEdge maxEdgeCycle) {

        DirectedGraph contractedGraph = new DirectedGraph();
        contractedGraph.addNode(MAX_ID);

        this.contractedNodes.add(MAX_ID);

        HashMap<Integer, WeightedEdge> incidentCycleMap = new HashMap<>();
        Set<Integer> contractionSet = new HashSet<>();

        for (WeightedEdge weightedEdge : cycle) {
            int dst = weightedEdge.getDest();
            int src = weightedEdge.getSrc();

            contractionSet.add(dst);
            contractionSet.add(src);
            incidentCycleMap.put(dst, weightedEdge);
        }

        // minimum edge weight
        int maxW = maxEdgeCycle.getWeight();

        List<WeightedEdge> allEdges = current.getAllEdges();

        for (WeightedEdge edge : allEdges) {

            int src = edge.getSrc();
            int dst = edge.getDest();
            int w = edge.getWeight();

            // if the edge inside the cycle does not belong to the contracted graph
            if (contractionSet.contains(src) && contractionSet.contains(dst)) {
                continue;
            }

            // edge pointing outside the cycle
            else if (contractionSet.contains(src) && !contractionSet.contains(dst)) {
                WeightedEdge weightedEdge = new WeightedEdge(MAX_ID, dst, w);
                weightedEdge.setParent(edge);
                contractedGraph.addEdge(weightedEdge);
            }

            // edge pointing at the cycle
            else if (!contractionSet.contains(src) && contractionSet.contains(dst)) {

                // find the reduced costs
                WeightedEdge edgeIncidentOnDst = incidentCycleMap.get(dst);
                int incidentOnDstWeight = edgeIncidentOnDst.getWeight();
                int reducedCost = w +  maxW - incidentOnDstWeight;

                WeightedEdge weightedEdge = new WeightedEdge(src, MAX_ID, reducedCost);
                weightedEdge.setParent(edge);
                contractedGraph.addEdge(weightedEdge);

                // edge has nothing to do with the cycle
            } else if (!contractionSet.contains(src) && !contractionSet.contains(dst)) {

                WeightedEdge weightedEdge = new WeightedEdge(src, dst, w);
                weightedEdge.setParent(edge);
                contractedGraph.addEdge(weightedEdge);
            }
        }

        // finally reverse the graph
        contractedGraph.reverseGraph();

        // increment ID for new nodes
        this.MAX_ID++;
        return contractedGraph;
    }

    /**
     * Compute the cycle present in weightedEdges list
     *
     * @param weightedEdges - weightedEdges list
     * @return List of weighted edges
     */
    private List<WeightedEdge> getCycle(List<WeightedEdge> weightedEdges) {

        DirectedGraph cycleGraph = new DirectedGraph();

        for (WeightedEdge weightedEdge : weightedEdges) {

            cycleGraph.addEdge(weightedEdge);
        }

        DynamicDirectedCycle dynamicDirectedCycle = new DynamicDirectedCycle(cycleGraph);
        Stack<Integer> cycle = dynamicDirectedCycle.cycle();

        List<WeightedEdge> circuit = new LinkedList<>();

        while (cycle.size() > 1) {

            int u = cycle.pop();
            int v = cycle.peek();

            for (WeightedEdge weightedEdge : weightedEdges) {

                if (u == weightedEdge.getSrc() && v == weightedEdge.getDest()) {
                    circuit.add(weightedEdge);
                }
            }
        }

        return circuit;
    }

    /**
     * Find minimum edge incident in a given node
     *
     * @param current - current graph
     * @param n       - node
     * @return minimum edge incident on n or null if n does not have incident edges
     */
    private WeightedEdge getIncidentEdgeOnNode(DirectedGraph current, Integer n) {

        List<WeightedEdge> weightedEdges = current.getNeighbors(n);

        if (weightedEdges.size() > 0) {
            return Collections.min(current.getNeighbors(n), this.cmp);
        } else {
            return null;
        }
    }

    @Override
    public List<WeightedEdge> getArborescence() {
        this.contraction();
        return this.expansion();
    }
}
