package net.phyloviz.immutable.algo.arborescence.camerini;

import net.phyloviz.graph.WeightedEdge;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class EdgeNode implements Serializable {

    /**
     * This class represents a node of the camerini
     * A Note on Finding Optimum Branching, 1979 by: P.M. Camerini, L. Fratta, solutionForest. Maffioli
     * Some changes were made to the contraction phase namely the construction of camerini solutionForest
     */

    private WeightedEdge edge;
    private EdgeNode parent;
    private List<EdgeNode> children;
    private boolean removeF;

    // reset the tree
    private EdgeNode initialParent;
    static final long serialVersionUID = 4252848494L;

    /**
     * Class constructor
     *
     * @param edge - WeightedEdge object
     */
    public EdgeNode(WeightedEdge edge) {
        this.edge = edge;
        this.removeF = false;
        this.parent = null;
        this.initialParent = null;
        this.children = new LinkedList<>();
    }

    /**
     * Add a single EdgeNode as a child of the current
     * EdgeNode
     *
     * @param edgeNode - edgeNode to be added
     */
    public void addChild(EdgeNode edgeNode) {
        this.children.add(edgeNode);
    }

    /**
     * Setter for the parent attribute
     *
     * @param parent - parent of current EdgeNode
     */
    public void setParent(EdgeNode parent) {

        if (this.initialParent == null) {
            this.initialParent = parent;
        }

        this.parent = parent;
    }

    public void setInitialParent(EdgeNode initialParent) {
        this.initialParent = initialParent;
    }

    /**
     * Set parent attribute to the initialParent
     */
    public void resetParent() {
        this.parent = this.initialParent;
    }

    /**
     * Check if a given node is a root
     *
     * @return true if EdgeNode does not have a parent, false otherwise
     */
    public boolean isRoot() {
        return this.parent == null;
    }

    /**
     * Return edge of EdgeNode
     *
     * @return Weighted Edge
     */
    public WeightedEdge getWeightedEdge() {
        return this.edge;
    }

    /**
     * WeightedEdge setter
     * @param edge - Weighted Edge object
     */
    public void setWeightedEdge(WeightedEdge edge) {
        this.edge = edge;
    }

    /**
     * Getter for parent attribute
     *
     * @return parent of current node
     */
    public EdgeNode getParent() {
        return this.parent;
    }

    /**
     * Getter for children
     *
     * @return List of child
     */
    public List<EdgeNode> getChildren() {
        return this.children;
    }

    /**
     * Set parameter removeF
     *
     * @param removeF - boolean
     */
    public void setRemoveF(boolean removeF) {
        this.removeF = removeF;
    }

    /**
     * Getter for removedF
     *
     * @return boolean removedF
     */
    public boolean isRemoveF() {
        return this.removeF;
    }

    /**
     * Clear children
     */
    public void clearChildren() {
        this.children.clear();
    }

    /**
     * Override toString method
     *
     * @return String
     */
    @Override
    public String toString() {

        String nodeID = this.getNodeStr(this);
        List<String> childrenID = new ArrayList<>(this.children.size());

        for (EdgeNode edgeNode : this.children) {
            childrenID.add(this.getNodeStr(edgeNode));
        }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("NodeID: ").append(nodeID);
        if (this.parent != null) {
            stringBuilder.append(" Parent: ").append(this.getNodeStr(this.parent));
        } else {
            stringBuilder.append(" Parent: null ");
        }
        stringBuilder.append(" Children: ");

        for (String st : childrenID) {
            stringBuilder.append(" NodeID: ").append(st);
        }

        return stringBuilder.toString();
    }

    /**
     * Simple util method
     *
     * @param edgeNode - edge node
     * @return String
     */
    private String getNodeStr(EdgeNode edgeNode) {

        WeightedEdge edge = edgeNode.edge;
        if (edge != null) {
            return "(u: " + edge.getSrc() + " v: " + edge.getDest() + " w: " + edge.getWeight() + ")";
        } else {
            return "null";
        }
    }
}
