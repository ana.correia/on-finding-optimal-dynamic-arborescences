package net.phyloviz.immutable.algo.arborescence;

public enum EdmondsEnum {

    EFFICIENT, INEFFICIENT, EFFICIENT_JAVA_HEAP, EFFICIENT_DISJOINT_SET, CAMERINI
}
