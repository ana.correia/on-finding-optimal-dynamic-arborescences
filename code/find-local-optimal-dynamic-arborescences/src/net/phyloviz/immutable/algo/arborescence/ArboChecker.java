package net.phyloviz.immutable.algo.arborescence;

import net.phyloviz.graph.DirectedGraph;
import net.phyloviz.graph.WeightedEdge;
import net.phyloviz.immutable.algo.connectivity.DirectedDfs;
import net.phyloviz.immutable.algo.cycles.DirectedCycle;
import net.phyloviz.immutable.algo.cycles.DynamicDirectedCycle;

import java.util.List;
import java.util.Set;

public class ArboChecker {

    /**
     * This class verifies if a given graph is a directed tree.
     * A directed tree follows the following criteria:
     * 1 - There are no cycles or circuits
     * 2 - All nodes are reachable from a given root
     * 3 - The in degree of every node except the root is equal to one
     */

    private DirectedGraph arborescence;
    private DirectedGraph reversedArboresncence;
    private int root;

    public ArboChecker(List<WeightedEdge> arborescence) {

        DirectedGraph directedGraph = new DirectedGraph();

        for (WeightedEdge weightedEdge : arborescence)
            directedGraph.addEdge(weightedEdge);

        this.constructorUtil(directedGraph);
    }

    /**
     * Class constructor
     * This constructor automatically finds the root
     *
     * @param arborescence - arborescence in Directed Graph object
     * @throws IllegalArgumentException if arborescence parameter has more than one root
     */
    public ArboChecker(DirectedGraph arborescence) {
        this.constructorUtil(arborescence);
    }

    private void constructorUtil(DirectedGraph arborescence) {

        this.arborescence = arborescence;
        this.reversedArboresncence = new DirectedGraph(arborescence);
        this.reversedArboresncence.reverseGraph();

        List<Integer> roots = this.reversedArboresncence.findZeroDegreeNode();

        if (roots.size() > 1) {

            throw new IllegalArgumentException("Input arborescence has more than one root: " + roots);
        }

        this.root = roots.get(0);
        System.out.println("ROOT: " + root);
    }

    /**
     * Class constructor
     *
     * @param arborescence - arborescence in Directed Graph object
     * @param root         r - root of arborescence
     */
    public ArboChecker(DirectedGraph arborescence, int root) {

        this.arborescence = arborescence;
        this.reversedArboresncence = new DirectedGraph(arborescence);
        this.reversedArboresncence.reverseGraph();
        this.root = root;
    }

    /**
     * Verifies if a given arborescence follows the criteria of directed tree
     *
     * @return true if arborescence follows all the criteria false otherwise
     */
    public boolean checkArbo() {

        DirectedCycle directedCycle = new DynamicDirectedCycle(this.arborescence);
        DirectedDfs directedDfs = new DirectedDfs(this.arborescence, this.root);

        return (!directedCycle.hasCycle()) && directedDfs.allNodesReachableFromRoot() && onlyOneIncidentEdgeForNode();
    }

    /**
     * Check if every node has no more than one incident edge
     *
     * @return true if every node has only one incident edge
     */
    public boolean onlyOneIncidentEdgeForNode() {

        boolean res = true;

        Set<Integer> nodeSet = this.reversedArboresncence.getNodeSet();
        for (Integer u : nodeSet) {
            if (this.reversedArboresncence.getNeighbors(u).size() > 1)
                res = false;
        }

        return res;
    }

    public int getRoot() {
        return this.root;
    }
}
