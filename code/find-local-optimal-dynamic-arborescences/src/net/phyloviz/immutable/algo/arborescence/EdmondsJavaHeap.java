package net.phyloviz.immutable.algo.arborescence;

import net.phyloviz.Arborescence;
import net.phyloviz.graph.DirectedGraph;
import net.phyloviz.graph.WeightedEdge;
import net.phyloviz.util.DisjointSet;

import java.util.*;

public class EdmondsJavaHeap implements Arborescence {

    private DirectedGraph G;
    private Comparator<WeightedEdge> cmp;
    private PriorityQueue<WeightedEdge>[] queues;
    private WeightedEdge[] in;
    private DisjointSet weaklyConnected;
    private DisjointSet stronglyConnected;

    private List<Set<WeightedEdge>> B;
    private List<Set<WeightedEdge>> Q;
    private List<WeightedEdge> QMax;
    private List<DisjointSet> stronglyConnectSets;
    private int level;
    private List<Integer> contractedNodes;


    private long unionOperations;
    private long popMinOperations;
    private long reducedCostOperations;
    private long findCycleOperations;
    private long nReductions;
    private long nCycles;
    private long nUnions;
    private BenchMark benchMark;

    /**
     * Class Constructor
     *
     * @param G   - Input graph
     * @param cmp - Edge comparator object
     */
    @SuppressWarnings("unchecked")
    public EdmondsJavaHeap(DirectedGraph G, Comparator<WeightedEdge> cmp) {

        this.G = G;
        this.cmp = cmp;

        this.queues = new PriorityQueue[this.G.size()];
        this.in = new WeightedEdge[this.G.size()];
        this.weaklyConnected = new DisjointSet(this.G.size());
        this.stronglyConnected = new DisjointSet(this.G.size());

        this.B = new ArrayList<>(this.G.size());
        this.Q = new ArrayList<>(this.G.size());
        this.QMax = new ArrayList<>(this.G.size());
        this.stronglyConnectSets = new ArrayList<>(this.G.size());
        this.stronglyConnectSets.add(new DisjointSet(this.stronglyConnected));

        this.contractedNodes = new ArrayList<>(this.G.size());
        this.level = 0;

        Set<Integer> nodeSet = this.G.getNodeSet();
        List<WeightedEdge> weightedEdges = this.G.getAllEdges();

        // init structures
        for (Integer node : nodeSet) {
            this.in[node] = null;
            this.queues[node] = new PriorityQueue<>(this.cmp);
        }

        // add edges to the respective queue
        // copy edge in the process
        for (WeightedEdge edge : weightedEdges) {
            int dst = edge.getDest();
            WeightedEdge copy = new WeightedEdge(edge);
            copy.setWeight(copy.getWeight());
            PriorityQueue<WeightedEdge> priorityQueue = this.queues[dst];
            priorityQueue.add(copy);
        }

        this.unionOperations = 0;
        this.popMinOperations = 0;
        this.reducedCostOperations = 0;
        this.findCycleOperations = 0;
        this.nReductions = 0;
        this.nCycles = 0;
        this.nUnions = 0;
    }

    /**
     * Returns arborescence of G
     *
     * @return List of edges containing arborescence of G
     */
    @Override
    public List<WeightedEdge> getArborescence() {

        return getArborescenceUtil();
    }


    /**
     * Perform all the necessary computation
     * Most of the code in this function is related to the contraction phase
     *
     * @return List of edges containing arborescence of G
     */
    @SuppressWarnings("Duplicates")
    private List<WeightedEdge> getArborescenceUtil() {

        // contraction phase
        Set<Integer> nodeSet = this.G.getNodeSet();
        LinkedList<Integer> roots = new LinkedList<>(nodeSet);

        // store the current branching
        LinkedList<WeightedEdge> branching = new LinkedList<>();
        long startContraction = System.currentTimeMillis();
        while (!roots.isEmpty()) {

            int root = roots.pop();

            long popStart = System.currentTimeMillis();
            // evaluate next root
            PriorityQueue<WeightedEdge> PQ = this.queues[root];
            if (PQ.isEmpty()) {
                continue;
            }

            // popping self loops
            WeightedEdge min = PQ.poll();
            while (PQ.size() > 0 && this.stronglyConnected.sameSet(min.getSrc(), min.getDest())) {
                min = PQ.poll();
            }

            long popEnd = System.currentTimeMillis();
            this.popMinOperations += (popEnd - popStart);

            int u = min.getSrc();
            int v = min.getDest();

            // this component does not have any edge that is not a self loop so we must check another component
            if (this.stronglyConnected.sameSet(u, v)) {
                continue;
            }

            // check if edge min is safe to add to the arborescence
            if (this.weaklyConnected.findSet(u) != this.weaklyConnected.findSet(v)) {
                this.in[root] = min;
                branching.add(min);
                this.weaklyConnected.unionSet(this.weaklyConnected.findSet(u), this.weaklyConnected.findSet(v));
            } else {

                this.nCycles++;
                // we have a cycle lets process it

                // store edges in the cycle
                HashSet<WeightedEdge> cycleEdges = new HashSet<>();
                cycleEdges.add(min);

                // store nodes in the cycle
                List<Integer> contractionSet = new ArrayList<>();
                contractionSet.add(this.stronglyConnected.findSet(v));
                this.in[root] = null;

                // find the cycle
                HashMap<Integer, WeightedEdge> map = new HashMap<>();
                map.put(this.stronglyConnected.findSet(v), min);
                long startFindCycle = System.currentTimeMillis();
                for (int i = this.stronglyConnected.findSet(min.getSrc());
                     this.in[i] != null;
                     i = this.stronglyConnected.findSet(this.in[i].getSrc())) {

                    map.put(i, this.in[i]);
                    cycleEdges.add(this.in[i]);
                    contractionSet.add(i);
                }

                long endFindCycle = System.currentTimeMillis();
                this.findCycleOperations += (endFindCycle - startFindCycle);

                // store the cycle
                this.Q.add(new HashSet<>(cycleEdges));

                //System.out.println("Cycle edges: " + cycleEdges);
                //System.out.println("Contraction set: " + contractionSet);

                // find maximum weight edge
                WeightedEdge maxEdgeCycle = Collections.max(cycleEdges, this.cmp);
                int maxW = maxEdgeCycle.getWeight();

                // compute reduced costs
                long startReduced = System.currentTimeMillis();
                //System.out.println("Contraction set: " + contractionSet);
                for (Integer node : contractionSet) {

                    PriorityQueue<WeightedEdge> Q = this.queues[node];
                    int incidentW = map.get(node).getWeight();

                    for (WeightedEdge edge : Q) {
                        int newW = edge.getWeight() - (incidentW - maxW);
                        edge.setWeight(newW);
                        this.nReductions++;
                    }
                }

                long endReduced = System.currentTimeMillis();
                this.reducedCostOperations += (endReduced - startReduced);

                // perform binaryHeapUnion
                for (WeightedEdge weightedEdge : cycleEdges) {
                    this.stronglyConnected.unionSet(weightedEdge.getSrc(), weightedEdge.getDest());
                }

                // find cycle representative
                int rep = this.stronglyConnected.findSet(maxEdgeCycle.getSrc());

                // the representative will be the next node to be evaluated
                roots.add(0, rep);

                long unionStart = System.currentTimeMillis();
                PriorityQueue<WeightedEdge> heap = this.queues[rep];
                //System.out.println("Heap size: " + heap.size());
                // perform heap binaryHeapUnion
                for (Integer node : contractionSet) {
                    if (rep != node) {
                        heap.addAll(this.queues[node]);
                        this.queues[node] = null;
                        this.nUnions++;
                        //System.out.println("After binaryHeapUnion Heap size: " + heap.size());
                    }
                }
                long unionEnd = System.currentTimeMillis();
                this.unionOperations += (unionEnd - unionStart);

                // store maximum weight edge
                this.QMax.add(maxEdgeCycle);
                // add to the contracted nodes the cycle representative
                this.contractedNodes.add(rep);
                // store disjoint set data structure
                this.stronglyConnectSets.add(new DisjointSet(this.stronglyConnected));
                // store current branching
                this.B.add(new HashSet<>(branching));
                // clear the branching for the next iteration
                branching.removeAll(cycleEdges);
                // increment contraction level
                this.level++;
            }
        }

        long endContraction = System.currentTimeMillis();
        this.B.add(new HashSet<>(branching));
        // return solution

        long solStart = System.currentTimeMillis();
        List<WeightedEdge> solution = this.expansion();
        long solEnd = System.currentTimeMillis();
        //System.out.println("Expansion time: " + (solEnd - solStart) / 1000F);

        this.benchMark = new BenchMark(this.unionOperations,
                this.popMinOperations,
                reducedCostOperations,
                this.findCycleOperations,
                this.nReductions,
                this.nCycles,
                this.nUnions,
                endContraction-startContraction,
                solEnd - solStart);

        return solution;
    }

    /**
     * This function handles the expansion phase of the algorithm
     *
     * @return minimum spanning arborescence
     */
    @SuppressWarnings("Duplicates")
    private List<WeightedEdge> expansion() {
        // perform expansion phase

        HashSet<WeightedEdge> edges = new HashSet<>(this.B.get(this.level));
        while (this.level > 0) {

            this.level--;

            int mergedNode = this.contractedNodes.get(this.level);
            Set<WeightedEdge> circuit = this.Q.get(this.level);

            DisjointSet uf = this.stronglyConnectSets.get(this.level + 1);
            boolean isRoot = this.isRoot(edges, mergedNode, uf);

            if (isRoot) {

                edges.addAll(circuit);
                WeightedEdge max = this.QMax.get(this.level);
                edges.remove(max);

            } else {

                DisjointSet _uf = this.stronglyConnectSets.get(this.level);

                HashSet<Integer> endpoints = new HashSet<>();

                for (WeightedEdge edge : edges) {
                    int dst = _uf.findSet(edge.getDest());
                    endpoints.add(dst);
                }

                for (WeightedEdge cycleEdge : circuit) {

                    int dst = _uf.findSet(cycleEdge.getDest());

                    if (!endpoints.contains(dst)) {
                        edges.add(cycleEdge);
                    }
                }

            }
        }

        List<WeightedEdge> sol = new ArrayList<>(edges.size());
        for (WeightedEdge edge : edges) {
            edge.setWeight(edge.getOriginalWeight());
            sol.add(edge);
        }

        return sol;
    }

    /**
     * Check if mergedNode is root or not
     *
     * @param edges      - partial solution
     * @param mergedNode - super node
     * @param uf         - binaryHeapUnion find
     * @return true if merged node is root, false otherwise
     */
    @SuppressWarnings("Duplicates")
    private boolean isRoot(HashSet<WeightedEdge> edges, int mergedNode, DisjointSet uf) {

        boolean result = true;
        for (WeightedEdge weightedEdge : edges) {

            int dst = weightedEdge.getDest();
            dst = uf.findSet(dst);

            if (dst == mergedNode) {
                result = false;
                break;
            }
        }

        return result;
    }

    /**
     * Calculate arborescence weight
     *
     * @param lst - list with arborescence
     * @return arborescene weight
     */
    @Override
    public int computeWeight(List<WeightedEdge> lst) {

        int w = 0;
        for (WeightedEdge weightedEdge : lst) {
            w += weightedEdge.getWeight();
        }

        return w;
    }

    @Override
    public BenchMark getBenchMark() {
        return this.benchMark;
    }
}
