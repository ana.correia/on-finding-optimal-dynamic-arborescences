package net.phyloviz.immutable.algo.arborescence;

public class BenchMark {

    private long unionOperations;
    private long popMinOperations;
    private long reducedCostOperations;
    private long findCycleOperations;
    private long nReductions;
    private long nCycles;
    private long nUnions;
    private long contraction;
    private long expansion;
    private long historic;
    
    public BenchMark(long unionOperations, long popMinOperations, long reducedCostOperations, long findCycleOperations, 
                     long nReductions, long nCycles, long nUnions, long contraction, long expansion, long historic) {
        
        
        this(unionOperations,  popMinOperations,  reducedCostOperations,  findCycleOperations, nReductions,  nCycles,  nUnions,  contraction,  expansion);
        this.historic = historic;
    }
    
    
    public BenchMark(long unionOperations, long popMinOperations, long reducedCostOperations, long findCycleOperations,
                     long nReductions, long nCycles, long nUnions, long contraction, long expansion)
    {
        this.unionOperations = unionOperations;
        this.popMinOperations = popMinOperations;
        this.reducedCostOperations = reducedCostOperations;
        this.findCycleOperations = findCycleOperations;
        this.nReductions = nReductions;
        this.nCycles = nCycles;
        this.nUnions = nUnions;
        this.contraction = contraction;
        this.expansion = expansion;

    }


    public long getUnionOperations() {
        return unionOperations;
    }

    public long getPopMinOperations() {
        return popMinOperations;
    }

    public long getReducedCostOperations() {
        return reducedCostOperations;
    }

    public long getFindCycleOperations() {
        return findCycleOperations;
    }

    public long getnReductions() {
        return nReductions;
    }

    public long getnCycles() {
        return nCycles;
    }

    public long getnUnions() {
        return nUnions;
    }



    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Union time: ").append(this.unionOperations / 1000F);
        stringBuilder.append("\n");
        stringBuilder.append("Pop minimum time: ").append(this.popMinOperations / 1000F);
        stringBuilder.append("\n");
        stringBuilder.append("Reduced cost operation: ").append(this.reducedCostOperations / 1000F);
        stringBuilder.append("\n");
        stringBuilder.append("# of reduce costs: ").append(this.nReductions);
        stringBuilder.append("\n");
        stringBuilder.append("Find cycle operation: ").append(this.findCycleOperations / 1000F);
        stringBuilder.append("\n");
        stringBuilder.append("Historic: ").append(this.historic / 1000F);
        stringBuilder.append("\n");
        stringBuilder.append("# cycles: ").append(this.nCycles);
        stringBuilder.append("\n");
        stringBuilder.append("# unions: ").append(this.nUnions);
        stringBuilder.append("\n");
        stringBuilder.append("Contraction time: ").append(this.contraction/1000F);
        stringBuilder.append("\n");
        stringBuilder.append("Expansion time: ").append(this.expansion/1000F);
        return stringBuilder.toString();
    }
}
