package net.phyloviz.immutable.algo.cycles;

import java.util.Stack;

public interface DirectedCycle {

    public Stack<Integer> cycle();
    public boolean hasCycle();
}
