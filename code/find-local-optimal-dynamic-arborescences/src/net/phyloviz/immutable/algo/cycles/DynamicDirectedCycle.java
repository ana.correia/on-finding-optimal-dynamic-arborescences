package net.phyloviz.immutable.algo.cycles;

import net.phyloviz.graph.DirectedGraph;
import net.phyloviz.graph.WeightedEdge;

import java.util.*;

public class DynamicDirectedCycle implements DirectedCycle {


    private HashMap<Integer, Boolean> marked;        // marked[v] = has vertex v been marked?
    private HashMap<Integer, Integer> edgeTo;        // edgeTo[v] = previous vertex on path to v
    private HashMap<Integer, Boolean> onStack;       // onStack[v] = is vertex on the stack?

    private Stack<Integer> cycle;    // directed cycle (or null if no such cycle)
    private DirectedGraph G;

    /**
     * Class constructor
     * @param G - input graph
     */
    public DynamicDirectedCycle(DirectedGraph G) {

        Set<Integer> nodes = G.getNodeSet();
        this.G = G;
        this.marked = new HashMap<>();
        this.onStack = new HashMap<>();
        this.edgeTo = new HashMap<>();

        for (Integer v : nodes) {
            this.marked.put(v, false);
            this.onStack.put(v, false);
            this.edgeTo.put(v, -1);
        }

        for (Integer v : nodes)
            if (!this.marked.get(v))
                this.dfs(G, v);
    }

    // check that algorithm computes either the topological order or finds a directed cycle
    private void dfs(DirectedGraph G, int v) {

        this.onStack.put(v, true);
        this.marked.put(v, true);

        for (WeightedEdge e : G.getNeighbors(v)) {

            int w = e.getDest();

            // short circuit if directed cycle found
            if (this.hasCycle())
                return;

            // found new vertex, so recur
            else if (!this.marked.get(w)) {
                this.edgeTo.put(w, v);
                this.dfs(G, w);
            }

            // trace back directed cycle
            else if (this.onStack.get(w)) {

                this.cycle = new Stack<>();

                for (int x = v; x != w; x = this.edgeTo.get(x)) {
                    this.cycle.push(x);
                }

                this.cycle.push(w);
                this.cycle.push(v);
                assert check();
            }
        }

        this.onStack.put(v, false);
    }

    // certify that digraph has a directed cycle if it reports one
    @SuppressWarnings("Duplicates")
    private boolean check() {

        if (hasCycle()) {
            // verify cycle
            int first = -1, last = -1;
            for (int v : cycle()) {
                if (first == -1) first = v;
                last = v;
            }
            if (first != last) {
                System.err.printf("cycle begins with %d and ends with %d\n", first, last);
                return false;
            }
        }

        return true;
    }

    /**
     * Does the digraph have a directed cycle?
     *
     * @return {@code true} if the digraph has a directed cycle, {@code false} otherwise
     */
    @Override
    public boolean hasCycle() {
        return this.cycle != null;
    }

    /**
     * Returns a directed cycle if the digraph has a directed cycle, and {@code null} otherwise.
     *
     * @return a directed cycle (as an iterable) if the digraph has a directed cycle,
     * and {@code null} otherwise
     */
    @Override
    public Stack<Integer> cycle() {
        return this.cycle;
    }

    public List<WeightedEdge> cycleEdges() {

        List<WeightedEdge> edgeList = new LinkedList<>();
        while (this.cycle.size() > 1) {
            int u = this.cycle.pop();
            int v = this.cycle.peek();
            edgeList.add(this.G.findEdge(u,v).get(0));
        }

        return edgeList;
    }

}
