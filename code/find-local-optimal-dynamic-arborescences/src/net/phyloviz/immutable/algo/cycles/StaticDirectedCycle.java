package net.phyloviz.immutable.algo.cycles;

import net.phyloviz.graph.DirectedGraph;
import net.phyloviz.graph.WeightedEdge;

import java.util.ArrayList;
import java.util.Stack;

public class StaticDirectedCycle implements DirectedCycle {
    /**
     * Mainly based on Robert Sedwick Algorithms 4th Edition page 576
     */

    private boolean[] marked;        // marked[v] = has vertex v been marked?
    private int[] edgeTo;            // edgeTo[v] = previous vertex on path to v
    private boolean[] onStack;       // onStack[v] = is vertex on the stack?

    private Stack<Integer> cycle;    // directed cycle (or null if no such cycle)

    private ArrayList<Stack<Integer>> cycles;

    /**
     * Determines whether the digraph G has a directed cycle and, if so,
     * finds such a cycle.
     *
     * @param G the digraph
     */
    public StaticDirectedCycle(DirectedGraph G) {

        this.marked = new boolean[G.size()];
        this.onStack = new boolean[G.size()];
        this.edgeTo = new int[G.size()];
        this.cycles = new ArrayList<>();

        for (int v = 0; v < G.size(); v++)
            if (!this.marked[v] && this.cycle == null)
                this.dfs(G, v);

    }

    /**
     * Does the digraph have a directed cycle?
     *
     * @return {@code true} if the digraph has a directed cycle, {@code false} otherwise
     */
    @Override
    public boolean hasCycle() {
        return this.cycle != null;
    }

    /**
     * Returns a directed cycle if the digraph has a directed cycle, and {@code null} otherwise.
     *
     * @return a directed cycle (as an iterable) if the digraph has a directed cycle,
     * and {@code null} otherwise
     */
    @Override
    public Stack<Integer> cycle() {
        return cycle;
    }

    public ArrayList<Stack<Integer>> getCycles() {
        return this.cycles;
    }

    // check that algorithm computes either the topological order or finds a directed cycle
    private void dfs(DirectedGraph G, int v) {

        this.onStack[v] = true;
        this.marked[v] = true;

        for (WeightedEdge e : G.getNeighbors(v)) {

            int w = e.getDest();

            // short circuit if directed cycle found
            if (this.hasCycle())
                return;

                // found new vertex, so recur
            else if (!this.marked[w]) {
                this.edgeTo[w] = v;
                this.dfs(G, w);
            }

            // trace back directed cycle
            else if (this.onStack[w]) {

                this.cycle = new Stack<>();
                Stack<Integer> stack = new Stack<>();
                for (int x = v; x != w; x = this.edgeTo[x]) {

                    this.cycle.push(x);
                    stack.push(x);
                }

                this.cycle.push(w);
                this.cycle.push(v);
                stack.push(w);
                stack.push(v);

                this.cycles.add(stack);

                assert check();
            }
        }

        this.onStack[v] = false;
    }

    // certify that digraph has a directed cycle if it reports one
    @SuppressWarnings("Duplicates")
    private boolean check() {

        if (hasCycle()) {
            // verify cycle
            int first = -1, last = -1;
            for (int v : cycle()) {
                if (first == -1) first = v;
                last = v;
            }
            if (first != last) {
                System.err.printf("cycle begins with %d and ends with %d\n", first, last);
                return false;
            }
        }

        return true;
    }
}