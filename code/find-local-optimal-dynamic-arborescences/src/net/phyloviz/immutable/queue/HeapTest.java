package net.phyloviz.immutable.queue;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class HeapTest {


    private Integer[] data;
    private Integer[] sortedData;
    private Comparator<Integer> cmp;

    @Before
    public void setup() {


        this.data = new Integer[]{4, 1, 8, 6, 9, 5, 10, 2, 3, 7};

        this.sortedData = this.data.clone();
        Arrays.sort(this.sortedData);

        Comparator<Integer> cmp = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return data[o1] - data[o2];
            }
        };

        this.cmp = cmp;
    }

    @Test
    public void testPushUpdate(){

        Heap<Integer> binHeap = new BinaryHeap(this.data.length, this.cmp);

        for (int i = 0; i < this.data.length; i++) {
            binHeap.push(i);
        }

        this.data[9] = -1;
        binHeap.push(9);
        assertEquals(9,(int) binHeap.pop());

        this.data[9] = 100;
        binHeap.push(9);
    }

    private void populateHeap(Heap heap, int s) {

        for (int i = 0; i < s; i++) {
            heap.push(i);
        }

        assertEquals(heap.size(), s);
    }


    @Test
    public void testInsertAndPopBinomialHeap() {
        BinaryHeap heap = new BinaryHeap(this.data.length, this.cmp);
        this.testInsertAndPop(heap);
    }

    @Test
    public void testInsertAndPopRankRelaxed() {
        FastRankRelaxedHeap heap = new FastRankRelaxedHeap(this.data.length, this.cmp);
        this.testInsertAndPop(heap);
    }

    @Test
    public void testInsertAndPopBinaryHeap() {
        BinomialHeap heap = new BinomialHeap(this.cmp);
        this.testInsertAndPop(heap);

    }

    private void testInsertAndPop(Heap<Integer>  heap) {

        this.populateHeap(heap, this.data.length);

        for (int i : this.sortedData) {

            int k = this.data[heap.top()];
            assertEquals(i, k);
            int j = this.data[heap.pop()];
            assertEquals(i, j);
        }

        assertEquals(heap.size(), 0);
    }

    @Test
    public void testUnionBinomialHeap() {

        Integer[] arr = new Integer[]{4, 1, 8, 6, 9, 5, 10, 2, 3, 7, 11, 88, 99, 57, 9999, 64};
        Integer[] sorted = arr.clone();
        Arrays.sort(sorted);

        Comparator<Integer> cmp = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return arr[o1] - arr[o2];
            }
        };

        BinomialHeap heap = new BinomialHeap(cmp);
        BinomialHeap heap2 = new BinomialHeap(cmp);

        this.testUnion(heap, heap2, arr, sorted);
    }

    @Test
    public void testUnionBinaryHeap() {

        Integer[] arr = new Integer[]{4, 1, 8, 6, 9, 5, 10, 2, 3, 7, 11, 88, 99, 57, 9999, 64};
        Integer[] sorted = arr.clone();
        Arrays.sort(sorted);

        Comparator<Integer> cmp = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return arr[o1] - arr[o2];
            }
        };


        BinaryHeap heap = new BinaryHeap(arr.length / 2, cmp);
        BinaryHeap heap2 = new BinaryHeap(arr.length / 2, cmp);
        this.testUnion(heap, heap2, arr, sorted);

    }

    private void testUnion(Heap<Integer>  heap, Heap<Integer>  heap1, Integer[] arr, Integer[] sorted) {

        for (int i = 0; i < arr.length / 2; i++) {
            heap.push(i);
        }


        for (int i = arr.length / 2; i < arr.length; i++) {
            heap1.push(i);

        }

        heap.union(heap1);

        assertEquals(arr.length, heap.size());

        for (int i : sorted) {
            int k = arr[heap.pop()];
            assertEquals(i, k);
        }
    }

    @Test
    public void testUpdateFastRankedRelaxed() {
        FastRankRelaxedHeap heap = new FastRankRelaxedHeap(this.data.length, this.cmp);
        this.testUpdate(heap);
    }

    @Test
    public void testUpdateBinaryHeap() {
        BinaryHeap heap = new BinaryHeap(this.data.length, this.cmp);
        this.testUpdate(heap);
    }


    @Test
    public void testUpdateBinomialHeap() {
        BinomialHeap heap = new BinomialHeap(this.cmp);
        this.testUpdate(heap);
    }

    private void testUpdate(Heap<Integer>  heap) {
        this.populateHeap(heap, this.data.length);

        this.data[6] = -1;
        heap.update(6);

        int k = this.data[heap.top()];
        assertEquals(-1, k);
        assertEquals(this.data.length, heap.size());

        int j = this.data[heap.pop()];
        assertEquals(-1, j);
        assertEquals(this.data.length - 1, heap.size());
    }

    @Test
    public void testDeleteBinomialHeap() {

        BinomialHeap heap = new BinomialHeap(this.cmp);
        this.populateHeap(heap, this.data.length);
        this.testDelete(heap, 0);
    }

    @Test
    public void testDeleteBinaryHeap() {

        BinaryHeap heap = new BinaryHeap(this.data.length, this.cmp);
        this.populateHeap(heap, this.data.length);
        this.testDelete(heap, 0);
    }

    @Test
    public void testDeleteFastRankedRelaxed() {

        FastRankRelaxedHeap heap = new FastRankRelaxedHeap(this.data.length, this.cmp);
        this.populateHeap(heap, this.data.length);
        this.testDelete(heap, 0);
    }


    private void testDelete(Heap<Integer> heap, int index) {

        heap.delete(index);

        int item = this.data[index];
        assertEquals(this.data.length - 1, heap.size());

        while (heap.size() > 0) {

            int k = this.data[heap.pop()];
            assertNotEquals(item, k);
        }

    }

}
