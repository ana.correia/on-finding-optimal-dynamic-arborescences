package net.phyloviz.immutable.queue;

import java.util.Comparator;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.Set;

public class BinaryHeap implements Heap<Integer> {

    private Integer[] A;
    private int heapSize;
    private HashMap<Integer, Integer> keyToPos;
    private Comparator<Integer> cmp;
    private boolean floydHeapConstruction = false;

    /**
     * Class constructor
     * Worst case O(1)
     *
     * @param capacity - initial size of the floydHeapConstruction heap
     * @param cmp      - comparator object of validation type T
     * @throws IllegalArgumentException if capacity <= 0 or comp == null
     */
    public BinaryHeap(int capacity, Comparator<Integer> cmp) {

        if (capacity <= 0 || cmp == null) {
            throw new IllegalArgumentException("The heap size must be larger than 0 and the comparator must not be null");
        }

        this.A = new Integer[capacity + 1];
        this.keyToPos = new HashMap<>(capacity);
        this.cmp = cmp;
        this.heapSize = 0;
    }

    public BinaryHeap(int capacity, Comparator<Integer> cmp, boolean floydHeapConstruction) {
        this(capacity, cmp);
        this.floydHeapConstruction = floydHeapConstruction;

    }


    public BinaryHeap(int[] keys, Comparator<Integer> cmp) {

        this.A = new Integer[keys.length + 1];

        for (int i = 0; i < keys.length; i++) {
            this.A[i + 1] = keys[i];
        }

        this.keyToPos = new HashMap<>(keys.length);
        this.cmp = cmp;
        this.buildHeap();
    }

    /**
     * Pushes a new item to the heap
     * Worst case is O(log(n))
     *
     * @param idx item to be pushed in the heap
     */
    @Override
    public void push(Integer idx) {

        if (this.keyToPos.containsKey(idx)) {
            this.heapifyUp(this.keyToPos.get(idx));
            return;
        }


        this.heapSize++;
        this.A[this.heapSize] = idx;
        this.keyToPos.put(idx, this.heapSize);
        this.heapifyUp(this.heapSize);
    }

    /**
     * Decrease key
     * Worst case is O(log(n))
     *
     * @param idx - index
     */
    @Override
    public void update(Integer idx) {
        this.heapifyUp(this.keyToPos.get(idx));
    }

    /**
     * Deletes the item at position index
     * Worst case is O(log(n))
     *
     * @param idx - key to be deleted
     * @throws IllegalStateException  if the heap is empty
     * @throws NoSuchElementException if key does not exists
     */
    @Override
    public void delete(Integer idx) {

        if (this.isEmpty()) {
            throw new IllegalStateException("The heap is empty");
        }

        if (!this.hasKey(idx)) {
            throw new NoSuchElementException();
        }


        int keyPosition = this.keyToPos.get(idx);
        this.A[keyPosition] = this.A[this.heapSize];
        this.A[this.heapSize] = null;
        this.heapifyDown(keyPosition);
        this.keyToPos.remove(idx);
        this.heapSize--;
    }


    /**
     * This function unites two heaps
     * Worst case is O(n)
     *
     * @param heap heap
     * @throws IllegalArgumentException if parameter heap is not Binary heap type
     */
    @Override
    public void union(Heap<Integer> heap) {

        if (!(heap instanceof BinaryHeap)) {
            throw new IllegalArgumentException("The parameter heap must be of Binary Heap type");
        }

        if (this.floydHeapConstruction) {
            this.unionFloyd(heap);
        } else {
            this.dummyUnion(heap);
        }
    }

    /**
     * This methods add ony by one element of parameter heap
     * When current heap is full duplicate the space
     * Worst case is O(n log n)
     * @param heap - heap
     */
    private void dummyUnion(Heap heap) {

        BinaryHeap binaryHeap = (BinaryHeap) heap;
        Integer[] A = binaryHeap.getA();

        for (int i = 1; i <= binaryHeap.heapSize; i++) {

            if (this.heapSize + 1 == this.A.length) {
                int newLen = this.A.length * 2;
                Integer[] newA = new Integer[newLen];
                System.arraycopy(this.A, 0, newA, 0, this.A.length);
                this.A = newA;
            }

            this.push(A[i]);
        }
    }

    /**
     * This method implements the heap-construction procedure developed by Floyd
     * this method tends to have many cache misses
     * Worst case is O(n)
     * @param heap - heap
     */
    private void unionFloyd(Heap heap) {

        BinaryHeap binaryHeap = (BinaryHeap) heap;
        Integer[] arr = binaryHeap.A;
        int newHeapSize = binaryHeap.heapSize + this.heapSize + 1;
        Integer[] newArr = new Integer[newHeapSize];
        newArr[0] = null;

        for (int i = 1; i <= this.heapSize; i++) {
            newArr[i] = this.A[i];
        }

        int k = this.heapSize + 1;
        for (int i = 1; i <= binaryHeap.heapSize; i++) {
            this.keyToPos.put(arr[i], k);
            newArr[k] = arr[i];
            k++;
        }

        this.A = newArr;
        this.heapSize = this.A.length - 1;
        this.buildHeap();

    }

    /**
     * Returns minimum element on the heap
     * Worst case is O(log(n))
     *
     * @return minimum element on the heap
     * @throws IllegalStateException if heap is empty
     */
    @Override
    public Integer top() {

        if (this.isEmpty()) {
            throw new IllegalStateException("The heap is empty");
        }

        return this.A[1];
    }

    /**
     * Removes the node with the lowest key
     * Worst case is O(log(n))
     *
     * @return min node
     * @throws IllegalStateException if heap is empty
     */
    @Override
    public Integer pop() {

        if (this.isEmpty()) {
            throw new IllegalStateException("The heap is empty");
        }

        int min = this.A[1];
        this.A[1] = this.A[this.heapSize];
        this.A[this.heapSize] = null;
        this.heapSize--;
        this.heapifyDown(1);
        this.keyToPos.remove(min);
        return min;
    }

    /**
     * Returns size of heap
     * Worst case is O(1)
     *
     * @return size of heap
     */
    @Override
    public int size() {
        return this.heapSize;
    }

    /**
     * Check if heap is Empty
     * Worst case is O(1)
     *
     * @return true if heap is empty, false otherwise
     */
    @Override
    public boolean isEmpty() {
        return this.heapSize == 0;
    }

    /**
     * Check if a given key exists on the heap
     * Average case is O(1)
     *
     * @param key - key
     * @return - true if key exists false otherwise
     */
    @Override
    public boolean hasKey(Integer key) {
        return this.keyToPos.containsKey(key);
    }

    /**
     * Comparator setter
     *
     * @param cmp - comparator object
     */
    @Override
    public void setComparator(Comparator<Integer> cmp) {
        this.cmp = cmp;
    }

    /**
     * Getter for key set
     *
     * @return Current keys on heap
     */
    @Override
    public Set<Integer> getKeySet() {
        return this.keyToPos.keySet();
    }

    /**
     * Getter for heap array holding the keys
     *
     * @return - array holding the keys
     */
    public Integer[] getA() {
        return this.A;
    }

    /**
     * Comparator getter
     *
     * @return comparator
     */
    @Override
    public Comparator comparator() {
        return this.cmp;
    }

    /**
     * Getter for the hashmap mapping keys to position in A
     *
     * @return key to position mapper
     */
    public HashMap<Integer, Integer> getKeyToPos() {
        return this.keyToPos;
    }

    /**
     * Override of to string method
     *
     * @return string
     */
    @Override
    public String toString() {
        return this.toStringHelper();
    }

    /**
     * Build a string with the content of the heap
     *
     * @return String containing the heap content
     */
    private String toStringHelper() {

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < (this.A.length) / 2; i++) {
            boolean f = false;

            for (int j = 0; (j < Math.pow(2, i)) && (j + Math.pow(2, i)) <= this.heapSize; j++) {

                builder.append(this.A[j + (int) Math.pow(2, i) - 1]);
                builder.append(" ");
                f = true;
            }

            if (!f) {
                break;
            }

            builder.append("\n");
        }

        return builder.toString();
    }

    /**
     * Calculates the position of the father of idx
     * Worst case is O(1)
     *
     * @param i child position
     * @return position of the father of idx
     */
    private int parent(int i) {
        return i >> 1;
    }

    /**
     * Calculates the position of the leftIndex child of idx
     * Worst case is O(1)
     *
     * @param i parentIndex index
     * @return index of the leftIndex child of idx
     */
    private int left(int i) {
        return i << 1;
    }

    /**
     * Calculates the position of the rightIndex child of idx
     * Worst case is O(1)
     *
     * @param i parentIndex index
     * @return index of the rightIndex child of idx
     */
    private int right(int i) {
        return (i << 1) + 1;
    }

    /**
     * Maintain the min heap property by going down on the heap
     * Worst case is O(log(n))
     *
     * @param i index where we start to heapify down
     */
    private void heapifyDown(int i) {

        int l = this.left(i);
        int r = this.right(i);
        int smallest = i;

        if (l <= this.heapSize && this.cmp.compare(this.A[l], this.A[i]) < 0) {

            smallest = l;
        }

        if (r <= this.heapSize && this.cmp.compare(this.A[r], this.A[smallest]) < 0) {
            smallest = r;
        }

        if (smallest != i) {
            this.swap(i, smallest);
            this.heapifyDown(smallest);
        }
    }

    /**
     * Maintain the min heap property
     * Worst case is O(log(n))
     *
     * @param i where we start to heapify
     */
    private void heapifyUp(int i) {

        int index = i;
        while (index > 1 && (this.cmp.compare(this.A[this.parent(index)], this.A[index]) > 0)) {
            this.swap(index, this.parent(index));
            index = this.parent(index);
        }
    }


    /**
     * Swaps i with j in the heap
     * Worst case is O(1)
     *
     * @param i position
     * @param j position
     */
    private void swap(int i, int j) {

        this.keyToPos.replace(this.A[i], j);
        this.keyToPos.replace(this.A[j], i);

        int temp = this.A[i];
        this.A[i] = this.A[j];
        this.A[j] = temp;
    }

    /**
     * This function builds the heap maintaining the min heap property
     * Worst case is O(n)
     *
     * @throws IllegalStateException if the heap attribute is null
     */
    private void buildHeap() {

        if (this.A == null) {
            throw new IllegalStateException("The heap is null");
        }

        this.heapSize = this.A.length - 1;
        for (int i = (this.A.length / 2); i >= 1; i--) {
            this.heapifyDown(i);
        }
    }
}
