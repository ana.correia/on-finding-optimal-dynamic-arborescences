package net.phyloviz.immutable.queue;

import net.phyloviz.graph.WeightedEdge;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Set;

public class JavaBinaryHeap implements Heap<WeightedEdge> {

    private PriorityQueue<WeightedEdge> PQ;
    private Comparator<WeightedEdge> cmp;

    public JavaBinaryHeap(Comparator<WeightedEdge> cmp){

        this.cmp = cmp;
        this.PQ = new PriorityQueue<>(this.cmp);
    }

    public JavaBinaryHeap(Comparator<WeightedEdge> cmp, int n){

        this.cmp = cmp;
        this.PQ = new PriorityQueue<>(n, this.cmp);
    }

    @Override
    public Comparator<WeightedEdge> comparator() {
        return this.cmp;
    }

    @Override
    public void push(WeightedEdge idx) {
        this.PQ.add(idx);
    }

    @Override
    public void update(WeightedEdge idx) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(WeightedEdge idx) {
        this.PQ.remove(idx);
    }

    @Override
    public void union(Heap heap) {

        if (!(heap instanceof JavaBinaryHeap)) {
            throw new IllegalArgumentException("heap must be of type Binomial Heap");
        }

        JavaBinaryHeap other= (JavaBinaryHeap) heap;
        this.PQ.addAll(other.PQ);
    }

    @Override
    public WeightedEdge top() {
        return this.PQ.peek();
    }

    @Override
    public WeightedEdge pop() {
        return this.PQ.poll();
    }

    @Override
    public int size() {
        return this.PQ.size();
    }

    @Override
    public boolean isEmpty() {
        return this.PQ.isEmpty();
    }

    @Override
    public boolean hasKey(WeightedEdge key) {
        return this.PQ.contains(key);
    }

    @Override
    public void setComparator(Comparator<WeightedEdge> cmp) {
        this.cmp = cmp;
    }

    @Override
    public Set<WeightedEdge> getKeySet() {
        return null;
    }
}
