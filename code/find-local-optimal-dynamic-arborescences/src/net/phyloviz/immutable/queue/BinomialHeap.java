package net.phyloviz.immutable.queue;

import java.util.*;


public class BinomialHeap implements Heap<Integer> {

    public class Node {

        private int key;
        private int degree;
        private Node parent, child, sibling;
        private Boolean remove;

        /**
         * Class attributes
         *
         * @param key - key
         */
        @SuppressWarnings("Duplicates")
        private Node(int key) {

            this.key = key;
            this.degree = 0;
            this.parent = null;
            this.child = null;
            this.sibling = null;
            this.remove = false;
        }

        @SuppressWarnings("Duplicates")
        private Node(Node node) {
            this.key = node.key;
            this.degree = node.degree;
            this.parent = node.parent;
            this.child = node.child;
            this.sibling = node.sibling;
            this.remove = node.remove;
        }

        /**
         * Override of equals method
         *
         * @param other - other object
         * @return true if node key are equal false otherwise
         */
        @Override
        public boolean equals(Object other) {

            if (other == null)
                return false;

            if (!(other instanceof Node))
                return false;

            return this.key == ((Node) other).key;
        }

        /**
         * Override of to string method
         *
         * @return String
         */
        @Override
        public String toString() {

            StringBuilder builder = new StringBuilder();
            toStringHelper(0, builder);
            return builder.toString();
        }

        /**
         * Helper method to print the object
         *
         * @param level - level of print
         * @param sb    - String builder object
         */
        private void toStringHelper(int level, StringBuilder sb) {
            Node curr = this;
            while (curr != null) {

                for (int i = 0; i < level; i++) {
                    sb.append(" ");
                }

                sb.append(curr.key);
                sb.append("\n");

                if (curr.child != null) {
                    curr.child.toStringHelper(level + 1, sb);
                }

                curr = curr.sibling;
            }
        }
    }

    /**
     * Binomial heap attributes
     */

    private Node head;
    private Comparator<Integer> cmp;
    private HashMap<Integer, Node> map;

    /**
     * Class constructor
     * Worst case O(1)
     *
     * @param cmp Comparator object
     * @throws IllegalArgumentException if cmp is null
     */
    public BinomialHeap(Comparator<Integer> cmp) {

        if (cmp == null) {
            throw new IllegalArgumentException("The comparators must not be null");
        }

        this.cmp = cmp;
        this.head = null;
        this.map = new HashMap<>();
    }

    /**
     * Class constructor
     * Worst case O(1)
     *
     * @param cmp  Comparator object
     * @param head Head BinomialNode
     * @throws IllegalArgumentException if cmp is null
     */
    public BinomialHeap(Node head, Comparator<Integer> cmp) {

        if (cmp == null) {
            throw new IllegalArgumentException("The comparator must not be null");
        }

        this.cmp = cmp;
        this.head = head;
        this.map = new HashMap<>();
    }

    /**
     * Class constructor
     * Worst case O(n log(n))
     *
     * @param cmp   Comparator object
     * @param items Head BinomialNode
     * @throws IllegalArgumentException if cmp is null
     */
    public BinomialHeap(int[] items, Comparator<Integer> cmp) {

        if (items == null || cmp == null) {
            throw new IllegalArgumentException("The comparators and items must not be null");
        }

        this.head = null;
        this.cmp = cmp;
        this.map = new HashMap<>();

        for (int item : items) {
            this.push(item);
        }
    }

    /**
     * This constructor was only used to take run times
     */
    public BinomialHeap() {
        this.head = null;
        this.map = new HashMap<>();
    }

    @Override
    public Comparator<Integer> comparator() {
        return this.cmp;
    }

    /**
     * Pushes a new item into the heap
     * Worst case is O(log(n))
     *
     * @param item - key or item to pushed
     */
    @Override
    @SuppressWarnings("Duplicates")
    public void push(Integer item) {

        if (this.map.containsKey(item)) {
            this.update(item);
            return;
        }

        Node node = new Node(item);
        BinomialHeap tempHeap = new BinomialHeap(node, this.cmp);
        this.map.put(item, node);
        this.head = this.unite(tempHeap);
    }

    /**
     * Only supports decrease key
     * Worst case is O(log(n))
     *
     * @param idx
     */
    @Override
    public void update(Integer idx) {
        Node node = this.keyToNode(idx);
        this.heapifyUp(node, false);
    }

    /**
     * Getter to the head parameter
     *
     * @return head
     */
    public Node getHead() {
        return this.head;
    }


    /**
     * Override toString method
     *
     * @return String with heap content
     */
    @Override
    public String toString() {

        if (this.head == null)
            return "Empty heap";

        return head.toString();
    }

    /**
     * Given a key returns the node
     * It uses an hash mao
     *
     * @param key - key in node
     * @return null if key not in heap, otherwise BinomialNode containing the key
     */
    public Node keyToNode(int key) {

        if (this.head == null) {
            return null;
        }

        return this.map.get(key);
    }

    /**
     * Getter of the map
     *
     * @return map
     */
    public HashMap<Integer, Node> getMap() {
        return this.map;
    }

    public List<Integer> getAllKeys() {

        if (this.head == null) {
            return null;
        }

        List<Node> nodeList = new ArrayList<>();
        List<Integer> res = new LinkedList<>();
        res.add(this.head.key);
        nodeList.add(this.head);

        while (nodeList.size() > 0) {

            Node curr = nodeList.remove(0);

            if (curr.sibling != null) {
                nodeList.add(curr.sibling);
                res.add(curr.sibling.key);
            }

            if (curr.child != null) {
                nodeList.add(curr.child);
                res.add(curr.child.key);
            }
        }

        return res;
    }

    /**
     * Linearly searches for a node with a given key
     *
     * @param idx key
     * @return null if key not in heap, otherwise BinomialNode containing the key
     */
    public Node linearKeyToNode(int idx) {

        if (this.head == null) {
            return null;
        }

        List<Node> nodeList = new ArrayList<>();
        nodeList.add(this.head);

        while (nodeList.size() > 0) {

            Node curr = nodeList.remove(0);

            if (curr.key == idx) {
                return curr;
            }

            if (curr.sibling != null) {
                nodeList.add(curr.sibling);
            }

            if (curr.child != null) {
                nodeList.add(curr.child);
            }
        }

        return null;
    }


    /**
     * This method maintains the min heap property
     * Worst case is O(log(n))
     *
     * @param node - node from where we start bubbling up and swapping
     * @param n    - true if we want to assume decrease key to -infinity, false otherwise
     */
    @SuppressWarnings("Duplicates")
    private Node heapifyUp(Node node, boolean n) {

        Node y = node;
        Node z = node.parent;

        while (z != null && (n || this.cmp.compare(y.key, z.key) < 0)) {

            this.swap(y, z);

            y = z;
            z = z.parent;
        }

        return y;
    }

    /**
     * Auxiliary function that swaps the attributes of two nodes
     *
     * @param y - node y
     * @param z - node z
     */
    @SuppressWarnings("Duplicates")
    private void swap(Node y, Node z) {

        int tmp = y.key;
        boolean b = y.remove;

        //update the mapping
        this.map.replace(y.key, z);
        this.map.replace(z.key, y);

        y.key = z.key;
        y.remove = z.remove;

        z.key = tmp;
        z.remove = b;
    }

    /**
     * Deletes the item at position index
     * Worst case is O(log(n))
     *
     * @param idx - idx to be deleted
     * @return deleted idx
     * @throws IllegalStateException if the heap is empty
     */
    @Override
    @SuppressWarnings("Duplicates")
    public void delete(Integer idx) {

        if (this.isEmpty()) {
            throw new IllegalStateException("The heap is empty");
        }

        //System.out.println(this.map);

        Node node = this.keyToNode(idx);
        node.remove = true;

        this.heapifyUp(node, true);
        this.map.remove(idx);

        this.pop();


    }

    private void printRootList() {

        Node current = this.head;
        System.out.print("ROOT list: ");

        while (current != null) {
            System.out.print(" Key : " + current.key + " Remove " + current.remove);
            current = current.sibling;
        }

        System.out.println();
    }

    /**
     * Returns the minimum value of the root list
     * Worst Case is O(log(n) + 1)
     *
     * @return minimum
     */
    @Override
    @SuppressWarnings("Duplicates")
    public Integer top() {

        if (this.isEmpty()) {
            throw new IllegalStateException("The heap is empty");
        }

        Node min = this.head;
        Node next = min.sibling;

        while (next != null) {

            if (this.cmp.compare(min.key, next.key) > 0) {
                min = next;
            }

            next = next.sibling;
        }

        return min.key;
    }

    /**
     * Removes the minimum node from the heap
     * Worst case is O(log n)
     *
     * @return Minimum key
     * @throws IllegalStateException if the heap does not have items
     */
    @Override
    @SuppressWarnings("Duplicates")
    public Integer pop() {

        if (this.head == null) {
            throw new IllegalStateException("The heap is empty");
        }

        Node min = this.head;
        Node next = min.sibling;
        Node nextPrev = min;
        Node minPrev = null;

        while (next != null) {

            if (next.remove) {
                min = next;
                minPrev = nextPrev;
                break;
            }

            if (this.cmp.compare(next.key, min.key) < 0) {
                min = next;
                minPrev = nextPrev;
            }

            nextPrev = next;
            next = next.sibling;

        }

        this.removeNode(min, minPrev);
        this.map.remove(min.key);
        return min.key;
    }


    /**
     * Method removes root from the root list and reverses the points
     * Worst case is O(log(n))
     *
     * @param root - node to be removed
     * @param prev - previous node to be removed
     */
    @SuppressWarnings("Duplicates")
    private void removeNode(Node root, Node prev) {

        // remove root from the heap
        if (root.key == this.head.key) {
            this.head = root.sibling;
        } else {
            prev.sibling = root.sibling;
        }

        //reverse the order of root children
        Node newHead = null;
        Node child = root.child;

        while (child != null) {

            Node next = child.sibling;
            child.sibling = newHead;
            child.parent = null;
            newHead = child;
            child = next;
        }

        BinomialHeap newHeap = new BinomialHeap(newHead, this.cmp);
        this.head = this.unite(newHeap);
        //this.unionSet(newHeap);
    }

    /**
     * Checks if the min heap property is maintained in all elements of the root list
     *
     * @return true if heap
     */
    public boolean minHeapProperty() {

        Node parent = this.head;

        while (parent != null) {

            Node child = parent.child;

            while (child != null) {

                //System.out.println("Parent " + parent.key + " Child " + child.key);

                if (this.cmp.compare(parent.key, child.key) > 0) {
                    return false;
                }


                Node childSibling = child.sibling;


                while (childSibling != null) {

                    //System.out.println("Parent " + parent.key + " Child " + childSibling.key);

                    if (this.cmp.compare(parent.key, childSibling.key) > 0)
                        return false;

                    childSibling = childSibling.sibling;
                }


                child = child.child;
            }


            parent = parent.sibling;
        }


        return true;
    }


    /**
     * Calculates the number of nodes on the heap
     * Just go through the root list and use the property 2^order to calculate the number
     * of nodes on the heap
     * Worst case is O(log(n) + 1)
     *
     * @return number of elements on the heap
     */
    @Override
    @SuppressWarnings("Duplicates")
    public int size() {

        int result = 0;

        Node node = this.head;

        while (node != null) {

            int tmp = 1 << node.degree; //bitwise operations are faster
            result |= tmp;

            node = node.sibling;
        }

        return result;
    }

    /**
     * Clears the heap
     * Worst case is O(1)
     */
    public void clear() {
        this.head = null;
    }

    /**
     * Verifies if heap is empty
     * Worst case is O(1)
     *
     * @return true if heap is empty, otherwise false
     */
    public boolean isEmpty() {
        return this.head == null;
    }

    @Override
    public boolean hasKey(Integer key) {
        return this.map.containsKey(key);
    }

    @Override
    public void setComparator(Comparator<Integer> cmp) {
        this.cmp = cmp;
    }

    @Override
    public Set<Integer> getKeySet() {
        return this.map.keySet();
    }

    /**
     * This method unites two binomial heaps
     * Worst case is O(log(n))
     *
     * @param heap - Binomial Heap
     * @throws IllegalArgumentException if parameter heap is not of type binomial heap
     */
    public void union(Heap<Integer> heap) {

        if (!(heap instanceof BinomialHeap)) {
            throw new IllegalArgumentException("heap must be of type Binomial Heap");
        }

        this.head = this.unite((BinomialHeap) heap);
    }

    /**
     * This method really unites two binomial heaps
     * Worst case is O(log(n))
     *
     * @param heap -  Binomial Heap
     * @return new head
     */
    @SuppressWarnings("Duplicates")
    private Node unite(BinomialHeap heap) {

        Node newHead = this.merge(this, heap);

        this.head = null;
        heap.head = null;

        if (newHead == null) {
            return null;
        }

        Node prev = null;
        Node curr = newHead;
        Node next = newHead.sibling;

        while (next != null) {
            if (curr.degree != next.degree || (next.sibling != null && next.sibling.degree == curr.degree)) {
                prev = curr;
                curr = next;
            } else {
                if (this.cmp.compare(curr.key, next.key) < 0) {
                    curr.sibling = next.sibling;
                    this.link(next, curr);

                } else {
                    if (prev == null) {
                        newHead = next;
                    } else {
                        prev.sibling = next;
                    }

                    this.link(curr, next);
                    curr = next;
                }
            }

            next = curr.sibling;
        }


        for (Map.Entry<Integer, Node> entry : heap.map.entrySet()) {

            int key = entry.getKey();
            Node node = entry.getValue();
            this.map.put(key, new Node(node));
        }

        return newHead;
    }

    /**
     * Links two nodes on the heap
     * Worst case is O(1)
     *
     * @param y node y
     * @param z node z
     */
    @SuppressWarnings("Duplicates")
    private void link(Node y, Node z) {
        y.parent = z;
        y.sibling = z.child;
        z.child = y;
        z.degree++;
    }

    /**
     * This method unites the root list of heap1 and heap2 that is sorted by degree monotonically increasing order
     *
     * @param heap1 Binomial Heap
     * @param heap2 other binomial heap
     * @return new head node
     */
    @SuppressWarnings("Duplicates")
    private Node merge(BinomialHeap heap1, BinomialHeap heap2) {
        if (heap1.head == null) {
            return heap2.head;
        } else if (heap2.head == null) {
            return heap1.head;
        } else {

            Node head;
            Node heap1Next = heap1.head;
            Node heap2Next = heap2.head;

            if (heap1.head.degree <= heap2.head.degree) {
                head = heap1.head;
                heap1Next = heap1Next.sibling;
            } else {

                head = heap2.head;
                heap2Next = heap2Next.sibling;
            }

            Node tail = head;

            while (heap1Next != null && heap2Next != null) {
                if (heap1Next.degree <= heap2Next.degree) {
                    tail.sibling = heap1Next;
                    heap1Next = heap1Next.sibling;
                } else {
                    tail.sibling = heap2Next;
                    heap2Next = heap2Next.sibling;
                }

                tail = tail.sibling;
            }

            if (heap1Next != null) {
                tail.sibling = heap1Next;
            } else {
                tail.sibling = heap2Next;
            }

            return head;
        }
    }
}
