package net.phyloviz.immutable.queue;

public enum HeapType {BINOMIAL, BINARY, FAST_RANK_RELAXED, JAVA_BINARY_HEAP}
