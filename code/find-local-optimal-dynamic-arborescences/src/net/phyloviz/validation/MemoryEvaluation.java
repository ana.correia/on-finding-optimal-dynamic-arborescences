package net.phyloviz.validation;

import net.phyloviz.Arborescence;
import net.phyloviz.comparators.Comparators;
import net.phyloviz.dynamic.algo.arborescence.ATree;
import net.phyloviz.dynamic.algo.arborescence.BlackBox;
import net.phyloviz.dynamic.algo.arborescence.DynamicArborescence;
import net.phyloviz.graph.DirectedGraph;
import net.phyloviz.graph.WeightedEdge;
import net.phyloviz.immutable.algo.arborescence.CameriniExpansion;
import net.phyloviz.immutable.algo.arborescence.OriginalImpl;
import net.phyloviz.immutable.queue.HeapType;
import net.phyloviz.io.Directory;
import net.phyloviz.io.IO;
import net.phyloviz.io.PhylogeneticIO;
import net.phyloviz.phylogenetic.matrix.DirectedPairWiseMatrix;
import net.phyloviz.phylogenetic.otu.OTU;
import net.phyloviz.util.ArboEnum;

import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.*;

public class MemoryEvaluation {

    private static final Runtime runtime = Runtime.getRuntime();
    private static final MemoryMXBean mxBean = ManagementFactory.getMemoryMXBean();
    private static final double  MEGABYTE = 1024 * 1024;


    public static double bytesToMeg(double bytes) {

        double v = bytes / MEGABYTE;

        DecimalFormat df = new DecimalFormat("0.00");
        String format = df.format(v);

        try {
            v = df.parse(format).doubleValue();
        } catch (ParseException e) {
            System.err.println(v);
        }

        return v;
    }


    private static long getGcCount() throws Exception {
        long sum = 0;

        for (GarbageCollectorMXBean b : ManagementFactory.getGarbageCollectorMXBeans()) {
            long count = b.getCollectionCount();

            if (count == -1)
                throw new Exception("No garbage collector online");

            sum +=  count;
        }

        return sum;
    }

    private static void waitGC() throws Exception {
        long before = getGcCount();
        System.gc();
        while (getGcCount() == before);
    }

    public static void main(String[] args) throws Exception {

        String parse = args[0];
        switch(parse)
        {
            case "memoryPhyloPolynomial":
                memoryPhyloPolynomial();
                break;
            case "memoryPhylo":
                memoryPhylo();
                break;
            case "memoryStaticRemove":
                memoryStaticRemove(args);
                break;
            case "memoryDynamicRemove":
                memoryDynamicRemove(args);
                break;
            case "memoryStaticUpdate":
                memoryStaticUpdate(args);
                break;
            case "memoryDynamicUpdate":
                memoryDynamicUpdate(args);
                break;
            default:
                System.out.println("no match");
                break;
        }
        //memoryPhylo();
        //memoryDynamicRemove();
        //waitGC();
        //memoryStaticRemove();
        //memoryPhyloPolynomial();
    }


    private static void memoryPhylo() throws Exception{

        String dir = Directory.PHYLOGENETIC_DATASET;
        int rep = 5;

        String [] dataset = new String[]{"clostridium.Griffiths.list", "Moraxella.Achtman7GeneMLST.list", "Salmonella.Achtman7GeneMLST.list", "Yersinia.cgMLSTv1.list", "Yersinia.wgMLST.list"};
        String [] names = new String[]{"clostridium.Griffiths", "Moraxella.Achtman7GeneMLST" , "Salmonella.Achtman7GeneMLST","Yersinia.cgMLSTv1", "Yersinia.wgMLST"};

        for (int i = 0; i < dataset.length; i++){
            String f = dir + dataset[i];
            String f_name = Directory.EDMONDS_EVAL + names[i];
            System.out.println(f_name);
            waitGC();
            memoryPhyloUtil(f, rep, f_name);
        }
    }

    private static void memoryPhyloPolynomial() throws Exception {
        String dataset = Directory.PHYLOGENETIC_DATASET + "clostridium.Griffiths.list";
        String f_name = "clostridium.Griffiths";
        int rep = 5;
        memoryPhyloPolynomialUtil(dataset, rep, f_name);
    }


    @SuppressWarnings("Duplicates")
    private static void memoryPhyloPolynomialUtil(String dataset, int rep, String fileName) throws Exception {

        DirectedGraph G = null;
        List<OTU> mainOtusLst = PhylogeneticIO.parseFile(dataset);
        float floatSize = (float) mainOtusLst.size();
        TreeMap<Integer, Double> map2 = new TreeMap<>();

        for (float i = 0.1F; i <= 1.1; i = i + 0.1F) {

            int split = (int) (floatSize * i);
            List<OTU> subList = mainOtusLst.subList(0, split);
            DirectedPairWiseMatrix directedPairWiseMatrix = new DirectedPairWiseMatrix(subList);
            System.out.println(" memoryPhyloPolynomial - i: " + i);

            long sum_mx = 0;

            waitGC();

            for (int j = 0; j < rep ; j++) {

                G = directedPairWiseMatrix.matrixToDirectedGraph();

                Arborescence edmonds = new OriginalImpl(G, Comparators.totalOrderCmp());

                edmonds.getArborescence();

                waitGC();
                long after_mx = mxBean.getHeapMemoryUsage().getUsed() + mxBean.getNonHeapMemoryUsage().getUsed();
                sum_mx += after_mx;
            }

            double avg_mx = sum_mx / rep;

            assert G != null;

            int E = G.nEdges();
            int V = G.size();
            int VE = V + E;
            map2.put(VE, bytesToMeg(avg_mx));

            System.out.println("RuntimeMX consumed Memory:" + bytesToMeg(avg_mx));

            System.out.println("========================");
        }

        IO.writeMapIntegerDouble(map2, fileName + "non_mem.txt");
    }

    @SuppressWarnings("Duplicates")
    private static void memoryPhyloUtil(String dataset, int rep, String fileName) throws Exception {

        DirectedGraph G = null;
        List<OTU> mainOtusLst = PhylogeneticIO.parseFile(dataset);
        float floatSize = (float) mainOtusLst.size();
        TreeMap<Integer, Double> map = new TreeMap<>();
        TreeMap<Integer, Double> map2 = new TreeMap<>();

        for (float i = 0.1F; i <= 1.1; i = i + 0.1F) {

            int split = (int) (floatSize * i);
            List<OTU> subList = mainOtusLst.subList(0, split);
            DirectedPairWiseMatrix directedPairWiseMatrix = new DirectedPairWiseMatrix(subList);
            System.out.println("memoryPhylo i: " + i);

            long sum = 0;
            long sum_mx = 0;

            waitGC();

            for (int j = 0; j < rep ; j++) {

                G = directedPairWiseMatrix.matrixToDirectedGraph();

                Arborescence edmonds = new CameriniExpansion(G, Comparators.totalOrderCmp(), HeapType.BINOMIAL,
                        true, true, ArboEnum.FASTER_ARBO_DISJOINT_SET);

                edmonds.getArborescence();

                waitGC();
                long after_mx = mxBean.getHeapMemoryUsage().getUsed() + mxBean.getNonHeapMemoryUsage().getUsed();
                long after_memory = runtime.totalMemory() - runtime.freeMemory();

                sum += after_memory;
                sum_mx += after_mx;
            }

            double avg_mem = sum / rep;
            double avg_mx = sum_mx / rep;

            assert G != null;

            int E = G.nEdges();
            int V = G.size();
            int VE = V + E;
            map.put(VE, bytesToMeg(avg_mem));
            map2.put(VE, bytesToMeg(avg_mx));

            System.out.println("Runtime consumed Memory:" + bytesToMeg(avg_mem));
            System.out.println("RuntimeMX consumed Memory:" + bytesToMeg(avg_mx));

            System.out.println("========================");
        }

        IO.writeMapIntegerDouble(map, fileName + "_mem.txt");
        IO.writeMapIntegerDouble(map2, fileName + "_mx.txt");

    }

    private static void memoryDynamicRemove(String[] args) throws Exception {

        String dir = Directory.PHYLOGENETIC_DATASET;
        int rep = 3;
        int n = 5;

        //String [] dataset = new String[]{"clostridium.Griffiths.list", "Moraxella.Achtman7GeneMLST.list", "Yersinia.cgMLSTv1.list", "Yersinia.wgMLST.list","Salmonella.Achtman7GeneMLST.list"};
        //String [] names = new String[]{"clostridium.Griffiths", "Moraxella.Achtman7GeneMLST" ,"Yersinia.cgMLSTv1", "Yersinia.wgMLST","Salmonella.Achtman7GeneMLST"};

        String[] dataset = new String[]{"clostridium.Griffiths.list", "Moraxella.Achtman7GeneMLST.list", "Escherichia.Achtman7GeneMLST.list", "Yersinia.McNally.list", "Salmonella.Achtman7GeneMLST.list"};
        String[] names = new String[]{"clostridium.Griffiths", "Moraxella.Achtman7GeneMLST", "Escherichia.Achtman7GeneMLST", "Yersinia.McNally", "Salmonella.Achtman7GeneMLST"};

        if (args.length > 1) {

            String parse = args[1];
            switch (parse) {
                case "c":
                    dataset = new String[]{"clostridium.Griffiths.list"};
                    names = new String[]{"clostridium.Griffiths"};
                    rep = 5;
                    break;
                case "e":
                    dataset = new String[]{"Escherichia.Achtman7GeneMLST.list"};
                    names = new String[]{"Escherichia.Achtman7GeneMLST"};
                    rep = 1;
                    break;
                case "m":
                    dataset = new String[]{"Moraxella.Achtman7GeneMLST.list"};
                    names = new String[]{"Moraxella.Achtman7GeneMLST"};
                    rep = 5;
                    break;
                case "s":
                    dataset = new String[]{"Salmonella.Achtman7GeneMLST.list"};
                    names = new String[]{"Salmonella.Achtman7GeneMLST"};
                    rep = 5;
                    break;
                case "y":
                    dataset = new String[]{"Yersinia.McNally.list"};
                    names = new String[]{"Yersinia.McNally"};
                    rep = 5;
                    break;
            }
        }


        for (int i = 0; i < dataset.length; i++){
            String f =  dir + dataset[i];
            String f_name = Directory.EDMONDS_EVAL + names[i];
            System.out.println(f_name);
            waitGC();
            memoryDynamicRemoveUtil(f, rep, n, f_name);
            waitGC();
        }
    }

    @SuppressWarnings("Duplicates")
    private static void memoryDynamicRemoveUtil(String dataset, int rep, int n, String fileName) throws Exception {

        DirectedGraph G = null;
        List<OTU> mainOtusLst = PhylogeneticIO.parseFile(dataset);
        float floatSize = (float) mainOtusLst.size();
        TreeMap<Integer, Double> map = new TreeMap<>();

        for (float i = 0.1F; i <= 1.1; i = i + 0.1F) {

            Random rand = new Random();
            int split = (int) (floatSize * i);
            List<OTU> subList = mainOtusLst.subList(0, split);
            DirectedPairWiseMatrix directedPairWiseMatrix = new DirectedPairWiseMatrix(subList);
            System.out.println("i: " + i);

            double mx = 0;
            for (int j = 0; j < rep; j++) {

                G = directedPairWiseMatrix.matrixToDirectedGraph();

                BlackBox blackBox = new BlackBox(G, Comparators.totalOrderCmp());
                blackBox.getArborescence();

                ATree aTree = blackBox.getATree();
                List<WeightedEdge> edgeList = new ArrayList<>(aTree.getEdgesInTree());
                // Collections.shuffle(edgeList);
                List<WeightedEdge> sublist = edgeList.subList(0,n);
                DynamicArborescence dynamicArborescence = new DynamicArborescence(aTree, Comparators.totalOrderCmp(), new DirectedGraph(G));

                waitGC();
                long m = 0;
                int k = 0;
                for (int l = 0; l < n; l++){
                    WeightedEdge e = edgeList.get(rand.nextInt(edgeList.size()));

                // for (WeightedEdge e: sublist){

                    if (e == null)
                        continue;

                    dynamicArborescence.removeEdge(e);
                    waitGC();
                    long after_mx = mxBean.getHeapMemoryUsage().getUsed() + mxBean.getNonHeapMemoryUsage().getUsed();
                    m = m + after_mx;
                    k++;
                }

                double avg_m = m/k;
                mx += avg_m;
            }

            double avg_mx = (double) mx / rep;
            assert G != null;
            int V = G.size();
            int E = G.nEdges();
            double mb = bytesToMeg(avg_mx);
            map.put((V+E), mb);
            System.out.println("RuntimeMX consumed Memory:" + mb);
        }

        IO.writeMapIntegerDouble(map, fileName + "_dynamic_remove_mx.txt");
    }

    @SuppressWarnings("Duplicates")
    private static void memoryStaticRemove(String[] args) throws Exception {

        String dir = Directory.PHYLOGENETIC_DATASET;
        int rep = 3;
        int n = 5;
        //String [] dataset = new String[]{"clostridium.Griffiths.list", "Moraxella.Achtman7GeneMLST.list", "Yersinia.cgMLSTv1.list", "Yersinia.wgMLST.list","Salmonella.Achtman7GeneMLST.list"};
        //String [] names = new String[]{"clostridium.Griffiths", "Moraxella.Achtman7GeneMLST" ,"Yersinia.cgMLSTv1", "Yersinia.wgMLST","Salmonella.Achtman7GeneMLST"};

        String[] dataset = new String[]{"clostridium.Griffiths.list", "Moraxella.Achtman7GeneMLST.list", "Escherichia.Achtman7GeneMLST.list", "Yersinia.McNally.list", "Salmonella.Achtman7GeneMLST.list"};
        String[] names = new String[]{"clostridium.Griffiths", "Moraxella.Achtman7GeneMLST", "Escherichia.Achtman7GeneMLST", "Yersinia.McNally", "Salmonella.Achtman7GeneMLST"};

        if (args.length > 1) {

            String parse = args[1];
            switch (parse) {
                case "c":
                    dataset = new String[]{"clostridium.Griffiths.list"};
                    names = new String[]{"clostridium.Griffiths"};
                    rep = 5;
                    break;
                case "e":
                    dataset = new String[]{"Escherichia.Achtman7GeneMLST.list"};
                    names = new String[]{"Escherichia.Achtman7GeneMLST"};
                    rep = 1;
                    break;
                case "m":
                    dataset = new String[]{"Moraxella.Achtman7GeneMLST.list"};
                    names = new String[]{"Moraxella.Achtman7GeneMLST"};
                    rep = 5;
                    break;
                case "s":
                    dataset = new String[]{"Salmonella.Achtman7GeneMLST.list"};
                    names = new String[]{"Salmonella.Achtman7GeneMLST"};
                    rep = 5;
                    break;
                case "y":
                    dataset = new String[]{"Yersinia.McNally.list"};
                    names = new String[]{"Yersinia.McNally"};
                    rep = 5;
                    break;
            }
        }
        for (int i = 0; i < dataset.length; i++){
            String f =  dir + dataset[i];
            System.out.println(f);
            String f_name = Directory.EDMONDS_EVAL + names[i];
            System.out.println(f_name);
            waitGC();
            memoryStaticRemoveUtil(f, rep, n, f_name);
        }

    }

    @SuppressWarnings("Duplicates")
    private static void memoryStaticRemoveUtil(String dataset, int rep, int n, String fileName) throws Exception {

        Random rand = new Random();
        DirectedGraph G = null;
        List<OTU> mainOtusLst = PhylogeneticIO.parseFile(dataset);
        float floatSize = (float) mainOtusLst.size();
        TreeMap<Integer, Double> map2 = new TreeMap<>();

        for (float i = 0.1F; i <= 1.1; i = i + 0.1F) {

            int split = (int) (floatSize * i);
            List<OTU> subList = mainOtusLst.subList(0, split);
            DirectedPairWiseMatrix directedPairWiseMatrix = new DirectedPairWiseMatrix(subList);

            G = directedPairWiseMatrix.matrixToDirectedGraph();
            Arborescence edmonds = new CameriniExpansion(G, Comparators.totalOrderCmp(), HeapType.BINOMIAL, true, true, ArboEnum.FASTER_ARBO_DISJOINT_SET);
            G.reverseGraph();
            List<WeightedEdge> edgeList = edmonds.getArborescence();
            // Collections.shuffle(edgeList);
            System.out.println("memoryStaticRemove i: " + i);
            long sum_mx = 0;
            waitGC();
            for (int j = 0; j < rep ; j++) {

                List<WeightedEdge> subEdgeList = edgeList.subList(0,n);

                waitGC();

                long m = 0;
                int k = 0;
                for (int l = 0; l < n; l++) {
                    WeightedEdge e = edgeList.get(rand.nextInt(edgeList.size()));

                // for (WeightedEdge e: subEdgeList) {
                    G.removeEdge(e);
                    edmonds = new CameriniExpansion(G, Comparators.totalOrderCmp(), HeapType.BINOMIAL, true, true, ArboEnum.FASTER_ARBO_DISJOINT_SET);
                    edmonds.getArborescence();
                    G.reverseGraph();

                    waitGC();
                    long after_mx = mxBean.getHeapMemoryUsage().getUsed() + mxBean.getNonHeapMemoryUsage().getUsed();
                    m += after_mx;
                    k++;
                }
                double avg_m = m/k;
                sum_mx += avg_m;
            }

            double avg_mx = sum_mx / rep;
            int E = G.nEdges();
            int V = G.size();
            int VE = V + E;
            map2.put(VE, bytesToMeg(avg_mx));
            System.out.println("RuntimeMX consumed Memory:" + bytesToMeg(avg_mx));
            System.out.println("========================");
        }
        IO.writeMapIntegerDouble(map2, fileName + "_static_mx.txt");
    }


    private static void memoryDynamicUpdate(String[] args) throws Exception {

        String dir = Directory.PHYLOGENETIC_DATASET;
        int[] reps = new int[]{5, 5, 1, 5, 5};
        int n = 5;

        //String [] dataset = new String[]{"clostridium.Griffiths.list", "Moraxella.Achtman7GeneMLST.list", "Yersinia.cgMLSTv1.list", "Yersinia.wgMLST.list"};//, "Salmonella.Achtman7GeneMLST.list", "Yersinia.cgMLSTv1.list", "Yersinia.wgMLST.list"};
        //String [] names = new String[]{"clostridium.Griffiths", "Moraxella.Achtman7GeneMLST", "Yersinia.cgMLSTv1", "Yersinia.wgMLST"};// , "Salmonella.Achtman7GeneMLST","Yersinia.cgMLSTv1", "Yersinia.wgMLST"};
        String[] dataset = new String[]{"clostridium.Griffiths.list", "Moraxella.Achtman7GeneMLST.list", "Escherichia.Achtman7GeneMLST.list", "Yersinia.McNally.list", "Salmonella.Achtman7GeneMLST.list"};
        String[] names = new String[]{"clostridium.Griffiths", "Moraxella.Achtman7GeneMLST", "Escherichia.Achtman7GeneMLST", "Yersinia.McNally", "Salmonella.Achtman7GeneMLST"};

        if (args.length > 1) {

            String parse = args[1];
            switch (parse) {
                case "c":
                    dataset = new String[]{"clostridium.Griffiths.list"};
                    names = new String[]{"clostridium.Griffiths"};
                    reps = new int[]{5};
                    break;
                case "e":
                    dataset = new String[]{"Escherichia.Achtman7GeneMLST.list"};
                    names = new String[]{"Escherichia.Achtman7GeneMLST"};
                    reps = new int[]{1};
                    break;
                case "m":
                    dataset = new String[]{"Moraxella.Achtman7GeneMLST.list"};
                    names = new String[]{"Moraxella.Achtman7GeneMLST"};
                    reps = new int[]{5};
                    break;
                case "s":
                    dataset = new String[]{"Salmonella.Achtman7GeneMLST.list"};
                    names = new String[]{"Salmonella.Achtman7GeneMLST"};
                    reps = new int[]{5};
                    break;
                case "y":
                    dataset = new String[]{"Yersinia.McNally.list"};
                    names = new String[]{"Yersinia.McNally"};
                    reps = new int[]{5};
                    break;
            }
        }

        for (int i = 0; i < dataset.length; i++){
            String f =  dir + dataset[i];
            String f_name = Directory.EDMONDS_EVAL + names[i];
            System.out.println("Memory dynamic file " + f_name);
            waitGC();
            memoryDynamicUpdateUtil(f, reps[i], n, f_name);
            waitGC();
        }
    }

    @SuppressWarnings("Duplicates")
    private static void memoryDynamicUpdateUtil(String dataset, int rep, int n, String fileName) throws Exception {

        DirectedGraph G = null;
        List<OTU> mainOtusLst = PhylogeneticIO.parseFile(dataset);
        float floatSize = (float) mainOtusLst.size();
        TreeMap<Integer, Double> map = new TreeMap<>();

        for (float i = 0.1F; i <= 1.1; i = i + 0.1F) {

            int split = (int) (floatSize * i);
            List<OTU> subList = mainOtusLst.subList(0, split);
            DirectedPairWiseMatrix directedPairWiseMatrix = new DirectedPairWiseMatrix(subList);
            System.out.println("i: " + i);

            double sum_mx = 0;
            for (int j = 0; j < rep; j++) {
                Random rand = new Random();
                G = directedPairWiseMatrix.matrixToDirectedGraph();

                BlackBox blackBox = new BlackBox(G, Comparators.totalOrderCmp());
                blackBox.getArborescence();

                ATree aTree = blackBox.getATree();
                List<WeightedEdge> edgeList = new ArrayList<>(aTree.getEdgesInTree());
                // Collections.shuffle(edgeList);
                List<WeightedEdge> sublist = edgeList.subList(0,n);
                DynamicArborescence dynamicArborescence = new DynamicArborescence(aTree, Comparators.totalOrderCmp(), new DirectedGraph(G));

                waitGC();
                long m = 0;
                int k = 0;
                for (int l = 0; l < n; l++){
                    WeightedEdge edge = edgeList.get(rand.nextInt(edgeList.size()));

                    if (edge == null)
                        continue;

                    edgeList = dynamicArborescence.updateCost(edge, Integer.MAX_VALUE);
                    waitGC();
                    long after_mx = mxBean.getHeapMemoryUsage().getUsed() + mxBean.getNonHeapMemoryUsage().getUsed();
                    m = m + after_mx;
                    k++;
                }
                double avg_m = m/k;
                sum_mx += avg_m;
            }

            double avg_mx = (double) sum_mx / rep;
            assert G != null;
            int V = G.size();
            int E = G.nEdges();
            double mb = bytesToMeg(avg_mx);
            map.put((V+E), mb);
            System.out.println("RuntimeMX consumed Memory:" + mb);
            IO.writeMapIntegerDouble(map, fileName + "_dynamic_update_mx.txt");

        }

        IO.writeMapIntegerDouble(map, fileName + "dynamic_update_mx.txt");
    }

    @SuppressWarnings("Duplicates")
    private static void memoryStaticUpdate(String[] args) throws Exception {

        String dir = Directory.PHYLOGENETIC_DATASET;
        int rep = 5;
        int n = 5;
        String[] dataset = new String[]{"clostridium.Griffiths.list", "Moraxella.Achtman7GeneMLST.list", "Escherichia.Achtman7GeneMLST.list", "Yersinia.McNally.list", "Salmonella.Achtman7GeneMLST.list"};
        String[] names = new String[]{"clostridium.Griffiths", "Moraxella.Achtman7GeneMLST", "Escherichia.Achtman7GeneMLST", "Yersinia.McNally","Salmonella.Achtman7GeneMLST"};

        if (args.length > 1) {

            String parse = args[1];
            switch (parse) {
                case "c":
                    dataset = new String[]{"clostridium.Griffiths.list"};
                    names = new String[]{"clostridium.Griffiths"};
                    break;
                case "e":
                    dataset = new String[]{"Escherichia.Achtman7GeneMLST.list"};
                    names = new String[]{"Escherichia.Achtman7GeneMLST"};
                    break;
                case "m":
                    dataset = new String[]{"Moraxella.Achtman7GeneMLST.list"};
                    names = new String[]{"Moraxella.Achtman7GeneMLST"};
                    break;
                case "s":
                    dataset = new String[]{"Salmonella.Achtman7GeneMLST.list"};
                    names = new String[]{"Salmonella.Achtman7GeneMLST"};
                    break;
                case "y":
                    dataset = new String[]{"Yersinia.McNally.list"};
                    names = new String[]{"Yersinia.McNally"};
                    break;
            }
        }

        for (int i = 0; i < dataset.length; i++){
            String f =  dir + dataset[i];
            String f_name = Directory.EDMONDS_EVAL + names[i];
            System.out.println(f_name);
            waitGC();
            memoryStaticUpdateUtil(f, rep, n, f_name);
        }

    }

    @SuppressWarnings("Duplicates")
    private static void memoryStaticUpdateUtil(String dataset, int rep, int n, String fileName) throws Exception {

        Random rand = new Random();
        DirectedGraph G = null;
        List<OTU> mainOtusLst = PhylogeneticIO.parseFile(dataset);
        float floatSize = (float) mainOtusLst.size();
        TreeMap<Integer, Double> map2 = new TreeMap<>();

        for (float i = 0.1F; i <= 1.1; i = i + 0.1F) {

            int split = (int) (floatSize * i);
            List<OTU> subList = mainOtusLst.subList(0, split);
            DirectedPairWiseMatrix directedPairWiseMatrix = new DirectedPairWiseMatrix(subList);

            G = directedPairWiseMatrix.matrixToDirectedGraph();
            Arborescence edmonds = new CameriniExpansion(G, Comparators.totalOrderCmp(), HeapType.BINOMIAL, true, true, ArboEnum.FASTER_ARBO_DISJOINT_SET);
            G.reverseGraph();
            List<WeightedEdge> edgeList = edmonds.getArborescence();
            //Collections.shuffle(edgeList);
            System.out.println("memoryStaticUpdate i: " + i);
            long sum_mx = 0;
            waitGC();
            for (int j = 0; j < rep ; j++) {

                List<WeightedEdge> subEdgeList = edgeList.subList(0,n);

                waitGC();
                long m = 0;
                int k = 0;
                for (int l = 0; l < n; l++) {
                    WeightedEdge edge = edgeList.get(rand.nextInt(edgeList.size()));

                    if ((edge == null) ||( edge.getWeight() == Integer.MAX_VALUE))
                        continue;

                    G.addEdge(edge.getSrc(), edge.getDest(), Integer.MAX_VALUE);
                    G.removeEdge(edge);
                    edmonds = new CameriniExpansion(G, Comparators.totalOrderCmp(), HeapType.BINOMIAL, true, true, ArboEnum.FASTER_ARBO_DISJOINT_SET);
                    edgeList = edmonds.getArborescence();
                    G.reverseGraph();

                    waitGC();
                    long after_mx = mxBean.getHeapMemoryUsage().getUsed() + mxBean.getNonHeapMemoryUsage().getUsed();
                    m += after_mx;
                    k ++;

                }
                double avg_m = m/k;
                sum_mx += avg_m;
            }

            double avg_mx = (double) sum_mx / rep;
            int E = G.nEdges();
            int V = G.size();
            int VE = V + E;
            map2.put(VE, bytesToMeg(avg_mx));
            System.out.println("RuntimeMX consumed Memory:" + bytesToMeg(avg_mx));
            System.out.println("========================");
            IO.writeMapIntegerDouble(map2, fileName + "_static_update_mx.txt");

        }
        IO.writeMapIntegerDouble(map2, fileName + "_static_update_mx.txt");
    }
    /*
    List<MemoryPoolMXBean> pools = ManagementFactory.getMemoryPoolMXBeans();
    long total = 0;
            for (MemoryPoolMXBean memoryPoolMXBean : pools)
    {
        if (memoryPoolMXBean.getType() == MemoryType.HEAP)
        {
            long peakUsed = memoryPoolMXBean.getPeakUsage().getUsed();
            System.out.println("Peak used for: " + memoryPoolMXBean.getName() + " is: " + peakUsed);
            total = total + peakUsed;
        }
    }
     */
}
