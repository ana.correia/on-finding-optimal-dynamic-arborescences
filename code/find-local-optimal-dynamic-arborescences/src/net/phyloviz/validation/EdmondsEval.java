package net.phyloviz.validation;

import net.phyloviz.immutable.algo.arborescence.EdmondsEnum;
import net.phyloviz.io.Directory;
import net.phyloviz.immutable.queue.HeapType;
import net.phyloviz.io.IO;
import net.phyloviz.util.HomogenousGraph;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class EdmondsEval {


    public static void main(String[] args) throws IOException {

        //TODO ADD TESTS WITH FAST RANKED RELAXED but union not yet implemented
        //edmondsV3();
        //cacheMisses();
        //binaryEvaluation();
        //inneficientEdmonds();
        //phylogeneticEvaluation();
        //primVsEdmonds();
        //edmondsSlow();
        String parse = args[0];
        switch(parse) {
            case "union":
                UnionRandomGraphs();
                break;
            case "meld":
                binaryEvalRandomGraph();
                break;
            case "primVSedmondsRand":
                primVsEdmondsRandGraph();
            break;
            case "primVSedmondsReal":
                primVsEdmonds();
                break;
            case "heap":
                EdmondsRandomGraphs();
                break;
            default:
                System.out.println("no match");
                break;
        }
    }

    private static void primVsEdmonds() throws IOException {

        int[] rep = new int[]{10,5,3};
        String [] dataset = new String[]{"clostridium.Griffiths.list", "Moraxella.Achtman7GeneMLST.list", "Salmonella.Achtman7GeneMLST.list"};
        String [] names = new String[]{"clostridium.Griffiths", "Moraxella.Achtman7GeneMLST" , "Salmonella.Achtman7GeneMLST"};


        for (int i = 0; i < rep.length; i++) {
            int r = rep[i];
            String s = dataset[i];
            String n = names[i];
            Validation.primEdmondsPhyloEval(Directory.PHYLOGENETIC_DATASET + s, r, n);
        }

    }

    private static void primVsEdmondsRandGraph() throws IOException {
        int repetitions = 5;
        int numberNodes = 1000;
        int[] degree = new int[]{100, 200, 300, 400, 500, 600, 700, 800, 900, 999};
        int nbDiffGraphs = 5;

        Map<Double, Double> primMap = new TreeMap<>();
        Map<Double, Double> edmondsMap = new TreeMap<>();
        System.out.println("--------Prim vs Edmonds Rand graphs ---------");

        for (int i = 0; i < degree.length; i++) {
            int d = degree[i];

            System.out.println("degree " + d);
            for (int j = 0; j < nbDiffGraphs; j ++) {
                HomogenousGraph exp = new HomogenousGraph(numberNodes, d);
                //System.out.println(exp.toString());
                Validation.runPrimEdmondPhyloEvalUtil(repetitions, primMap, edmondsMap, exp.graphUndir, exp.graphDir);
            }
        }

        String fileName = Directory.PRIM_EVAL_RAND + numberNodes;
        IO.writeMapDoubleDouble(edmondsMap, fileName + "_edmonds.txt");
        IO.writeMapDoubleDouble(primMap, fileName + "_prim.txt");
    }


    private static void phylogeneticEvaluation() throws IOException{

        int repetitions = 5;
        //String datasetName = "Yersinia.cgMLSTv1.list";
        String datasetName = "Moraxella.Achtman7GeneMLST.list";
        String dir = Directory.PHYLOGENETIC_DATASET + datasetName;
        //Validation.philogeneticEvaluation(dir, repetitions, Directory.EDMONDS_EVAL + "Yersinia.cgMLSTv1");
        Validation.philogeneticEvaluation(dir, repetitions, Directory.EDMONDS_EVAL + "Moraxella.Achtman7GeneMLST");
    }

    private static void inneficientEdmonds() throws IOException {

        int repetitions = 5;

        String dir = Directory.SCALE_FREE;
        System.out.println("SCALE FREE INEFFICIENT");
        Validation.runEdmondsEvaluation(dir, repetitions, Directory.EDMONDS_EVAL + "inefficient_scale_free", null, EdmondsEnum.INEFFICIENT);

        dir = Directory.SPARSE_GRAPH;
        System.out.println("SPARSE INEFFICIENT");
        Validation.runEdmondsEvaluation(dir, repetitions, Directory.EDMONDS_EVAL + "inefficient_sparse", null, EdmondsEnum.INEFFICIENT);

        dir = Directory.DIRECTED_COMPLETE;
        System.out.println("COMPLETE INEFFICIENT");

        Validation.runEdmondsEvaluation(dir, repetitions, Directory.EDMONDS_EVAL + "inefficient_complete", null, EdmondsEnum.INEFFICIENT);
    }

    private static void cacheMisses() throws IOException {

        int repetitions = 5;
        String directory = Directory.DIRECTED_COMPLETE;
        System.out.println("Cache miss complete graph ");
        Validation.cacheMissesEvaluation(directory, repetitions, Directory.EDMONDS_EVAL + "cache_miss_complete");

        System.out.println("Cache miss sparse graph ");
        directory = Directory.SPARSE_GRAPH;
        Validation.cacheMissesEvaluation(directory, repetitions, Directory.EDMONDS_EVAL + "cache_miss_sparse");

    }

    private static void binaryEvaluation() throws IOException {

        int repetitions = 5;
        String directory = Directory.DIRECTED_COMPLETE;
        System.out.println("Binary Union Evaluation complete graph");
        Validation.binaryFloydUnion(directory, repetitions, Directory.EDMONDS_EVAL + "union_complete");

        directory = Directory.SPARSE_GRAPH;
        System.out.println("Binary Union Evaluation sparse graph");
        Validation.binaryFloydUnion(directory, repetitions, Directory.EDMONDS_EVAL + "union_sparse");
    }

    private static void binaryEvalRandomGraph() throws IOException {
        int repetitions = 5;
        int numberNodes = 1000;
        int[] degree = new int[]{100, 200, 300, 400, 500, 600, 700, 800, 900, 999};
        int nbDiffGraphs = 5;
        System.out.println("--------- MELD eval random graphs -------");

        Map<Double, Double> timesFloyd = new TreeMap<>();
        Map<Double, Double> timesInsert = new TreeMap<>();

        for (int i = 0; i < degree.length; i++) {
            int d = degree[i];
            System.out.println("degree " + d);
            for (int j = 0; j < nbDiffGraphs; j ++) {
                HomogenousGraph exp = new HomogenousGraph(numberNodes, d);
                Validation.binaryFloydUnionUtil(repetitions, exp.graphDir, timesFloyd, timesInsert);
            }
        }

        String fileName = Directory.EDMONDS_COMP_RAND + numberNodes;
        IO.writeMapDoubleDouble(timesFloyd, fileName + "_meld_floyd.txt");
        IO.writeMapDoubleDouble(timesInsert, fileName + "_meld_insert.txt");
    }

    private static void edmondsSlow() throws IOException {

        int repetitions = 5;
        String dir = Directory.SCALE_FREE;
        System.out.println("Running Scale free Binary");
        Validation.runEdmondsEvaluation(dir, repetitions, Directory.EDMONDS_EVAL + "scale_free_test_binary_disj", HeapType.BINARY, EdmondsEnum.EFFICIENT_DISJOINT_SET);
        System.out.println("Running Scale free Binomial");
        Validation.runEdmondsEvaluation(dir, repetitions, Directory.EDMONDS_EVAL + "scale_free_test_binomial_disj", HeapType.BINOMIAL, EdmondsEnum.EFFICIENT_DISJOINT_SET);

        dir = Directory.SPARSE_GRAPH;
        System.out.println("Running Sparse Binary");
        Validation.runEdmondsEvaluation(dir, repetitions, Directory.EDMONDS_EVAL + "sparse_test_binary_disj", HeapType.BINARY, EdmondsEnum.EFFICIENT_DISJOINT_SET);
        System.out.println("Running Sparse Binomial");
        Validation.runEdmondsEvaluation(dir, repetitions, Directory.EDMONDS_EVAL + "sparse_test_binomial_disj", HeapType.BINOMIAL, EdmondsEnum.EFFICIENT_DISJOINT_SET);

        dir = Directory.DIRECTED_COMPLETE;
        System.out.println("Running Complete Binary");
        Validation.runEdmondsEvaluation(dir, repetitions, Directory.EDMONDS_EVAL + "complete_test_binary_disj", HeapType.BINARY, EdmondsEnum.EFFICIENT_DISJOINT_SET);
        System.out.println("Running Complete Binomial");
        Validation.runEdmondsEvaluation(dir, repetitions, Directory.EDMONDS_EVAL + "complete_test_binomial_disj", HeapType.BINOMIAL, EdmondsEnum.EFFICIENT_DISJOINT_SET);

    }
    private static void edmondsV2() throws IOException {

        int repetitions = 5;
        String dir = Directory.SCALE_FREE;
        System.out.println("Running Scale free Binary");
        Validation.runEdmondsEvaluation(dir, repetitions, Directory.EDMONDS_EVAL + "scale_free_test_binary", HeapType.BINARY, EdmondsEnum.EFFICIENT);
        System.out.println("Running Scale free Binomial");
        Validation.runEdmondsEvaluation(dir, repetitions, Directory.EDMONDS_EVAL + "scale_free_test_binomial", HeapType.BINOMIAL, EdmondsEnum.EFFICIENT);

        dir = Directory.SPARSE_GRAPH;
        System.out.println("Running Sparse Binary");
        Validation.runEdmondsEvaluation(dir, repetitions, Directory.EDMONDS_EVAL + "sparse_test_binary", HeapType.BINARY, EdmondsEnum.EFFICIENT);
        System.out.println("Running Sparse Binomial");
        Validation.runEdmondsEvaluation(dir, repetitions, Directory.EDMONDS_EVAL + "sparse_test_binomial", HeapType.BINOMIAL, EdmondsEnum.EFFICIENT);

        dir = Directory.DIRECTED_COMPLETE;
        System.out.println("Running Complete Binary");
        Validation.runEdmondsEvaluation(dir, repetitions, Directory.EDMONDS_EVAL + "complete_test_binary", HeapType.BINARY, EdmondsEnum.EFFICIENT);
        System.out.println("Running Complete Binomial");
        Validation.runEdmondsEvaluation(dir, repetitions, Directory.EDMONDS_EVAL + "complete_test_binomial", HeapType.BINOMIAL, EdmondsEnum.EFFICIENT);

    }

    private static void edmondsV3() throws IOException {

        int repetitions = 5;
        String dir = Directory.SCALE_FREE;
        System.out.println("Running Scale free Binary");
        Validation.runEdmondsEvaluation(dir, repetitions, Directory.EDMONDS_EVAL + "scale_free_test_binary_v3", HeapType.BINARY, EdmondsEnum.CAMERINI);
        System.out.println("Running Scale free Binomial");
        Validation.runEdmondsEvaluation(dir, repetitions, Directory.EDMONDS_EVAL + "scale_free_test_binomial_v3", HeapType.BINOMIAL, EdmondsEnum.CAMERINI);

        dir = Directory.SPARSE_GRAPH;
        System.out.println("Running Sparse Binary");
        Validation.runEdmondsEvaluation(dir, repetitions, Directory.EDMONDS_EVAL + "sparse_test_binary_v3", HeapType.BINARY, EdmondsEnum.CAMERINI);
        System.out.println("Running Sparse Binomial");
        Validation.runEdmondsEvaluation(dir, repetitions, Directory.EDMONDS_EVAL + "sparse_test_binomial_v3", HeapType.BINOMIAL, EdmondsEnum.CAMERINI);

        dir = Directory.DIRECTED_COMPLETE;
        System.out.println("Running Complete Binary");
        Validation.runEdmondsEvaluation(dir, repetitions, Directory.EDMONDS_EVAL + "complete_test_binary_v3", HeapType.BINARY, EdmondsEnum.EFFICIENT_DISJOINT_SET);
        System.out.println("Running Complete Binomial");
        Validation.runEdmondsEvaluation(dir, repetitions, Directory.EDMONDS_EVAL + "complete_test_binomial_v3", HeapType.BINOMIAL, EdmondsEnum.EFFICIENT_DISJOINT_SET);

    }

    private static void EdmondsRandomGraphs() throws IOException {
        int repetitions = 5;
        int numberNodes = 1000;
        int[] degree = new int[]{100, 200, 300, 400, 500, 600, 700, 800, 900};
        int nbDiffGraphs = 5;

        //Map<Double, Double> timesInefficientMap = new TreeMap<>();
        //Map<Double, Double> timesCameriniMap = new TreeMap<>();
        Map<Double, Double> timesEfficientMap = new TreeMap<>();
        Map<Double, Double> timesDisjointMap = new TreeMap<>();

        System.out.println("------- Edmond eff vs ineff rand graphs --------");

        for (int i = 0; i < degree.length; i++) {
            int d = degree[i];
            System.out.println("degree " + d);
            for (int j = 0; j < nbDiffGraphs; j ++) {
                HomogenousGraph exp = new HomogenousGraph(numberNodes, d);

               //Validation.runEdmondsRandGraphs(repetitions, exp.graphDir, HeapType.BINOMIAL, EdmondsEnum.INEFFICIENT,
               //          timesInefficientMap);
               /*Validation.runEdmondsRandGraphs(repetitions, exp.graphDir, HeapType.BINOMIAL, EdmondsEnum.CAMERINI,
                        timesCameriniMap);*/
                Validation.runEdmondsRandGraphs(repetitions, exp.graphDir, HeapType.BINOMIAL, EdmondsEnum.EFFICIENT,
                        timesEfficientMap);
                Validation.runEdmondsRandGraphs(repetitions, exp.graphDir, HeapType.BINOMIAL, EdmondsEnum.EFFICIENT_DISJOINT_SET,
                        timesDisjointMap);
            }
        }

        String fileName = Directory.EDMONDS_COMP_RAND + numberNodes;
        //IO.writeMapDoubleDouble(timesInefficientMap, fileName + "_inneficient.txt");
        //IO.writeMapDoubleDouble(timesCameriniMap, fileName + "_camerini.txt");
        IO.writeMapDoubleDouble(timesEfficientMap, fileName + "_efficient.txt");
        IO.writeMapDoubleDouble(timesDisjointMap, fileName + "_disjoint.txt");
    }


    private static void UnionRandomGraphs() throws IOException {
        int repetitions = 5;
        int numberNodes = 1000;
        int[] degree = new int[]{100, 200, 300, 400, 500, 600, 700, 800, 900};
        int nbDiffGraphs = 5;

        Map<Double, Double> timesCameriniMap = new TreeMap<>();
        Map<Double, Double> timesUnion = new TreeMap<>();

        System.out.println("------- Union time on rand graphs --------");

        for (int i = 0; i < degree.length; i++) {
            int d = degree[i];
            System.out.println("degree " + d);
            for (int j = 0; j < nbDiffGraphs; j ++) {
                HomogenousGraph exp = new HomogenousGraph(numberNodes, d);
                Validation.runUnionTimers(repetitions, exp.graphDir, timesCameriniMap, timesUnion);
            }
        }

        String fileName = Directory.EDMONDS_COMP_RAND + numberNodes;
        IO.writeMapDoubleDouble(timesUnion, fileName + "union_only.txt");
        IO.writeMapDoubleDouble(timesCameriniMap, fileName + "union_all.txt");
    }
}
