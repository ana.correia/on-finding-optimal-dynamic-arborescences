package net.phyloviz.validation;

import net.phyloviz.io.Directory;
import net.phyloviz.io.IO;
import net.phyloviz.util.HomogenousGraph;

import java.util.Map;
import java.util.TreeMap;

/**
 *
 * This class is responsible for performing comparison tests between Dynamic arborescence vs static Edmonds.
 *
 *
 */

public class DynamicEval {

    public static void main(String[] args) throws Exception {

        //runPhylogeneticTestRemove();
        //runPhylogeneticTestAdd();
        String parse = args[0];
        switch(parse) {
            case "rand":
                runUpdateRandomGraph();
            break;
            case "real":
                runUpdateRealGraph(args);
            break;
        }
        /*
        Runnable t1 = new Runnable() {
            public void run() {
                try{

                    runPhylogeneticTestAdd();


                } catch (Exception e){}

            }
        };

        Runnable t2 = new Runnable() {
            public void run() {
                try{

                    runPhylogeneticTestRemove();

                } catch (Exception e){}

            }
        };

        new Thread(t1).start();
        new Thread(t2).start();
         */
    }

    private static void runCompleteGraphTest() throws Exception {

        String dir = Directory.SMALL_DIRECTED_COMPLETE;
        int rep = 3;
        int n = 10;
        long s = System.currentTimeMillis();
        Validation.dynamicAddTest(dir, rep, Directory.DYNAMIC_EVAL + "add_test", n);
        long e = System.currentTimeMillis();
        Validation.dynamicRemoveTest(dir, rep, Directory.DYNAMIC_EVAL + "remove_test", n);
        long e2 = System.currentTimeMillis();
        System.out.println("Dynamic ADD: " + (e - s) / 1000F);
        System.out.println("Dynamic DELETE: " + (e2 - e) / 1000F);
    }

    private static void runPhylogeneticTestAdd() throws Exception {

        //String [] dataset = new String[]{"clostridium.Griffiths.list", "Moraxella.Achtman7GeneMLST.list", "Salmonella.Achtman7GeneMLST.list"};
        //String [] names = new String[]{"clostridium.Griffiths", "Moraxella.Achtman7GeneMLST" , "Salmonella.Achtman7GeneMLST"};

        String[] dataset = new String[]{"Yersinia.cgMLSTv1.list", "Yersinia.wgMLST.list"};
        String[] names = new String[]{"Yersinia.cgMLSTv1", "Yersinia.wgMLST"};

        int rep = 3;
        int n = 10;

        long start = System.currentTimeMillis();


        for (int i = 0; i < dataset.length; i++) {

            String s = dataset[i];
            String name = names[i];

            Validation.philogeneticEvalDMSAAdd(Directory.PHYLOGENETIC_DATASET + s, rep, Directory.EDMONDS_EVAL + name, n);
        }

        long end = System.currentTimeMillis();
        long elapsed = end - start;
        System.out.println("Dynamic ADD: " + elapsed / 1000F);
    }

    private static void runPhylogeneticTestRemove() throws Exception {

        String[] dataset = new String[]{"clostridium.Griffiths.list", "Moraxella.Achtman7GeneMLST.list", "Yersinia.cgMLSTv1.list", "Yersinia.wgMLST.list", "Salmonella.Achtman7GeneMLST.list"};
        String[] names = new String[]{"clostridium.Griffiths", "Moraxella.Achtman7GeneMLST", "Salmonella.Achtman7GeneMLST", "Yersinia.cgMLSTv1", "Yersinia.wgMLST"};

        //String [] dataset = new String[]{"Yersinia.cgMLSTv1.list", "Yersinia.wgMLST.list"};
        //String [] names = new String[]{"Yersinia.cgMLSTv1", "Yersinia.wgMLST"};

        int[] reps = new int[]{3, 3, 3, 3, 2};
        int n = 10;
        long start = System.currentTimeMillis();

        for (int i = 0; i < dataset.length; i++) {

            String s = dataset[i];
            String name = names[i];

            Validation.philogeneticEvalDMSARemove(Directory.PHYLOGENETIC_DATASET + s, reps[i], Directory.EDMONDS_EVAL + name, n);
        }

        long end = System.currentTimeMillis();
        long elapsed = end - start;
        System.out.println("Dynamic DELETE: " + elapsed / 1000F);
    }

    private static void runUpdateRandomGraph() throws Exception {
        int nbUpdates = 10;
        int numberNodes = 1000;
        int[] degree = new int[]{100, 200, 300, 400, 500, 600, 700, 800, 900};
        int nbDiffGraphs = 5;
        System.out.println("-----Update random graphs-----");

        Map<Double, Double> timesStatic = new TreeMap<>();
        Map<Double, Double> timesDynamic = new TreeMap<>();

        for (int i = 0; i < degree.length; i++) {
            int d = degree[i];
            for (int j = 0; j < nbDiffGraphs; j++) {
                System.out.println( "degree " + d + " rep " + j);
                HomogenousGraph exp = new HomogenousGraph(numberNodes, d);
                //System.out.println(exp.graphDir.toString());
                Validation.dynamicUpdateTestUtil(exp.graphDir, timesStatic, timesDynamic, 1, nbUpdates);
            }

        }
        String fileName = Directory.EDMONDS_COMP_RAND + numberNodes;

        IO.writeMapDoubleDouble(timesStatic, fileName + "_static_update.txt");
        IO.writeMapDoubleDouble(timesDynamic, fileName + "_dyn_update.txt");
    }

    private static void runUpdateRealGraph(String[] args) throws Exception {

        String[] dataset = new String[]{"clostridium.Griffiths.list", "Moraxella.Achtman7GeneMLST.list", "Yersinia.McNally.list", "Escherichia.Achtman7GeneMLST.list", "Salmonella.Achtman7GeneMLST.list"};
        String[] names = new String[]{"clostridium.Griffiths", "Moraxella.Achtman7GeneMLST", "Yersinia.McNally", "Escherichia.Achtman7GeneMLST", "Salmonella.Achtman7GeneMLST"};
        int[] reps = new int[]{5, 5, 5, 3, 3};
        int n = 5;

        if (args.length > 1) {

            String parse = args[1];
            switch (parse) {
                case "c":
                    dataset = new String[]{"clostridium.Griffiths.list"};
                    names = new String[]{"clostridium.Griffiths"};
                    reps = new int[]{5};
                    break;
                case "e":
                    dataset = new String[]{"Escherichia.Achtman7GeneMLST.list"};
                    names = new String[]{"Escherichia.Achtman7GeneMLST"};
                    reps = new int[]{3};
                    break;
                case "m":
                    dataset = new String[]{"Moraxella.Achtman7GeneMLST.list"};
                    names = new String[]{"Moraxella.Achtman7GeneMLST"};
                    reps = new int[]{5};
                    break;
                case "s":
                    dataset = new String[]{"Salmonella.Achtman7GeneMLST.list"};
                    names = new String[]{"Salmonella.Achtman7GeneMLST"};
                    reps = new int[]{3};
                    break;
                case "y":
                    dataset = new String[]{"Yersinia.McNally.list"};
                    names = new String[]{"Yersinia.McNally"};
                    reps = new int[]{5};
                    break;
            }
        }

        System.out.println("------Update real graphs ------");
        long start = System.currentTimeMillis();

        for (int i = 0; i < dataset.length; i++) {

            String s = dataset[i];
            String name = names[i];
            System.out.println("file " + names);
            Validation.dynamicUpdateTest(Directory.PHYLOGENETIC_DATASET + s, reps[i], Directory.EDMONDS_EVAL + name, n);
        }

        long end = System.currentTimeMillis();
        long elapsed = end - start;
        System.out.println("Dynamic DELETE: " + elapsed / 1000F);
    }
}
