package net.phyloviz.validation;

import net.phyloviz.immutable.queue.HeapType;
import net.phyloviz.io.Directory;

import java.io.IOException;

public class PrimEval {

    public static void main(String[] args) throws IOException {

        String dir = Directory.UNDIRECTED_COMPLETE;
        int repetitions = 5;

        Validation.runEvaluationPrim(dir, repetitions, Directory.PRIM_EVAL + "complete_test_fast_rank_relaxed", HeapType.FAST_RANK_RELAXED);
        Validation.runEvaluationPrim(dir, repetitions, Directory.PRIM_EVAL + "complete_test_binary", HeapType.BINARY);
        Validation.runEvaluationPrim(dir, repetitions, Directory.PRIM_EVAL + "complete_test_binomial", HeapType.BINOMIAL);

        dir = Directory.UNDIRECTED_GRAPH;
        Validation.runEvaluationPrim(dir, repetitions, Directory.PRIM_EVAL + "test_fast_rank_relaxed", HeapType.FAST_RANK_RELAXED);
        Validation.runEvaluationPrim(dir, repetitions, Directory.PRIM_EVAL + "test_binary", HeapType.BINARY);
        Validation.runEvaluationPrim(dir, repetitions, Directory.PRIM_EVAL + "test_binomial", HeapType.BINOMIAL);
    }
}
