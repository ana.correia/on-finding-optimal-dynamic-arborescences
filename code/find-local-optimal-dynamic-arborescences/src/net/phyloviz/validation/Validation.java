package net.phyloviz.validation;


import net.phyloviz.Arborescence;
import net.phyloviz.comparators.Comparators;
import net.phyloviz.dynamic.algo.arborescence.ATree;
import net.phyloviz.dynamic.algo.arborescence.BlackBox;
import net.phyloviz.dynamic.algo.arborescence.DynamicArborescence;
import net.phyloviz.graph.DirectedGraph;
import net.phyloviz.graph.GraphInterface;
import net.phyloviz.graph.UndirectedGraph;
import net.phyloviz.graph.WeightedEdge;
import net.phyloviz.immutable.algo.arborescence.*;
import net.phyloviz.immutable.algo.connectivity.KosarajuSCC;
import net.phyloviz.immutable.algo.connectivity.StronglyConnected;
import net.phyloviz.immutable.algo.tree.MSTAlgorithm;
import net.phyloviz.immutable.queue.HeapType;
import net.phyloviz.io.IO;
import net.phyloviz.io.PhylogeneticIO;
import net.phyloviz.phylogenetic.matrix.DirectedPairWiseMatrix;
import net.phyloviz.phylogenetic.matrix.UndirectedPairWiseMatrix;
import net.phyloviz.phylogenetic.otu.OTU;
import net.phyloviz.util.ArboEnum;

import java.io.IOException;
import java.util.*;


public class Validation {

    /**
     * Evaluate Prim algorithm performance
     *
     * @param dir         - undirected graph directory
     * @param repetitions - repetitions
     * @param fileName    - file name
     * @param heapType    - heap type
     * @throws IOException
     */
    public static void runEvaluationPrim(String dir, int repetitions, String fileName, HeapType heapType) throws IOException {

        List<String> mainList = new ArrayList<>(IO.listFiles(dir));
        mainList.sort(String::compareTo);

        TreeMap<Double, Double> elogv = new TreeMap<>();
        UndirectedGraph graph = null;

        for (int i = 0; i < mainList.size(); i = i + 5) {

            // find sublist
            List<String> subList = mainList.subList(i, i + 5);
            List<Double> subListAvgTimes = new ArrayList<>(subList.size());

            // with every graph with x nodes run the algorithm several time and take the average
            for (String file : subList) {

                List<Long> times = new ArrayList<>(repetitions);

                graph = IO.readUndirectedGraph(dir + file);
                System.out.println("Graph: " + file);

                for (int k = 0; k < repetitions; k++) {

                    MSTAlgorithm mstAlgorithm = new MSTAlgorithm(new UndirectedGraph(graph), Comparators.totalOrderCmp(), heapType);
                    long start = System.currentTimeMillis();
                    List<WeightedEdge> tree = mstAlgorithm.getTree();
                    long end = System.currentTimeMillis();
                    long elapsed = end - start;
                    times.add(elapsed);
                }

                double averagedTimes = meanLong(times) / 1000F;
                subListAvgTimes.add(averagedTimes);
            }

            assert graph != null;
            addDataToMap(elogv, subListAvgTimes, graph);
        }

        IO.writeMapDoubleDouble(elogv, fileName + "_elogv.txt");
    }

    /**
     * General method that runs edmonds algorithm
     *
     * @param dir         - graph directory
     * @param repetitions - repetitions
     * @param fileName    - file name
     * @param heapType    - heap type
     * @param version     - edmonds version
     * @throws IOException
     */
    @SuppressWarnings("Duplicates")
    public static void runEdmondsEvaluation(String dir, int repetitions, String fileName, HeapType heapType, EdmondsEnum version) throws IOException {

        List<String> mainList = new ArrayList<>(IO.listFiles(dir));
        mainList.sort(String::compareTo);

        TreeMap<Double, Double> doubleDoubleTreeMap = new TreeMap<>();
        TreeMap<Double, Double> doubleDoubleTreeMapUnion = new TreeMap<>();

        DirectedGraph graph = null;

        for (int i = 0; i < mainList.size(); i = i + 5) {

            // find sublist
            List<String> subList = mainList.subList(i, i + 5);
            List<Double> subListAvgTimes = new ArrayList<>(subList.size());
            List<Double> subListAvgReducedCosts = new ArrayList<>(subList.size());

            // with every graph with x nodes run the algorithm several time and take the average
            for (String file : subList) {

                List<Long> times = new ArrayList<>(repetitions);
                List<Long> reducedCostsTime = new ArrayList<>(repetitions);

                graph = IO.readDirectedGraph(dir + file);
                //System.out.println("Graph: " + file);

                for (int k = 0; k < repetitions; k++) {

                    Arborescence arborescenceFinder = null;
                    switch (version) {
                        case EFFICIENT:
                            arborescenceFinder = new EdmondsSlowReduction(new DirectedGraph(graph), Comparators.totalOrderCmp(), heapType, true, ArboEnum.FASTER_ARBO_DISJOINT_SET);
                            break;
                        case INEFFICIENT:
                            arborescenceFinder = new OriginalImpl(new DirectedGraph(graph), Comparators.totalOrderCmp());
                            break;
                        case EFFICIENT_JAVA_HEAP:
                            arborescenceFinder = new EdmondsJavaHeap(new DirectedGraph(graph), Comparators.totalOrderCmp());
                            break;
                        case EFFICIENT_DISJOINT_SET:
                            arborescenceFinder = new EdmondsFastReduction(new DirectedGraph(graph), Comparators.totalOrderCmp(), heapType, true, ArboEnum.FASTER_ARBO_DISJOINT_SET);
                            break;
                        case CAMERINI:
                            arborescenceFinder = new CameriniExpansion(new DirectedGraph(graph), Comparators.totalOrderCmp(), heapType, true, ArboEnum.FASTER_ARBO_DISJOINT_SET);
                            break;
                        default:
                            throw new IllegalArgumentException("Illegal version");
                    }

                    long start = System.currentTimeMillis();
                    arborescenceFinder.getArborescence();
                    long end = System.currentTimeMillis();
                    long elapsed = end - start;
                    times.add(elapsed);

                    if (version != EdmondsEnum.INEFFICIENT) {
                        BenchMark benchMark = arborescenceFinder.getBenchMark();
                        reducedCostsTime.add(elapsed - benchMark.getReducedCostOperations());
                    }
                }

                subListAvgTimes.add(meanLong(times) / 1000F);
                if (version != EdmondsEnum.INEFFICIENT)
                    subListAvgReducedCosts.add(meanLong(reducedCostsTime) / 1000F);
            }

            assert graph != null;
            addDataToMap(doubleDoubleTreeMap, subListAvgTimes, graph);

            if (version != EdmondsEnum.INEFFICIENT)
                addDataToMap(doubleDoubleTreeMapUnion, subListAvgReducedCosts, graph);
        }

        IO.writeMapDoubleDouble(doubleDoubleTreeMap, fileName + "_elogv.txt");

        if (version != EdmondsEnum.INEFFICIENT)
            IO.writeMapDoubleDouble(doubleDoubleTreeMapUnion, fileName + "_elogv_no_reduced.txt");
    }

    public static void runEdmondsRandGraphs(int repetitions, DirectedGraph graph, HeapType heapType, EdmondsEnum version,
                                            Map<Double, Double> times) throws IOException {

        List<Long> t = new ArrayList<>();

        for (int k = 0; k < repetitions; k++) {

            Arborescence arborescenceFinder = null;
            switch (version) {
                case EFFICIENT:
                    arborescenceFinder = new EdmondsSlowReduction(new DirectedGraph(graph), Comparators.totalOrderCmp(),
                            heapType, true, ArboEnum.FASTER_ARBO_DISJOINT_SET);
                    break;
                case INEFFICIENT:
                    arborescenceFinder = new OriginalImpl(new DirectedGraph(graph), Comparators.totalOrderCmp());
                    break;
                case EFFICIENT_JAVA_HEAP:
                    arborescenceFinder = new EdmondsJavaHeap(new DirectedGraph(graph), Comparators.totalOrderCmp());
                    break;
                case EFFICIENT_DISJOINT_SET:
                    arborescenceFinder = new EdmondsFastReduction(new DirectedGraph(graph), Comparators.totalOrderCmp(), heapType, true, ArboEnum.FASTER_ARBO_DISJOINT_SET);
                    break;
                case CAMERINI:
                    arborescenceFinder = new CameriniExpansion(new DirectedGraph(graph), Comparators.totalOrderCmp(),
                            heapType, true, ArboEnum.FASTER_ARBO_DISJOINT_SET);
                    break;
                default:
                    throw new IllegalArgumentException("Illegal version");
            }

            long start = System.currentTimeMillis();
            arborescenceFinder.getArborescence();
            long end = System.currentTimeMillis();
            long elapsed = end - start;
            t.add(elapsed);

        }
        int nVertices = graph.size();
        int nEdges = graph.nEdges();
        times.put(eLogV(nEdges, nVertices), meanLong(t) / 1000F);

    }

    public static void runUnionTimers(int repetitions, DirectedGraph graph,
                                            Map<Double, Double> timesAll, Map<Double, Double> timesUnion) throws IOException {

        List<Long> tAll = new ArrayList<>();
        List<Long> tUnion = new ArrayList<>();


        for (int k = 0; k < repetitions; k++) {

            Arborescence arborescenceFinder = null;

            arborescenceFinder = new CameriniExpansion(new DirectedGraph(graph), Comparators.totalOrderCmp(),
                      HeapType.BINOMIAL, true, ArboEnum.FASTER_ARBO_DISJOINT_SET);



            long start = System.currentTimeMillis();
            arborescenceFinder.getArborescence();
            long end = System.currentTimeMillis();
            long elapsed = end - start;
            tAll.add(elapsed);
            BenchMark benchMark = arborescenceFinder.getBenchMark();
            tUnion.add(benchMark.getUnionOperations());


        }
        int nVertices = graph.size();
        int nEdges = graph.nEdges();
        timesAll.put(eLogV(nEdges, nVertices), meanLong(tAll) / 1000F);
        timesUnion.put(eLogV(nEdges, nVertices), meanLong(tUnion) / 1000F);

    }

    /**
     * Evaluation of the heaps when the reduced cost computation is made linearly
     *
     * @param dir         - graph directory
     * @param repetitions - repetitions
     * @param fileName    - file name
     * @throws IOException
     */
    @SuppressWarnings("Duplicates")
    public static void cacheMissesEvaluation(String dir, int repetitions, String fileName) throws IOException {

        List<String> mainList = new ArrayList<>(IO.listFiles(dir));
        mainList.sort(Comparator.naturalOrder());

        TreeMap<Double, Double> javaHeapMap = new TreeMap<>();
        TreeMap<Double, Double> smartStorageMap = new TreeMap<>();
        TreeMap<Double, Double> noSmartStorageMap = new TreeMap<>();

        DirectedGraph graph = null;

        for (int i = 0; i < mainList.size(); i = i + 5) {

            // find sublist
            List<String> subList = mainList.subList(i, i + 5);

            List<Double> timesJavaHeap = new ArrayList<>(repetitions);
            List<Double> timesSmartStorage = new ArrayList<>(repetitions);
            List<Double> timesNoSmartStorage = new ArrayList<>(repetitions);

            for (String file : subList) {
                System.out.println("File: " + file);
                List<Long> _timesJavaHeap = new ArrayList<>(repetitions);
                List<Long> _timesSmartStorage = new ArrayList<>(repetitions);
                List<Long> _timesNoSmartStorage = new ArrayList<>(repetitions);

                graph = IO.readDirectedGraph(dir + file);

                for (int j = 0; j < repetitions; j++) {

                    Arborescence edJavaHeap = new EdmondsJavaHeap(new DirectedGraph(graph), Comparators.totalOrderCmp());
                    long elapsed = getReducedCostTime(edJavaHeap);
                    _timesJavaHeap.add(elapsed);
                }

                for (int j = 0; j < repetitions; j++) {
                    Arborescence edmondsBinaryDummy = new EdmondsSlowReduction(new DirectedGraph(graph), Comparators.totalOrderCmp(), HeapType.BINARY, true, ArboEnum.FASTER_ARBO_DISJOINT_SET);
                    long elapsed = getReducedCostTime(edmondsBinaryDummy);
                    _timesSmartStorage.add(elapsed);
                }

                for (int j = 0; j < repetitions; j++) {

                    Arborescence edNoSmartStorage = new EdmondsSlowReduction(new DirectedGraph(graph), Comparators.totalOrderCmp(), HeapType.BINARY, false, ArboEnum.FASTER_ARBO_DISJOINT_SET);
                    long elapsed = getReducedCostTime(edNoSmartStorage);
                    _timesNoSmartStorage.add(elapsed);
                }

                timesJavaHeap.add(meanLong(_timesJavaHeap) / 1000F);
                timesSmartStorage.add(meanLong(_timesSmartStorage) / 1000F);
                timesNoSmartStorage.add(meanLong(_timesNoSmartStorage) / 1000F);
            }

            assert graph != null;
            addDataToMap(javaHeapMap, timesJavaHeap, graph);
            addDataToMap(smartStorageMap, timesSmartStorage, graph);
            addDataToMap(noSmartStorageMap, timesNoSmartStorage, graph);
        }

        IO.writeMapDoubleDouble(javaHeapMap, fileName + "_java_heap.txt");
        IO.writeMapDoubleDouble(smartStorageMap, fileName + "_smart_storage.txt");
        IO.writeMapDoubleDouble(noSmartStorageMap, fileName + "_no_smart_storage.txt");
    }

    /**
     * Evaluate the binary heap union procedures
     *
     * @param dir         - directory of graphs
     * @param repetitions - repetitions
     * @param fileName    - file name
     * @throws IOException
     */
    @SuppressWarnings("Duplicates")
    public static void binaryFloydUnion(String dir, int repetitions, String fileName) throws IOException {

        List<String> mainList = new ArrayList<>(IO.listFiles(dir));
        mainList.sort((s, s2) -> s.compareTo(s2));

        TreeMap<Double, Double> binaryFloyd = new TreeMap<>();
        TreeMap<Double, Double> binaryDummy = new TreeMap<>();
        DirectedGraph graph = null;

        for (int i = 0; i < mainList.size(); i = i + 5) {

            // find sublist
            List<String> subList = mainList.subList(i, i + 5);

            List<Double> timesBinaryFloyd = new ArrayList<>(repetitions);
            List<Double> timesBinaryDummy = new ArrayList<>(repetitions);

            for (String file : subList) {


                graph = IO.readDirectedGraph(dir + file);

                binaryFloydUnionUtil(repetitions, graph, binaryFloyd, binaryDummy);
            }

            assert graph != null;

            //addDataToMap(binaryFloyd, timesBinaryFloyd, graph);
            //addDataToMap(binaryDummy, timesBinaryDummy, graph);
        }

        IO.writeMapDoubleDouble(binaryFloyd, fileName + "_binary_floyd.txt");
        IO.writeMapDoubleDouble(binaryDummy, fileName + "_binary_dummy.txt");
    }

    public static void binaryFloydUnionUtil(int repetitions, DirectedGraph graph, Map<Double, Double> timeFloyd,
                                            Map<Double, Double> timeInsert) throws IOException {

        List<Long> _timesBinFloyd = new ArrayList<>(repetitions);
        List<Long> _timesBinDummy = new ArrayList<>(repetitions);
        for (int j = 0; j < repetitions; j++) {

            // (DirectedGraph G, Comparator<WeightedEdge> cmp, HeapType heapType, boolean smartStorage, ArboEnum arboEnum
            Arborescence arborescence = new EdmondsFastReduction(new DirectedGraph(graph), Comparators.totalOrderCmp(),
                    HeapType.BINARY, true, false, ArboEnum.FASTER_ARBO_DISJOINT_SET);

            arborescence.getArborescence();
            BenchMark benchMark = arborescence.getBenchMark();
            long unionTime = benchMark.getUnionOperations();
            _timesBinDummy.add(unionTime);
        }

        for (int j = 0; j < repetitions; j++) {

            Arborescence arborescence = new EdmondsFastReduction(new DirectedGraph(graph), Comparators.totalOrderCmp(),
                    HeapType.BINARY, true, true, ArboEnum.FASTER_ARBO_DISJOINT_SET);

            arborescence.getArborescence();
            BenchMark benchMark = arborescence.getBenchMark();
            long unionTime = benchMark.getUnionOperations();
            _timesBinFloyd.add(unionTime);
        }

        assert graph != null;
        int nVertices = graph.size();
        int nEdges = graph.nEdges();
        timeFloyd.put(eLogV(nEdges, nVertices), meanLong(_timesBinFloyd) / 1000F);
        timeInsert.put(eLogV(nEdges, nVertices), meanLong(_timesBinDummy) / 1000F);
    }

        /**
         * Evaluate the performance of the disjoint sets
         *
         * @param dir         - directory of graphs
         * @param repetitions - repetitions
         * @param fileName    - filename
         * @throws IOException
         */
    @SuppressWarnings("Duplicates")
    public static void weightedDisjointSetEvaluation(String dir, int repetitions, String fileName) throws IOException {

        List<String> mainList = new ArrayList<>(IO.listFiles(dir));
        mainList.sort((s, s2) -> s.compareTo(s2));

        TreeMap<Double, Double> linearDisjointSet = new TreeMap<>();
        TreeMap<Double, Double> constantDisjointSet = new TreeMap<>();
        DirectedGraph graph = null;

        for (int i = 0; i < mainList.size(); i = i + 5) {

            // find sublist
            List<String> subList = mainList.subList(i, i + 5);

            List<Double> linearTimes = new ArrayList<>();
            List<Double> constantTimes = new ArrayList<>();

            for (String file : subList) {

                graph = IO.readDirectedGraph(dir + file);
                List<Long> _linearTimes = new ArrayList<>();
                List<Long> _constantTimes = new ArrayList<>();

                for (int j = 0; j < repetitions; j++) {

                    Arborescence arborescence = new CameriniExpansion(new DirectedGraph(graph), Comparators.totalOrderCmp(),
                            HeapType.BINOMIAL, true, false, ArboEnum.FASTER_ARBO_DISJOINT_SET);

                    long l = getReducedCostTime(arborescence);
                    _linearTimes.add(l);
                }


                for (int j = 0; j < repetitions; j++) {

                    Arborescence arborescence = new CameriniExpansion(new DirectedGraph(graph), Comparators.totalOrderCmp(),
                            HeapType.BINOMIAL, true, false, ArboEnum.LINEAR_ARBO_DISJOINT_SET);

                    long l = getReducedCostTime(arborescence);
                    _constantTimes.add(l);
                }

                linearTimes.add(meanLong(_linearTimes) / 1000F);
                constantTimes.add(meanLong(_constantTimes) / 1000F);
            }

            assert graph != null;
            addDataToMap(linearDisjointSet, linearTimes, graph);
            addDataToMap(constantDisjointSet, constantTimes, graph);
        }

        IO.writeMapDoubleDouble(linearDisjointSet, fileName + "_linear_disjoint_set.txt");
        IO.writeMapDoubleDouble(constantDisjointSet, fileName + "_constant_disjoint_set.txt");
    }

    public static void philogeneticEvaluation(String dir, int repetitions, String fileName) throws IOException {

        TreeMap<Double, Double> phyloMap = new TreeMap<>();

        List<OTU> mainOtusLst = PhylogeneticIO.parseFile(dir);
        int otusSize = mainOtusLst.size();
        float floatSize = (float) otusSize;
        System.out.println("Graph: " + dir);
        for (float i = 0.1F; i <= 1.0; i = i + 0.1F) {
            int split = (int) (floatSize * i);
            List<OTU> subList = mainOtusLst.subList(0, split);
            System.out.println("i: " + i);
            System.out.println("SubList size: " + split);
            phyloUtil(repetitions, phyloMap, subList);
        }

        phyloUtil(repetitions, phyloMap, mainOtusLst);
        IO.writeMapDoubleDouble(phyloMap, fileName + ".txt");
    }

    public static void primEdmondsPhyloEval(String dir, int repetitions, String fileName) throws IOException {

        Map<Double, Double> primMap = new TreeMap<>();
        Map<Double, Double> edmondsMap = new TreeMap<>();
        System.out.println("File: " + dir);
        List<OTU> mainOtusLst = PhylogeneticIO.parseFile(dir);

        float floatSize = (float) mainOtusLst.size();
        for (float i = 0.1F; i <= 1.0; i = i + 0.1F) {
            int split = (int) (floatSize * i);
            System.out.println("i: "  + i);
            List<OTU> subList = mainOtusLst.subList(0, split);
            primEdmondPhyloEvalUtil(repetitions, primMap, edmondsMap, subList);
        }

        primEdmondPhyloEvalUtil(repetitions, primMap, edmondsMap, mainOtusLst);

        IO.writeMapDoubleDouble(edmondsMap, fileName + "_edmonds.txt");
        IO.writeMapDoubleDouble(primMap, fileName + "_prim.txt");
    }

    private static void primEdmondPhyloEvalUtil(int repetitions, Map<Double, Double> primMap, Map<Double, Double> edmondsMap, List<OTU> subList) {

        UndirectedPairWiseMatrix undirectedPairWiseMatrix = new UndirectedPairWiseMatrix(subList);
        UndirectedGraph undirectedGraph = undirectedPairWiseMatrix.matrixToUndirectedGraph();
        DirectedGraph directedGraph = undirectedPairWiseMatrix.matrixToDirectedGraph();

        runPrimEdmondPhyloEvalUtil(repetitions, primMap, edmondsMap, undirectedGraph, directedGraph);
    }

    public static void runPrimEdmondPhyloEvalUtil(int repetitions, Map<Double, Double> primMap, Map<Double, Double> edmondsMap,
                UndirectedGraph undirectedGraph, DirectedGraph directedGraph) {

        List<Long> primTimes = new ArrayList<>();
        List<Long> edTimes = new ArrayList<>();

        for (int j = 0; j < repetitions; j++) {

            long start = System.currentTimeMillis();
            Arborescence arborescence = new CameriniExpansion(directedGraph,
                    Comparators.totalOrderCmp(), HeapType.BINOMIAL, true,
                    ArboEnum.FASTER_ARBO_DISJOINT_SET);
            arborescence.getArborescence();
            long end = System.currentTimeMillis();
            edTimes.add(end - start);

            start = System.currentTimeMillis();
            MSTAlgorithm mstAlgorithm = new MSTAlgorithm(undirectedGraph, Comparators.totalOrderCmp(), HeapType.FAST_RANK_RELAXED);
            mstAlgorithm.getTree();
            end = System.currentTimeMillis();
            primTimes.add(end - start);
        }

        int nVertices = directedGraph.size();
        int nEdges = undirectedGraph.nEdges();
        primMap.put(eLogV(nEdges, nVertices), meanLong(primTimes) / 1000F);
        edmondsMap.put(eLogV(nEdges*2, nVertices), meanLong(edTimes) / 1000F);
    }

    @SuppressWarnings("Duplicates")
    private static void phyloUtil(int repetitions, TreeMap<Double, Double> phyloMap, List<OTU> subList) {
        DirectedPairWiseMatrix directedPairWiseMatrix = new DirectedPairWiseMatrix(subList);
        DirectedGraph directedGraph = directedPairWiseMatrix.matrixToDirectedGraph();
        List<Long> times = new ArrayList<>();

        for (int j = 0; j < repetitions; j++) {

            Arborescence camerini = new CameriniExpansion(new DirectedGraph(directedGraph), Comparators.totalOrderCmp(), HeapType.BINOMIAL, true, ArboEnum.FASTER_ARBO_DISJOINT_SET);
            long start = System.currentTimeMillis();
            camerini.getArborescence();
            long end = System.currentTimeMillis();
            long elapsed = end - start;
            times.add(elapsed);
        }


        int vertices = directedGraph.size();
        int nEdges = directedGraph.nEdges();
        double mean = meanLong(times) / 1000F;
        double elogv = eLogV(nEdges, vertices);
        phyloMap.put(elogv, mean);
    }

    @SuppressWarnings("Duplicates")
    public static void dynamicAddTest(String directory, int rep, String fileName, int n) throws Exception {

        List<String> mainList = new ArrayList<>(IO.listFiles(directory));
        mainList.sort(String::compareTo);

        List<Double> logs = new LinkedList<>();
        List<Double> timeStaticFinal = new LinkedList<>();
        List<Double> timeDynamicFinal = new LinkedList<>();

        for (int i = 0; i < mainList.size(); i = i + 5) {

            List<Double> timeStatic = new LinkedList<>();
            List<Double> timeDynamic = new LinkedList<>();

            List<String> subList = mainList.subList(i, i + 5);
            DirectedGraph g = null;
            for (String gName: subList) {
                System.out.println("G: " + gName);
                DirectedGraph G = IO.readDirectedGraph(directory + gName);
                dynamicAddTestUtil(G, timeStatic, timeDynamic, rep, n);
                g = G;
            }

            timeDynamicFinal.add(meanDouble(timeDynamic));
            timeStaticFinal.add(meanDouble(timeStatic));

            assert g != null;

            int nVertices = g.size();
            int nEdges = g.nEdges();
            double elogv = eLogV(nEdges, nVertices);

            logs.add(elogv);
        }

        IO.writeMapDoubleDouble(timeDynamicFinal, logs, fileName + "_dynamic.txt");
        IO.writeMapDoubleDouble(timeStaticFinal, logs, fileName + "_static.txt");
    }

    @SuppressWarnings("Duplicates")
    private  static void dynamicAddTestUtil(DirectedGraph G, List<Double> timeStatic, List<Double> timeDynamic, int rep, int n){

        Random random = new Random(System.currentTimeMillis());
        int maxW = Collections.max(G.getAllEdges(), Comparators.totalOrderCmp()).getWeight()/3;

        List<Double> dynamicAddAVG = new LinkedList<>();
        List<Double> staticAddAVG = new LinkedList<>();

        int gSize = G.size();
        List<WeightedEdge> randomEdges = createRandomEdges(random, gSize, maxW, G);

        for (int i = 0 ; i < rep; i++) {

            DirectedGraph G1 = new DirectedGraph(G);
            DirectedGraph G2 = new DirectedGraph(G);

            BlackBox blackBox = new BlackBox(new DirectedGraph(G1), Comparators.totalOrderCmp());
            List<WeightedEdge> arbo = blackBox.getArborescence();
            ATree tree = blackBox.getATree();
            DynamicArborescence dynamicArborescence = new DynamicArborescence(tree, Comparators.totalOrderCmp(), G1);
            //Collections.shuffle(randomEdges);
            List<WeightedEdge> createEdges = randomEdges.subList(0,n);

            long _dTime = 0;
            long _sTime = 0;

            for (WeightedEdge newEdge: createEdges) {

                long start = System.currentTimeMillis();
                List<WeightedEdge> dynamicSol = dynamicArborescence.addNewEdge(newEdge);
                long end = System.currentTimeMillis();
                long elapsed = end - start;
                _dTime += elapsed;

                G2.addEdge(new WeightedEdge(newEdge));

                Arborescence blackBox1 = new BlackBox(G2, Comparators.totalOrderCmp());
                long start2 = System.currentTimeMillis();
                List<WeightedEdge> solution = blackBox1.getArborescence();
                long end2 = System.currentTimeMillis();
                elapsed = end2 - start2;

                _sTime+=elapsed;
            }

            dynamicAddAVG.add((double)_dTime/n);
            staticAddAVG.add((double)_sTime/n);
        }

        double d = meanDouble(dynamicAddAVG)/1000F;
        double s = meanDouble(staticAddAVG)/1000F;
        timeStatic.add(s);
        timeDynamic.add(d);
    }

    @SuppressWarnings("Duplicates")
    public static void dynamicRemoveTest(String directory, int rep, String fileName, int n) throws IOException {

        List<String> mainList = new ArrayList<>(IO.listFiles(directory));
        mainList.sort(String::compareTo);

        List<Double> logs = new LinkedList<>();
        List<Double> timeStaticFinal = new LinkedList<>();
        List<Double> timeDynamicFinal = new LinkedList<>();

        for (int i = 0; i < mainList.size(); i = i + 5) {

            List<Double> timeStatic = new LinkedList<>();
            List<Double> timeDynamic = new LinkedList<>();

            List<String> subList = mainList.subList(i, i + 5);
            DirectedGraph g = null;
            for (String gName: subList) {
                System.out.println("G: " + gName);
                DirectedGraph G = IO.readDirectedGraph(directory + gName);
                dynamicRemoveTestUtil(G, timeStatic, timeDynamic, rep, n);
                g = G;
            }

            timeDynamicFinal.add(meanDouble(timeDynamic));
            timeStaticFinal.add(meanDouble(timeStatic));

            assert g != null;

            int nVertices = g.size();
            int nEdges = g.nEdges();
            double elogv = eLogV(nEdges, nVertices);

            logs.add(elogv);
        }

        IO.writeMapDoubleDouble(timeDynamicFinal, logs, fileName + "_dynamic.txt");
        IO.writeMapDoubleDouble(timeStaticFinal, logs, fileName + "_static.txt");
    }

    @SuppressWarnings("Duplicates")
    private static void dynamicRemoveTestUtil(DirectedGraph G, List<Double> timeStatic, List<Double> timeDynamic, int rep, int n) {

        List<Double> dynamicTimes = new LinkedList<>();
        List<Double> staticTimes = new LinkedList<>();

        BlackBox blackBox1 = new BlackBox(new DirectedGraph(G), Comparators.totalOrderCmp());
        blackBox1.getArborescence();
        ATree aTree1 = blackBox1.getATree();
        List<WeightedEdge> treeEdges = new LinkedList<>(aTree1.getEdgesInTree());

        for (int i = 0; i < rep; i++) {

            DirectedGraph G1 = new DirectedGraph(G);
            DirectedGraph G2 = new DirectedGraph(G);

            BlackBox blackBox = new BlackBox(new DirectedGraph(G), Comparators.totalOrderCmp());
            blackBox.getArborescence();
            ATree aTree = blackBox.getATree();
            DynamicArborescence dynamicArborescence = new DynamicArborescence(aTree, Comparators.totalOrderCmp(), G1);

            boolean breakConnectivity = false;

            long dynamicTime = 0;
            long staticTime = 0;

            //Collections.shuffle(treeEdges);
            List<WeightedEdge> removedList = treeEdges.subList(0,n);

            int w = 0;
            for (WeightedEdge edge : removedList) {

                if (edge == null)
                    continue;

                if (breakConnectivity)
                    break;

                G2.removeEdge(edge);

                StronglyConnected stronglyConnected = new KosarajuSCC(G2, 0);
                if (!stronglyConnected.isStronglyConnected()) {
                    System.out.println("Breaks connectivity: " + edge);
                    breakConnectivity = true;
                }

                Arborescence cameriniExpansion = new BlackBox(G2, Comparators.totalOrderCmp());
                long start = System.currentTimeMillis();
                List<WeightedEdge> solution = cameriniExpansion.getArborescence();
                long end = System.currentTimeMillis();
                long elapsed = end - start;
                staticTime += elapsed;

                long start2 = System.currentTimeMillis();
                List<WeightedEdge> dynamicSol = dynamicArborescence.removeEdge(edge);
                long end2 = System.currentTimeMillis();
                elapsed = end2 - start2;
                dynamicTime += elapsed;

                dynamicArborescence.getAtree().getSolutionForest().resetSolutionForest();
                w++;
            }

            dynamicTimes.add((double)dynamicTime/w);
            staticTimes.add((double)staticTime/w);
        }

        double d = meanDouble(dynamicTimes)/1000F;
        double s = meanDouble(staticTimes)/1000F;
        timeStatic.add(s);
        timeDynamic.add(d);
    }


    public static void dynamicUpdateTest(String dir, int rep, String fileName, int n) throws IOException {

        DirectedPairWiseMatrix directedPairWiseMatrix;
        DirectedGraph directedGraph;

        Map<Double, Double> timeStaticFinal = new TreeMap<>();
        Map<Double, Double> timeDynamicFinal = new TreeMap<>();

        List<OTU> mainOtusLst = PhylogeneticIO.parseFile(dir);
        int otusSize = mainOtusLst.size();
        float floatSize = (float) otusSize;
        System.out.println("Graph: " + dir);
        for (float i = 0.1F; i <= 1.0; i = i + 0.1F) {
            int split = (int) (floatSize * i);
            List<OTU> subList = mainOtusLst.subList(0, split);
            directedPairWiseMatrix = new DirectedPairWiseMatrix(subList);
            directedGraph = directedPairWiseMatrix.matrixToDirectedGraph();
            System.out.println("i: " + i);
            System.out.println("SubList size: " + split);
            dynamicUpdateTestUtil(directedGraph, timeStaticFinal, timeDynamicFinal, rep, n);
            IO.writeMapDoubleDouble(timeDynamicFinal, fileName + "_update_dynamic.txt");
            IO.writeMapDoubleDouble(timeStaticFinal, fileName + "_update_static.txt");
        }
        directedPairWiseMatrix = new DirectedPairWiseMatrix(mainOtusLst);
        directedGraph = directedPairWiseMatrix.matrixToDirectedGraph();
        dynamicUpdateTestUtil(directedGraph, timeStaticFinal, timeDynamicFinal, rep, n);

        IO.writeMapDoubleDouble(timeDynamicFinal, fileName + "_update_dynamic.txt");
        IO.writeMapDoubleDouble(timeStaticFinal, fileName + "_update_static.txt");
    }

    public static void dynamicUpdateTestUtil(DirectedGraph G, Map<Double, Double> timeStatic, Map<Double, Double> timeDynamic, int repetition, int n) {

        List<Double> dynamicAddAVG = new LinkedList<>();
        List<Double> staticAddAVG = new LinkedList<>();

        for (int i = 0; i < repetition; i++) {
            DirectedGraph G1 = new DirectedGraph(G);
            DirectedGraph G2 = new DirectedGraph(G);
            Random rand = new Random();

            BlackBox blackBox = new BlackBox(new DirectedGraph(G), Comparators.totalOrderCmp());
            blackBox.getArborescence();
            ATree aTree = blackBox.getATree();
            DynamicArborescence dynamicArborescence = new DynamicArborescence(aTree, Comparators.totalOrderCmp(), G1);
            //List<WeightedEdge> treeEdges = new LinkedList<>(aTree.getEdgesInTree());
            //List<WeightedEdge> allEdges = G.getAllEdges();

            Arborescence initialisation = new BlackBox(G, Comparators.totalOrderCmp());
            List<WeightedEdge> treeEdges = initialisation.getArborescence();


            long dynamicTime = 0;
            long staticTime = 0;

            /*System.out.println("tree edges " + treeEdges.size());
            for (int j = 0; j < treeEdges.size(); j++)
                if (treeEdges.get(j) != null)
                    System.out.println(treeEdges.get(j).toString() + " ");*/

            // select edges
            //List<WeightedEdge> updateList = allEdges.subList(0,n);
            //List<WeightedEdge> updateList = treeEdges.subList(0, n);

            int w = 0;
            for (int j = 0; j < n ; j++) {
                WeightedEdge edge = treeEdges.get(rand.nextInt(treeEdges.size()));
                //System.out.println("update " + edge.toString());

                if (edge == null)
                    continue;

                G2.addEdge(edge.getSrc(), edge.getDest(), Integer.MAX_VALUE);
                G2.removeEdge(edge);

                Arborescence cameriniExpansion = new BlackBox(G2, Comparators.totalOrderCmp());
                long start = System.currentTimeMillis();
                treeEdges = cameriniExpansion.getArborescence();
                long end = System.currentTimeMillis();
                long elapsed = end - start;
                staticTime += elapsed;

               /* System.out.println("cam sol " + treeEdges.size());
                for (int k = 0; k < treeEdges.size(); k++)
                    if (treeEdges.get(k) != null)
                        System.out.println(treeEdges.get(k).toString() + " ");*/


                long start2 = System.currentTimeMillis();
                List<WeightedEdge> dynamicSol = dynamicArborescence.updateCost(edge, Integer.MAX_VALUE);
                long end2 = System.currentTimeMillis();
                elapsed = end2 - start2;
                dynamicTime += elapsed;

                w++;
            }

            dynamicAddAVG.add((double)dynamicTime/w);
            staticAddAVG.add((double)staticTime/w);

        }
        double d = meanDouble(dynamicAddAVG)/1000F;
        double s = meanDouble(staticAddAVG)/1000F;
        int nVertices = G.size();
        int nEdges = G.nEdges();
        timeStatic.put(eLogV(nEdges, nVertices), s);
        timeDynamic.put(eLogV(nEdges, nVertices), d);
    }

    public static List<WeightedEdge> createRandomEdges(Random random, int n, int maxW, DirectedGraph G) {

        List<WeightedEdge> edgeList = new LinkedList<>();

        for (int u = 0; u < n; u++){

            for (int v = 0; v < n; v++){

                if (u != v) {
                    int w = random.nextInt(maxW);
                    WeightedEdge weightedEdge = new WeightedEdge(u,v,w);
                    edgeList.add(weightedEdge);
                }
            }
        }

        return edgeList;
    }

    @SuppressWarnings("Duplicates")
    public static void philogeneticEvalDMSAAdd(String directory, int rep, String fileName, int n) throws IOException {

        System.out.println("ADD EVAL: " + directory);
        List<OTU> mainOtusLst = PhylogeneticIO.parseFile(directory);
        float floatSize = (float) mainOtusLst.size();

        Map<Double,Double> treeMapDynamic = new TreeMap<>();
        Map<Double,Double> treeMapStatic = new TreeMap<>();

        for (float i = 0.1F; i <= 1.0F; i = i + 0.1F) {
            System.out.println("add i : " + i);
            int split = (int) (floatSize * i);
            List<OTU> subList = mainOtusLst.subList(0, split);

            DirectedPairWiseMatrix directedPairWiseMatrix = new DirectedPairWiseMatrix(subList);
            DirectedGraph G = directedPairWiseMatrix.matrixToDirectedGraph();

            List<Double> timeStatic = new LinkedList<>();
            List<Double> timeDynamic = new LinkedList<>();

            dynamicAddTestUtil(G, timeStatic, timeDynamic, rep, n);
            dynamicPhilogeneticUtil(treeMapDynamic, treeMapStatic, G, timeStatic, timeDynamic);
        }

        IO.writeMapDoubleDouble(treeMapDynamic,fileName+"_dynamic_add.txt");
        IO.writeMapDoubleDouble(treeMapStatic,fileName+"_static_add.txt");
    }

    @SuppressWarnings("Duplicates")
    private static void dynamicPhilogeneticUtil(Map<Double, Double> treeMapDynamic, Map<Double, Double> treeMapStatic, DirectedGraph g, List<Double> timeStatic, List<Double> timeDynamic) {
        double avgStatic = meanDouble(timeStatic);
        double avgDynamic = meanDouble(timeDynamic);

        int nVertices = g.size();
        int nEdges = g.nEdges();
        double elogv = eLogV(nEdges, nVertices);

        treeMapStatic.put(elogv, avgStatic);
        treeMapDynamic.put(elogv, avgDynamic);
    }

    @SuppressWarnings("Duplicates")
    public static void philogeneticEvalDMSARemove(String directory, int rep, String fileName, int n) throws IOException {

        System.out.println("DELETE EVAL: " + directory);

        List<OTU> mainOtusLst = PhylogeneticIO.parseFile(directory);
        float floatSize = (float) mainOtusLst.size();

        Map<Double,Double> treeMapDynamic = new TreeMap<>();
        Map<Double,Double> treeMapStatic = new TreeMap<>();

        for (float i = 0.1F; i <= 1.0; i = i + 0.1F) {
            System.out.println("delete i : " + i);

            int split = (int) (floatSize * i);
            List<OTU> subList = mainOtusLst.subList(0, split);

            //UndirectedPairWiseMatrix undirectedPairWiseMatrix = new UndirectedPairWiseMatrix(subList);

            DirectedPairWiseMatrix directedPairWiseMatrix = new DirectedPairWiseMatrix(subList);
            DirectedGraph G = directedPairWiseMatrix.matrixToDirectedGraph();


            List<Double> timeStatic = new LinkedList<>();
            List<Double> timeDynamic = new LinkedList<>();

            dynamicRemoveTestUtil(G, timeStatic, timeDynamic, rep, n);
            dynamicPhilogeneticUtil(treeMapDynamic, treeMapStatic, G, timeStatic, timeDynamic);
        }

        IO.writeMapDoubleDouble(treeMapDynamic,fileName+"_dynamic_remove.txt");
        IO.writeMapDoubleDouble(treeMapStatic,fileName+"_static_remove.txt");
    }

    /**
     * Util function returns the reduced cost operation time
     *
     * @param arborescence - arborescence object
     * @return
     */
    private static long getReducedCostTime(Arborescence arborescence) {
        List<WeightedEdge> solution = arborescence.getArborescence();
        BenchMark benchMark = arborescence.getBenchMark();
        return benchMark.getReducedCostOperations();
    }

    /**
     * Util function add data to map
     *
     * @param treeMap         - map
     * @param subListAvgTimes - list
     * @param graph           - graph
     */
    private static void addDataToMap(TreeMap<Double, Double> treeMap, List<Double> subListAvgTimes, GraphInterface graph) {
        int nNodes = graph.size();
        int nEdges = graph.nEdges();
        double log = eLogV(nEdges, nNodes);
        treeMap.put(log, meanDouble(subListAvgTimes));
    }

    /**
     * Compute mean
     *
     * @param longList list with elements of type long
     * @return mean
     */
    private static double meanLong(List<Long> longList) {

        long acc = 0;

        for (Long l : longList) {
            acc += l;

        }

        double _acc = (double) acc;
        double size = (double) longList.size();

        double v = _acc / size;
        return v;
    }

    /**
     * Compute mean
     *
     * @param list - list of doubles
     * @return mean
     */
    private static double meanDouble(List<Double> list) {

        double acc = 0;

        for (Double l : list) {
            acc += l;
        }

        return acc / list.size();
    }

    /**
     * Compute E log_{2}(V)
     *
     * @param E - number of edges
     * @param V - number of nodes
     * @return E log_{2}(V)
     */
    private static double eLogV(int E, int V) {
        double l = (Math.log(V) / Math.log(2));
        return E * l;
    }
}