package net.phyloviz.serializable;

import java.io.*;

public class Serializer<T> {

    /**
     * Serialize an object
     *
     * @param fileName - file name
     * @param object   - object to be serialized
     * @throws IOException
     */
    public void serialize(String fileName, T object) throws IOException {
        //Saving of object in a file
        FileOutputStream file = new FileOutputStream(fileName);
        ObjectOutputStream out = new ObjectOutputStream(file);
        // Method for serialization of object
        out.writeObject(object);
        out.close();
        file.close();
    }

    /**
     * Deserialize an object
     *
     * @param fileName - serialized object
     * @return object T
     * @throws IOException            - if file does not exist
     * @throws ClassNotFoundException - if class does not exist
     */
    @SuppressWarnings("unchecked")
    public T deserialize(String fileName) throws IOException, ClassNotFoundException {
        T object;
        // Reading the object from a file
        FileInputStream file = new FileInputStream(fileName);
        ObjectInputStream in = new ObjectInputStream(file);
        // Method for deserialization of object
        object = (T) in.readObject();
        in.close();
        file.close();
        return object;
    }
}
