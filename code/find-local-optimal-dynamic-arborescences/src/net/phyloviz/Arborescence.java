package net.phyloviz;

import net.phyloviz.graph.WeightedEdge;
import net.phyloviz.immutable.algo.arborescence.BenchMark;

import java.util.List;

public interface Arborescence {

    List<WeightedEdge> getArborescence();
    int computeWeight(List<WeightedEdge> lst);
    BenchMark getBenchMark();
}
