package net.phyloviz.util;

import java.util.Arrays;

public class FasterArboDisjointSet extends DisjointSet implements ArboDisjointSet {

    private int[] weight;

    public FasterArboDisjointSet(FasterArboDisjointSet fasterArboDisjointSet) {

        super(fasterArboDisjointSet);
        this.size = fasterArboDisjointSet.size;
        this.weight = new int[this.size];
        System.arraycopy(fasterArboDisjointSet.weight, 0, this.weight, 0, this.size);
    }


    public FasterArboDisjointSet(int n) {

        super(n);
        this.weight = new int[this.size];

        for (int i = 0; i < this.size; i++) {
            this.weight[i] = 0;
        }
    }

    @Override
    public int findSet(int i) {

        if (i < 0 || i >= this.size) {
            throw new IllegalArgumentException("i must be positive and less than size");
        }

        for (; this.pi[i] != this.pi[this.pi[i]]; i = this.pi[i]) {
            this.weight[i] += this.weight[this.pi[i]];
            this.pi[i] = this.pi[this.pi[i]];
        }

        return this.pi[i];
    }


    @Override
    public int findWeight(int i) {

        int w = this.weight[i];
        for (; i != this.pi[i]; i = this.pi[i])
            w += this.weight[this.pi[i]];

        return w;
    }

    @Override
    @SuppressWarnings("Duplicates")
    public void unionSet(int i, int j) {

        if (i < 0 || j < 0 || i >= this.size || j >= this.size)
            throw new IllegalArgumentException();

        int iRoot = this.findSet(i);
        int jRoot = this.findSet(j);

        if (iRoot == jRoot) {
            return;
        }

        if (this.rank[iRoot] > this.rank[jRoot]) {
            this.pi[jRoot] = iRoot;
            this.weight[jRoot] -= this.weight[iRoot];
        } else if (this.rank[iRoot] < this.rank[jRoot]) {
            this.pi[iRoot] = jRoot;
            this.weight[iRoot] -= this.weight[jRoot];
        } else {
            this.pi[iRoot] = jRoot;
            this.weight[iRoot] -= this.weight[jRoot];
            this.rank[jRoot]++;
        }
    }

    @Override
    public void addWeight(int i, int w) {
        this.weight[this.findSet(i)] += w;
    }

    @Override
    public ArboDisjointSet clone() {
        return new FasterArboDisjointSet(this);
    }

    @Override
    public String toString() {
        return "pi:" + Arrays.toString(pi) + "\nw:" + Arrays.toString(this.weight);
    }
}
