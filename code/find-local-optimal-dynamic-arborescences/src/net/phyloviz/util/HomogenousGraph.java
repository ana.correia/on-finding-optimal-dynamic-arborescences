package net.phyloviz.util;

import net.phyloviz.comparators.Comparators;
import net.phyloviz.graph.DirectedGraph;
import net.phyloviz.graph.UndirectedGraph;
import net.phyloviz.graph.WeightedEdge;
import net.phyloviz.immutable.algo.tree.MSTAlgorithm;
import net.phyloviz.immutable.queue.HeapType;

import java.util.*;

import java.util.Comparator;
import java.util.Random;


public class HomogenousGraph {
        private int numberNodes;
        private int degree;
        private int nbEdges;
        private int[][] matrix;
        private int[] weights;
        public DirectedGraph graphDir;
        public UndirectedGraph graphUndir;
        private Random rand;

        public HomogenousGraph(int n, int e) {
            this.numberNodes = n;
            this.degree = e;
            this.rand = new Random();
            this.nbEdges = n*e;
            this.matrix = new int[nbEdges][2];
            this.weights = new int[nbEdges];
            this.graphDir = new DirectedGraph(n);
            this.graphUndir = new UndirectedGraph();

            createEdges();
            populateGraph();
            //toUndirected();

        }

        private void createEdges() {
            int cpt = 0;
            for (int i = 0; i < numberNodes; i++) {
                for (int j = 0; j < degree; j++) {
                    matrix[cpt][0] = i;
                    matrix[cpt][1] = (i + j + 1)%numberNodes;
                    cpt++;
                }
            }

            int[] perm = permutation();

            for (int i = 0; i < nbEdges; i++) {
                matrix[i][0] = perm[matrix[i][0]];
                matrix[i][1] = perm[matrix[i][1]];
            }

            shuffle();
        }

        private int[] permutation() {
            int[] perm = new int[numberNodes];

            for (int i = 0; i < numberNodes; i++)
                perm[i] = i;

            int tmp;
            for (int i = numberNodes ; i > 1;) {
                int j = rand.nextInt(i);
                i--;
                tmp = perm[i];
                perm[i] = perm[j];
                perm[j] = tmp;
            }
            //System.out.println(perm);
            return perm;
        }

        private void shuffle() {
            int[] tmp = new int[2];

            for (int i = nbEdges; i > 1;) {
                int j = rand.nextInt(i);
                i--;
                tmp[0] = matrix[i][0];
                tmp[1] = matrix[i][1];
                matrix[i][0] = matrix[j][0];
                matrix[i][1] = matrix[j][1];
                matrix[j][0] = tmp[0];
                matrix[j][1] = tmp[1];
            }
        }

        private void populateGraph() {

            for (int i = 0; i < nbEdges; i++) {
                int w = rand.nextInt(100) + 1;
                this.weights[i] = w;
                graphDir.addEdge(matrix[i][0], matrix[i][1], w);
            }

            for (int i = 0; i < nbEdges; i++) {
                WeightedEdge e = new WeightedEdge(matrix[i][0], matrix[i][1], weights[i]);
                graphUndir.addEdge(e);
            }

            // add a dummy node to have a strongly connected graph
            for (int i = 0; i < numberNodes; i++) {
                graphDir.addEdge(numberNodes, i, Integer.MAX_VALUE);
                graphDir.addEdge(i, numberNodes, Integer.MAX_VALUE);
            }
        }

        public void toUndirected() {
            for (int i = 0; i < nbEdges; i++) {
                graphDir.addEdge(matrix[i][1], matrix[i][0], this.weights[i]);
            }
            this.nbEdges = this.nbEdges*2;
        }

        public int nEdgesDir() {return this.nbEdges;}
        public int nEdgesUndir() {return this.nbEdges/2;}


    public String toString() {
        return this.graphDir.toString();
    }
}
