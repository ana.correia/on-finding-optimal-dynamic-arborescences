package net.phyloviz.util;

import java.util.Arrays;

public class LinearArboDisjointSet extends DisjointSet implements ArboDisjointSet {

    private int[] weight;

    public LinearArboDisjointSet(LinearArboDisjointSet linearArboDisjointSet) {

        super(linearArboDisjointSet);
        this.size = linearArboDisjointSet.size;
        this.weight = new int[this.size];
        System.arraycopy(linearArboDisjointSet.weight, 0, this.weight, 0, this.size);
    }


    public LinearArboDisjointSet(int n) {

        super(n);
        this.weight = new int[this.size];

        for (int i = 0; i < this.size; i++) {
            this.weight[i] = 0;
        }
    }

    @Override
    public int findWeight(int i) {
        return this.weight[i];
    }

    @Override
    public void addWeight(int i, int w) {

        i = this.findSet(i);
        int k = 0;
        for (Integer j : this.pi) {

            if (this.findSet(j) == i) {
                this.weight[k] += w;
            }

            k++;
        }
    }

    @Override
    public ArboDisjointSet clone() {
        return new LinearArboDisjointSet(this);
    }

    @Override
    public String toString() {
        return "pi: " + Arrays.toString(pi) + "\nw:" + Arrays.toString(weight);
    }
}
