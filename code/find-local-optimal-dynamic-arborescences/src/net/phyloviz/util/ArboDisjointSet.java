package net.phyloviz.util;

public interface ArboDisjointSet {

    int findSet(int i);

    void unionSet(int i, int j);

    int findWeight(int i);

    void addWeight(int i, int w);

    boolean sameSet(int i, int j);

    ArboDisjointSet clone();
}
