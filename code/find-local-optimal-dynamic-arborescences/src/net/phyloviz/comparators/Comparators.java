package net.phyloviz.comparators;

import net.phyloviz.graph.WeightedEdge;
import net.phyloviz.util.ArboDisjointSet;

import java.util.Comparator;

public class Comparators {

    /**
     * Simple Edge comparator only compares weights
     *
     * @return Comparator of edges
     */
    public static Comparator<WeightedEdge> getSimpleComparator() {

        Comparator<WeightedEdge> cmp = new Comparator<WeightedEdge>() {

            @Override
            public int compare(WeightedEdge o1, WeightedEdge o2) {

                return Integer.compare(o1.getWeight(), o2.getWeight());
            }
        };

        return cmp;
    }

    public static Comparator<WeightedEdge> comparatorWithDisjointSet(ArboDisjointSet disjointSet) {

        Comparator<WeightedEdge> cmp = new Comparator<WeightedEdge>() {

            @Override
            public int compare(WeightedEdge o1, WeightedEdge o2) {

                int wO1 = o1.getWeight() + disjointSet.findWeight(o1.getDest());
                int wO2 = o2.getWeight() + disjointSet.findWeight(o2.getDest());

                int result = wO1 - wO2;

                if (result != 0) {

                    return result;
                }

                return endpointEval(o1, o2);

            }
        };

        return cmp;
    }

    public static Comparator<WeightedEdge> totalOrderCmp() {

        Comparator<WeightedEdge> cmp = new Comparator<WeightedEdge>() {
            @Override
            public int compare(WeightedEdge o1, WeightedEdge o2) {

                int o1W = o1.getWeight();
                int o2W = o2.getWeight();

                int res = o1W - o2W;

                if (res != 0) {
                    return res;
                }


                return endpointEval(o1, o2);
            }
        };
        return cmp;
    }

    public static int endpointEval(WeightedEdge o1, WeightedEdge o2) {

        int u1 = o1.getSrc();
        int v1 = o1.getDest();

        int u2 = o2.getSrc();
        int v2 = o2.getDest();

        int u = Integer.min(u1, v1);
        int v = Integer.min(u2, v2);

        int r = u - v;

        if (r != 0)
            return r;

        u = Integer.max(u1, v1);
        v = Integer.max(u2, v2);

        r = u - v;

        if (r != 0)
            return r;


        return 0;
    }

    /**
     * Solution Comparator total order to store the solution
     *
     * @return Edge Comparator
     */
    public static Comparator<WeightedEdge> getSolutionComparator() {

        Comparator<WeightedEdge> cmp = new Comparator<WeightedEdge>() {

            @Override
            public int compare(WeightedEdge o1, WeightedEdge o2) {

                return endpointEval(o1, o2);
            }
        };

        return cmp;
    }
}
