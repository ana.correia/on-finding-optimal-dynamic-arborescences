package net.phyloviz.dynamic.algo.arborescence;

import net.phyloviz.Arborescence;
import net.phyloviz.dynamic.queue.Heap;
import net.phyloviz.dynamic.queue.BinomialHeap;
import net.phyloviz.graph.DirectedGraph;
import net.phyloviz.graph.WeightedEdge;
import net.phyloviz.immutable.algo.arborescence.BenchMark;
import net.phyloviz.immutable.algo.arborescence.camerini.EdgeNode;
import net.phyloviz.immutable.algo.arborescence.camerini.SolutionForest;
import net.phyloviz.util.ArboDisjointSet;
import net.phyloviz.util.DisjointSet;
import net.phyloviz.util.FasterArboDisjointSet;

import java.util.*;

public class BlackBox implements Arborescence {

    private DirectedGraph G;
    private Comparator<WeightedEdge> cmp;
    private Comparator<WeightedEdge> maxDisjointCmp;

    private Heap<WeightedEdge>[] queues;

    private DisjointSet weaklyConnected;
    private ArboDisjointSet stronglyConnected;

    private LinkedList<Integer> roots;

    private SolutionForest solutionForest;
    private ATree aTree;

    private ATNode[] inATNode;
    private EdgeNode[] inEdgeNode;
    private WeightedEdge[] in;

    private List<List<ATNode>> astNodeCycle;
    private List<List<EdgeNode>> edgeNodeCycle;

    private List<WeightedEdge> maxEdgeCycle;
    private List<Integer> maxCycleEdgeWeights;
    private List<List<WeightedEdge>> contractedEdges;

    private int V = 0;
    private int E = 0;


    /**
     * Class constructor
     *
     * @param G          - input directed graph object
     * @param comparator - Weighted Edge Comparator Object
     */
    @SuppressWarnings("unchecked")
    public BlackBox(DirectedGraph G, Comparator<WeightedEdge> comparator) {

        this.G = new DirectedGraph(G);
        this.G.reverseGraph();
        this.cmp = comparator;
        this.V = this.G.size();

        this.maxDisjointCmp = new Comparator<WeightedEdge>() {
            @Override
            public int compare(WeightedEdge edge1, WeightedEdge edge2) {
                return cmp.compare(getAdjustedWeightedEdge(edge1), getAdjustedWeightedEdge(edge2));
            }
        };

        this.stronglyConnected = new FasterArboDisjointSet(this.G.size());
        this.weaklyConnected = new DisjointSet(this.G.size());
        this.queues = new Heap[this.G.size()];
        this.in = new WeightedEdge[this.G.size()];
        this.inATNode = new ATNode[this.G.size()];
        this.inEdgeNode = new EdgeNode[this.G.size()];

        this.aTree = new ATree(this.G.size());
        this.solutionForest = new SolutionForest(this.G.size(), this.G.nEdges());

        Set<Integer> nodeSet = this.G.getNodeSet();
        this.roots = new LinkedList<>(nodeSet);

        this.astNodeCycle = new ArrayList<>(this.G.size());
        this.edgeNodeCycle = new ArrayList<>(this.G.size());
        this.maxEdgeCycle = new ArrayList<>(this.G.size());
        this.maxCycleEdgeWeights = new ArrayList<>(this.G.size());
        this.contractedEdges = new ArrayList<>(this.G.size());

        for (Integer node : nodeSet) {
            this.queues[node] = new BinomialHeap<WeightedEdge>(this.maxDisjointCmp);
            this.edgeNodeCycle.add(node, null);
            this.astNodeCycle.add(node, null);
            this.maxEdgeCycle.add(node,null);
            this.maxCycleEdgeWeights.add(node,null);
            this.contractedEdges.add(node,null);
        }

        List<WeightedEdge> allEdges = this.G.getAllEdges();
        this.E = allEdges.size();
        for (WeightedEdge edge : allEdges) {
            WeightedEdge copy = new WeightedEdge(edge);
            this.queues[copy.getDest()].push(copy);
        }
    }

    @SuppressWarnings("unchecked")
    public BlackBox(DirectedGraph G, Comparator<WeightedEdge> comparator, ATree aTree, List<ATNode> aTreeRoots, List<ATNode> removedContracted, WeightedEdge newEdge, WeightedEdge removeEdge)
    {

        this.G = new DirectedGraph(G);
        this.cmp = comparator;
        this.maxDisjointCmp = new Comparator<WeightedEdge>() {
            @Override
            public int compare(WeightedEdge edge1, WeightedEdge edge2) {
                return cmp.compare(getAdjustedWeightedEdge(edge1), getAdjustedWeightedEdge(edge2));
            }
        };

        this.aTree = aTree;
        this.solutionForest = this.aTree.getSolutionForest();
        this.in = new WeightedEdge[this.G.size()];
        this.inATNode = new ATNode[this.G.size()];
        this.inEdgeNode = new EdgeNode[this.G.size()];
        this.astNodeCycle = new ArrayList<>(this.G.size());
        this.edgeNodeCycle = new ArrayList<>(this.G.size());
        this.queues = new Heap[this.G.size()];

        this.maxEdgeCycle = new ArrayList<>(this.G.size());
        this.maxCycleEdgeWeights = new ArrayList<>(this.G.size());
        this.contractedEdges = new ArrayList<>(this.G.size());

        Set<Integer> nodeSet = this.G.getNodeSet();
        for (Integer node : nodeSet) {
            this.edgeNodeCycle.add(node, null);
            this.astNodeCycle.add(node, null);
            this.queues[node] = new BinomialHeap<>(this.maxDisjointCmp);
            this.maxEdgeCycle.add(node,null);
            this.maxCycleEdgeWeights.add(node,null);
            this.contractedEdges.add(node, null);
        }

        this.weaklyConnected = new DisjointSet(this.G.size());
        this.stronglyConnected = new FasterArboDisjointSet(this.G.size());

        this.recoverContraction();
        this.populateQueues(removedContracted,newEdge);
        this.setRoots(aTreeRoots);

        EdgeNode n = this.solutionForest.getEdgeNode(removeEdge);
        for (EdgeNode c: n.getChildren()){
            c.setInitialParent(null);
            c.setParent(null);
        }

        ATNode nn = this.aTree.getATNode(removeEdge);
        for (ATNode c: nn.getChildren()){
            c.setParent(null);
        }

        // remove the nodes related with the removed edge
        this.solutionForest.removeNodeFromNodeList(removeEdge);
        this.aTree.removeFromATNodeLst(removeEdge);
    }

    /**
     * Recover the contractions
     */
    private void recoverContraction() {

        this.solutionForest.resetMaxArr();
        this.solutionForest.clearRset();

        List<ATNode> roots = this.aTree.roots();

        for (ATNode root: roots){

            Stack<ATNode> nodeStack = this.aTree.reverseOrderUtil(root);

            while (!nodeStack.isEmpty()) {
                ATNode node = nodeStack.pop();
                List<ATNode> children = node.getChildren();
                this.processATNode(children, node);
            }
        }
    }

    /**
     * Initialize data-structures related with the cycles
     * @param children - List of ATNodes
     */
    private void processATNode(List<ATNode> children, ATNode node) {

        WeightedEdge maxEdge = node.getMaxEdgeCycle();
        int maxDst = this.stronglyConnected.findSet(maxEdge.getDest());
        int reducedCost = node.getMaxCycleW();

        for (ATNode child: children) {
             WeightedEdge e = child.getWeightedEdge();
             int dst = this.stronglyConnected.findSet(e.getDest());
             this.in[dst] = e;
             this.inATNode[dst] = child;
             this.inEdgeNode[dst] = this.solutionForest.getEdgeNode(e);
             this.computeReducedCostUtil(dst, reducedCost, e);
        }

        for (ATNode child: children) {
            WeightedEdge e = child.getWeightedEdge();
            int src = e.getSrc();
            int dst = e.getDest();
            this.stronglyConnected.unionSet(src, dst);
            this.weaklyConnected.unionSet(src,dst);
        }

        int rep = this.stronglyConnected.findSet(maxEdge.getDest());
        this.solutionForest.updateMaxArray(rep, maxDst);
        WeightedEdge edge = node.getWeightedEdge();
        EdgeNode edgeNode = this.solutionForest.getEdgeNode(edge);
        List<EdgeNode> edgeNodeChildren = edgeNode.getChildren();
        this.edgeNodeCycle.set(rep, edgeNodeChildren);
        this.astNodeCycle.set(rep, children);
        this.maxEdgeCycle.set(rep, maxEdge);
        this.contractedEdges.set(rep, new ArrayList<>(node.getContractedEdges()));
        this.maxCycleEdgeWeights.set(rep,reducedCost);
        this.maxEdgeCycle.set(rep, maxEdge);
    }


    /**
     * Set V' vertex set to the roots
     * @param aTreeRoots - List of ATNodes that are the roots of the decomposed tree
     */
    private void setRoots(List<ATNode> aTreeRoots) {
        this.roots = new LinkedList<>();
        for (ATNode node : aTreeRoots) {
            WeightedEdge edge = node.getWeightedEdge();
            int v = this.stronglyConnected.findSet(edge.getDest());
            this.roots.add(v);
            this.V++;
        }
    }

    /**
     * Insert edges in the queues
     * @param removedContracted - List of removed contracted ATNodes
     * @param newEdge - Edge addition case
     */
    private void populateQueues(List<ATNode> removedContracted, WeightedEdge newEdge) {
        //initialize priority queues
        for (ATNode node : removedContracted) {
            Set<WeightedEdge> contractedEdges = node.getContractedEdges();
            for (WeightedEdge edge : contractedEdges) {
                int dst = this.stronglyConnected.findSet(edge.getDest());
                this.queues[dst].push(edge);
                this.E++;
            }
        }
        // addEdge case
        if (newEdge != null) {
            int dst = this.stronglyConnected.findSet(newEdge.getDest());
            this.queues[dst].push(newEdge);
            this.E++;
        }
    }

    @Override
    public List<WeightedEdge> getArborescence() {
        return this.getArborescenceUtil();
    }

    /**
     * Compute the cost of a given list of weighted edges
     *
     * @param lst - list of weighed edges
     * @return weight
     */
    @Override
    public int computeWeight(List<WeightedEdge> lst) {
        int result = 0;
        for (WeightedEdge edge : lst) {
            result += edge.getOriginalWeight();
        }
        return result;
    }

    @Override
    public BenchMark getBenchMark() {
        return null;
    }

    /**
     * Getter for the ATree
     *
     * @return ATree
     */
    public ATree getATree() {
        return this.aTree;
    }

    /**
     * Util contraction function
     * @return List of WeightedEdge
     */
    private List<WeightedEdge> getArborescenceUtil() {

        while (!this.roots.isEmpty()) {

            int root = this.roots.pop();
            ATNode minATNode = new ATNode(root, null);
            WeightedEdge min = this.getMinWeightEdgeUtil(root, minATNode);

            if (min == null) {
                continue;
            }

            int u = min.getSrc();
            int v = min.getDest();

            // this edge is safe
            EdgeNode minEdgeNode = this.solutionForest.add(min);

            // set edge and the constant to be added to the edge in ATNode
            minATNode = this.aTree.add(minATNode, min);
            minATNode.setCurrentWeight(this.getAdjustedWeight(min));
            //minATNode.setCurrentWeight(this.stronglyConnected.findWeight(v));
            minATNode.setMaxCycleW(this.maxCycleEdgeWeights.get(root));
            minATNode.setMaxEdgeCycle(this.maxEdgeCycle.get(root));

            //System.out.println("MIN : " + min);
            this.buildTreeUtil(root, minEdgeNode, minATNode);

            // check if edge min is safe to add to the arborescence
            if (this.weaklyConnected.findSet(u) != this.weaklyConnected.findSet(v)) {
                this.safeAddUtil(root , min, minEdgeNode, minATNode, u , v);
            } else {
                // we have a cycle lets process it
                this.cycleContractionUtil(root, min, minEdgeNode, minATNode, v);
            }
        }

        this.aTree.setSolutionForest(this.solutionForest);
        return this.solutionForest.expansion();
    }

    /**
     * Add the edge and perform contraction
     * @param root - vertex
     * @param min - minimum weight edge
     * @param minEdgeNode - EdgeNode object
     * @param minATNode - ATNode object
     * @param u - min src
     * @param v - min dst
     */
    private void safeAddUtil(int root, WeightedEdge min, EdgeNode minEdgeNode, ATNode minATNode, int u, int v) {
        this.in[root] = min;
        this.inEdgeNode[root] = minEdgeNode;
        this.inATNode[root] = minATNode;
        this.weaklyConnected.unionSet(u, v);
    }

    /**
     * Add the edge and perform contraction
     * @param root - vertex
     * @param min - minimum weight edge
     * @param minEdgeNode - EdgeNode object
     * @param minATNode - ATNode object
     * @param v - min dst
     */
    private void cycleContractionUtil(int root, WeightedEdge min, EdgeNode minEdgeNode, ATNode minATNode, int v){

        List<WeightedEdge> cycleEdges =  new LinkedList<>();
        cycleEdges.add(min);
        List<EdgeNode> edgeNodeCycle = new LinkedList<>();
        edgeNodeCycle.add(minEdgeNode);
        List<ATNode> ATNodeCycle = new LinkedList<>();
        ATNodeCycle.add(minATNode);
        List<Integer> contractionSet = new LinkedList<>();
        Map<Integer, WeightedEdge> map = new HashMap<>();

        // find cycle
        this.findCycleUtil(cycleEdges, edgeNodeCycle, ATNodeCycle, contractionSet, map, min, root, v);

        // find maximum adjustedWeights edge
        WeightedEdge maxEdgeCycle = Collections.max(cycleEdges, this.maxDisjointCmp);

        int dst = this.stronglyConnected.findSet(maxEdgeCycle.getDest());
        int maxW = this.getAdjustedWeight(maxEdgeCycle);

        // compute reduced costs
        this.computeReducedCost(contractionSet, map, maxW);

        // perform unionSet
        for (WeightedEdge weightedEdge : cycleEdges) {
            this.stronglyConnected.unionSet(weightedEdge.getSrc(), weightedEdge.getDest());
        }

        // find cycle representative
        int rep = this.stronglyConnected.findSet(maxEdgeCycle.getDest());

        // the representative will be the next node to be evaluated
        this.roots.add(0, rep);

        // perform heap union
        this.heapUnion(contractionSet, rep);

        // set cycles
        this.edgeNodeCycle.set(rep, edgeNodeCycle);
        this.astNodeCycle.set(rep, ATNodeCycle);
        this.maxCycleEdgeWeights.set(rep, maxW);
        this.maxEdgeCycle.set(rep,maxEdgeCycle);
        this.solutionForest.updateMaxArray(rep, dst);
    }

    /**
     * Perform all needed operations related to building of ATree and Camerini forest
     * @param root - vertex
     * @param minEdgeNode - MinEdgeNode object incident in root
     * @param minATNode - MinATNode object incident in root
     */
    private void buildTreeUtil(int root, EdgeNode minEdgeNode, ATNode minATNode) {

        // if a node was in the tree and is the representative of a cycle no process
        if (minEdgeNode.getChildren().size() > 0){
            return;
        }

        // no cycle so it is a leaf
        if (this.edgeNodeCycle.get(root) == null) {
            // set leafs
            this.solutionForest.addPi(root, minEdgeNode);
            this.aTree.addPi(root, minATNode);
        } else {
            // cycle in the previous iteration of Edmonds Algorithm
            this.ATCycle(minATNode, root);
            this.edgeNodeCycle(minEdgeNode, root);
        }
    }

    /**
     * Find minimum weight edge incident in vertex root
     * @param root - vertex
     * @param minATNode - MinATNode object
     * @return - null if no edge is incident in root, otherwise a valid WeightedEdge object
     */
    private WeightedEdge getMinWeightEdgeUtil(int root, ATNode minATNode) {

        Heap<WeightedEdge> PQ = this.queues[root];

        // current component does not have any incident edge
        if (PQ.isEmpty()) {
            // whenever a cycle does not have an incident edge it becomes a super-node
            boolean b = this.ATCycle(minATNode, root);
            if (b) {
                this.solutionForest.addEntryToRset(root);
                this.aTree.add(minATNode, null);
            }

            return null;
        }

        // verifying if current component has any other edge than a self-loop
        WeightedEdge min = PQ.pop();
        while (!PQ.isEmpty() && this.stronglyConnected.sameSet(min.getSrc(), min.getDest())) {
            min = PQ.pop();
        }

        int u = min.getSrc();
        int v = min.getDest();

        // this component does not have any edge that is not a self loop so we must check another component
        if (this.stronglyConnected.sameSet(u, v)) {
            // whenever a cycle does not have an incident edge it becomes a super-node
            boolean b = this.ATCycle(minATNode, root);
            if (b) {
                this.solutionForest.addEntryToRset(root);
                this.aTree.add(minATNode, null);
            }

            return null;
        }

         return min;
    }

    /**
     * Perform heap union
     * @param contractionSet - contraction set
     * @param rep - cycle representative
     */
    private void heapUnion(List<Integer> contractionSet, int rep) {

        Heap<WeightedEdge> heap = this.queues[rep];
        List<WeightedEdge> allContracted = new LinkedList<>();
        List<WeightedEdge> contracted = this.findContractedEdges(heap, allContracted);

        for (WeightedEdge edge: contracted){
             heap.delete(edge);
        }

        for (Integer node : contractionSet) {
            if (rep != node) {
                Heap<WeightedEdge> nodeHeap = this.queues[node];
                contracted = this.findContractedEdges(nodeHeap, allContracted);
                for (WeightedEdge edge: contracted){
                    nodeHeap.delete(edge);
                }

                heap.union(nodeHeap);
            }
        }

        this.contractedEdges.set(rep, allContracted);
        //System.out.println("CONTRACTED: " + allContracted);
    }

    /**
     * Find contracted edges in a heap
     * @param heap - Heap object
     * @return List of contracted edges
     */
    private List<WeightedEdge> findContractedEdges(Heap<WeightedEdge> heap, List<WeightedEdge> allContracted) {
        List<WeightedEdge> contracted = new LinkedList<>();

        for (WeightedEdge edge: heap.getKeySet()){

            int src = edge.getSrc();
            int dst = edge.getDest();

            if (this.stronglyConnected.sameSet(src,dst)){
                contracted.add(edge);
                allContracted.add(edge);
            }
        }

        return contracted;
    }


    /**
     * Find the cycle
     * @param cycleEdges - WeighedEdge object List
     * @param edgeNodeCycle - EdgeNode object List
     * @param atNodeCycle - ATNode object List
     * @param contractionSet - Set of integers
     * @param map - Map<Integer,WeightedEdge>
     * @param min - WeightedEdge
     * @param root - Incident
     * @param v - Edge destination
     */
    private void findCycleUtil(List<WeightedEdge> cycleEdges, List<EdgeNode> edgeNodeCycle, List<ATNode> atNodeCycle,
                               List<Integer> contractionSet, Map<Integer,WeightedEdge> map, WeightedEdge min, int root, int v)
    {
        contractionSet.add(this.stronglyConnected.findSet(v));
        this.in[root] = null;
        this.inEdgeNode[root] = null;
        this.inATNode[root] = null;

        // find the cycle
        map.put(this.stronglyConnected.findSet(v), min);

        for (int i = this.stronglyConnected.findSet(min.getSrc());
             this.in[i] != null;
             i = this.stronglyConnected.findSet(this.in[i].getSrc()))
        {
            contractionSet.add(i);
            map.put(i, this.in[i]);
            cycleEdges.add(this.in[i]);
            edgeNodeCycle.add(this.inEdgeNode[i]);
            atNodeCycle.add(this.inATNode[i]);
        }
    }

    /**
     * Calculate the reduced costs
     *
     * @param contractionSet - contraction set
     * @param map  - map holding pair (edge.getDest(), edge)
     * @param maxW - maximum weight in the cycle
     */
    private void computeReducedCost(Iterable<Integer> contractionSet, Map<Integer, WeightedEdge> map, int maxW) {
        for (Integer node : contractionSet) {
            this.computeReducedCostUtil(node, maxW, map.get(node));
        }
    }

    /**
     * Compute reduced costs
     * @param vertex - vertex
     * @param maxW - max weight of cycle
     * @param edge - max cycle edge
     */
    private void computeReducedCostUtil(int vertex, int maxW, WeightedEdge edge) {
        int incidentW = this.getAdjustedWeight(edge);
        int reducedCost = maxW - incidentW;
        this.stronglyConnected.addWeight(vertex, reducedCost);
    }

    /**
     * Get the adjusted weight of a given edge
     *
     * @param edge - input edge
     * @return new WeightedEdge object with the new
     */
    private WeightedEdge getAdjustedWeightedEdge(WeightedEdge edge) {
        int w = this.getAdjustedWeight(edge);
        return new WeightedEdge(edge.getSrc(), edge.getDest(), w);
    }

    /**
     * Calculate the adjusted weight of a given edge
     *
     * @param edge - input edge
     * @return new weight
     */
    private int getAdjustedWeight(WeightedEdge edge) {
        return edge.getOriginalWeight() + this.stronglyConnected.findWeight(edge.getDest());
    }

    /**
     * Process the ATNode cycle
     *
     * @param minATNode - ATNode object
     * @param root - root
     */
    private boolean ATCycle(ATNode minATNode, int root) {

        List<ATNode> cycle = this.astNodeCycle.get(root);

        if (cycle == null) {
            return false;
        }

        // set super-node true
        minATNode.setSuperNode(true);
        // set parent to be a root
        minATNode.setParent(null);

        for (ATNode astNode : cycle) {
            // set parent of cycle node
            astNode.setParent(minATNode);
            // make node of cycle a child of the super-node
            minATNode.addChild(astNode);
            // add edge to the contracted edge list
            minATNode.addToContractedEdges(astNode.getWeightedEdge());
        }

        // add the contracted edges to the super-node
        List<WeightedEdge> contracted = this.contractedEdges.get(root);
        if (contracted != null && contracted.size() > 0) {
            minATNode.addToContractedEdges(contracted);
        }

        // whenever a node is
        if (minATNode.getMaxEdgeCycle() == null) {
            minATNode.setMaxEdgeCycle(this.maxEdgeCycle.get(root));
            minATNode.setMaxCycleW(this.maxCycleEdgeWeights.get(root));
        }

        return true;
    }

    /**
     * Util function process edgeNode cycle
     *
     * @param minEdgeNode - EdgeNode object
     * @param root - root
     */
    private void edgeNodeCycle(EdgeNode minEdgeNode, int root) {
        List<EdgeNode> cycle = this.edgeNodeCycle.get(root);
        for (EdgeNode edgeNode : cycle) {
            edgeNode.setParent(minEdgeNode);
            minEdgeNode.addChild(edgeNode);
        }
    }

    /**
     * Solution forest getter
     * @return Solution forest
     */
    public SolutionForest getSolutionForest() {
        return this.solutionForest;
    }


    public int getV() {
        return this.V;
    }


    public int getE() {
        return this.E;
    }
}
