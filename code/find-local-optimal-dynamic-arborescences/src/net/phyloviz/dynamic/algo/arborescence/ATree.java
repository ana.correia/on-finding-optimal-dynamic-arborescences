package net.phyloviz.dynamic.algo.arborescence;

import net.phyloviz.immutable.algo.arborescence.camerini.SolutionForest;
import net.phyloviz.graph.WeightedEdge;

import java.io.Serializable;
import java.util.*;

public class ATree implements Serializable {

    /**
     * Tree structure proposed in
     * Updating Directed Minimum Cost Spanning trees
     * G.Pollatos & Orestis Telelis & Vassilis Zissimopoulos
     */
    static final long serialVersionUID = 8418494L;
    // necessary to build the final solution
    private SolutionForest solutionForest;
    // ATree attributes
    private ATNode[] pi;
    private HashMap<WeightedEdge, ATNode> map;

    /**
     * Class constructor
     * Worst-case(O(1))
     *
     * @param astSize - size of the tree
     */
    public ATree(int astSize) {
        this.map = new HashMap<>(astSize);
        this.pi = new ATNode[astSize];
    }

    /**
     * Setter for the Camerini forest
     * Worst-case(O(1))
     *
     * @param forest - solution camerini
     */
    public void setSolutionForest(SolutionForest forest) {
        this.solutionForest = forest;
    }

    /**
     * Getter for SolutionForest
     * Worst-case(O(1))
     *
     * @return solution Forest
     */
    public SolutionForest getSolutionForest() {
        return this.solutionForest;
    }

    public Collection<ATNode> getATNodes() {

        return this.map.values();
    }

    /**
     * Find node
     * Worst Case O(n)
     *
     * @param edge - edge
     * @return ATNode
     */
    public ATNode getATNode(WeightedEdge edge) {

        return this.map.get(edge);
    }

    /**
     * Verify if an edge exists in our tree
     * @param edge - edge
     * @return true if edge in forest, false otherwise
     */
    public boolean contains(WeightedEdge edge) {

        return this.map.containsKey(edge);
    }

    /**
     * Getter Leaf Array
     * Worst-case(O(1))
     *
     * @return - pi
     */
    public ATNode[] getPi() {
        return this.pi;
    }

    /**
     * Add an entry to the array holding the leafs
     *
     * @param root      - position
     * @param minATNode - node
     */
    public void addPi(int root, ATNode minATNode) {
        this.pi[root] = minATNode;
    }


    /**
     * Remove elements in a given list from class attributes
     *
     * @param atNode - ATNode to be removed
     */
    public void removeFromATNodeLst(ATNode atNode) {

        this.map.remove(atNode.getWeightedEdge());
    }

    /**
     * Remove elements in a given list from class attributes
     *
     * @param edge - List of ATNodes to be removed
     */
    public void removeFromATNodeLst(WeightedEdge edge) {

        this.map.remove(edge);
    }


    /**
     * Getter for previously computed MSA
     * @return List of weighted edges
     */
    public List<WeightedEdge> getSolution() {

        return this.solutionForest.getSolution();
    }

    /**
     * Get all edges currently in the tree
     * @return Set of edges
     */
    public Set<WeightedEdge> getEdgesInTree(){
        return this.map.keySet();
    }

    /**
     * Get the number of nodes in the tree
     * @return  number of tree nodes
     */
    public int getSize() {

        return this.map.size();
    }

    /**
     * Decompose the ATree
     * @param weightedEdge - starting point of tree decomposition
     * @return - List if removed ATNodes
     */
    public List<ATNode> decomposeTree(WeightedEdge weightedEdge) {

        List<ATNode> removedATNode = new LinkedList<>();

        // The decomposition starts from a Node N where e(N) = weightedEdge
        // Getting a node takes in average O(1)
        ATNode N = this.getATNode(weightedEdge);

        // Find parent of Node N
        // Takes constant time
        ATNode parent = N.getParent();

        // Find the affected sets
        while (parent != null) {

            // remove from our list
            this.removeFromATNodeLst(parent);

            // Each of the children of a removed contracted node is made the root of its own subtree
            // I think that this condition always holds
            List<ATNode> children = parent.getChildren();
            for (ATNode child : children) {
                // child becomes a subtree
                child.setParent(null);
            }

            // clear children of the parent
            // TODO this method is linear maybe it is not necessary todo this operation
            children.clear();
            // store removed c-node
            removedATNode.add(parent);
            // go up in the tree
            parent = parent.getParent();
        }

        return removedATNode;
    }

    /**
     * Perform a reverse order transversal only storing in a stack
     * nodes with children
     * @param root - root node
     * @return Stack of nodes that represent cycle
     */
    public Stack<ATNode> reverseOrderUtil(ATNode root){

        Stack<ATNode> S = new Stack<>();
        Queue<ATNode> Q = new LinkedList<>();
        Q.add(root);

        while (!Q.isEmpty()){
            ATNode node = Q.poll();

            if(node.getChildren().size() > 0) {
                S.push(node);
            }

            Q.addAll(node.getChildren());
        }

        return S;
    }


    /**
     * Remove edge from the contracted list of an ATNode
     * Worst Case O(n)
     *
     * @param edge - edge to be removed
     */
    public void removeEdgeFromATreeContractedList(WeightedEdge edge) {

        for (ATNode node: this.map.values()) {
            if (node.getContractedEdges().contains(edge)) {
                node.getContractedEdges().remove(edge);
                return;
            }
        }
    }

    /**
     * Add ATNode to the tree
     *
     * @param minATNode - ATNode
     * @param edge - edge
     * @return - New ATNode or ATNode if exists
     */
    public ATNode add(ATNode minATNode, WeightedEdge edge) {

        // in case the node exists
        if (this.map.containsKey(edge)) {
            ATNode node = this.map.get(edge);
            node.setParent(null);
            return node;
        }

        minATNode.setWeightedEdge(edge);
        this.map.put(edge, minATNode);
        return minATNode;
    }

    /**
     * Find the roots of the tree
     *
     * Worst-case(O(n))
     * @return - List of ATNode
     */
    public List<ATNode> roots() {

        List<ATNode> roots = new LinkedList<>();

        for (ATNode node: this.map.values()){
            if (node.getParent() == null) {
                roots.add(node);
            }
        }

        return roots;
    }

    /**
     * Print tree in a transversal way
     */
    public void printTreeTransversal() {

        Comparator<WeightedEdge> comparator = net.phyloviz.comparators.Comparators.totalOrderCmp();
        Comparator<ATNode> cmp = new Comparator<ATNode>() {
            @Override
            public int compare(ATNode o1, ATNode o2) {

                WeightedEdge e1 = o1.getWeightedEdge();
                WeightedEdge e2 = o2.getWeightedEdge();

                return comparator.compare(e1,e2);
            }
        };

        for (ATNode root: this.roots()) {

            List<List<ATNode>> transversal = this.printTreeTransversalUtil(root);

            for (List<ATNode> atNodes: transversal) {
                atNodes.sort(cmp);
                for (ATNode atNode: atNodes) {

                    if (atNode.getParent() == null) {
                        System.out.print(atNode.getWeightedEdge() + " parent: null " + atNode.getContractedEdges() + "\t" );
                    }
                    else {
                        System.out.print(atNode.getWeightedEdge() + " parent: " + atNode.getParent().getWeightedEdge()
                                + " " + atNode.getContractedEdges() +  "\t");
                    }
                }

                System.out.println();
            }

            System.out.println();
        }
    }

    /**
     * Print the tree in a transversal way util function
     * @param root - ATNode root
     * @return Return List by level
     */
    private List<List<ATNode>> printTreeTransversalUtil(ATNode root){

        List<List<ATNode>> ans = new ArrayList<>();

        if(root == null)
            return ans;

        Queue<ATNode> q = new LinkedList<>();
        q.offer(root);

        while(!q.isEmpty()) {

            int size = q.size();
            List<ATNode> list = new ArrayList<>();

            for(int i = 0; i < size; i++) {

                ATNode node = q.poll();

                list.add(node);

                for(ATNode child : node.getChildren()) {
                    q.offer(child);
                }
            }

            ans.add(list);
        }

        return ans;
    }

    /**
     * Override toString method
     *
     * @return String
     */
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (ATNode node : this.map.values()) {
            stringBuilder.append(node.toString()).append("\n");
        }
        return stringBuilder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ATree aTree = (ATree) o;
        return Arrays.equals(pi, aTree.pi) &&
                Objects.equals(map, aTree.map);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(map);
        result = 31 * result + Arrays.hashCode(pi);
        return result;
    }
}
