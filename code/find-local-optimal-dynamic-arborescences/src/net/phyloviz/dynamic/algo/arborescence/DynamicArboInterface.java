package net.phyloviz.dynamic.algo.arborescence;

import net.phyloviz.graph.WeightedEdge;

import java.util.List;

public interface DynamicArboInterface {

    List<WeightedEdge> addNewEdge(WeightedEdge newEdge);

    List<WeightedEdge> removeEdge(WeightedEdge removeEdge);

    List<WeightedEdge> updateCost(WeightedEdge edge, int newCost);
}
