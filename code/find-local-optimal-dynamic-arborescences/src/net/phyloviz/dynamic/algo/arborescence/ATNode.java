package net.phyloviz.dynamic.algo.arborescence;

import net.phyloviz.graph.WeightedEdge;

import java.io.Serializable;
import java.util.*;

public class ATNode implements Serializable {

    /**
     * Node of the Active set Tree structure proposed in
     * Updating Directed Minimum Cost Spanning trees
     * G.Pollatos & Orestis Telelis & Vassilis Zissimopoulos
     */

    static final long serialVersionUID = 42852848494L;

    public static final ATNode ROOT = null;
    public static final int NO_WEIGHT = Integer.MAX_VALUE;

    private ATNode parent;
    private int id;
    private WeightedEdge weightedEdge;
    private int currentWeight;
    private List<ATNode> children;
    private boolean superNode;

    private Set<WeightedEdge> contractedEdges;
    private WeightedEdge maxEdgeCycle;
    private Integer maxCycleW;


    /**
     * Class constructor
     *
     * @param id - incident node
     * @param edge - edge
     */
    public ATNode(int id, WeightedEdge edge) {
        this.id = id;
        this.superNode = false;
        this.weightedEdge = edge;
        this.children = new ArrayList<>();
        this.contractedEdges = new HashSet<>();
        this.maxCycleW = NO_WEIGHT;
        this.maxEdgeCycle = null;
    }


    /**
     * Class constructor
     *
     * @param id  - incident node
     * @param edge - edge
     * @param currentWeight - weight of the edge when added to the node
     */
    public ATNode(int id, WeightedEdge edge, int currentWeight) {
        this(id, edge);
        this.currentWeight = currentWeight;
    }

    /**
     * Setter for the superNode attribute
     *
     * @param bool - boolean value
     */
    public void setSuperNode(boolean bool) {
        this.superNode = bool;
    }

    /**
     * Setter for currentWeight attribute
     * @param currentWeight - current weight of edge
     */
    public void setCurrentWeight(int currentWeight) {
        this.currentWeight = currentWeight;
    }

    /**
     * Add a child
     *
     * @param ATNode - ATree Node
     */
    public void addChild(ATNode ATNode) {
        this.children.add(ATNode);
    }

    /**
     * Add an edge to the contracted edge list
     *
     * @param edge - WeightedEdge object
     */
    public void addToContractedEdges(WeightedEdge edge) {
        this.contractedEdges.add(edge);
    }

    /**
     * Setter parent attribute
     *
     * @param parent -  ATNode
     */
    public void setParent(ATNode parent) {
        this.parent = parent;
    }

    /**
     * Setter for weighted edge attribute
     *
     * @param edge - WeightedEdge object
     */
    public void setWeightedEdge(WeightedEdge edge) {
        this.weightedEdge = edge;
    }

    /**
     * Getter for parent
     *
     * @return AST node Parent
     */
    public ATNode getParent() { return this.parent;}

    /**
     * Getter for weighted edge attribute
     *
     * @return weighted edge
     */
    public WeightedEdge getWeightedEdge() {
        return this.weightedEdge;
    }


    /**
     * Getter for children of the current Node
     *
     * @return - List of children
     */
    public List<ATNode> getChildren() {
        return this.children;
    }

    /**
     * ID Getter
     *
     * @return node id
     */
    public int incident() {
        return this.id;
    }

    /**
     * Getter for isSuperNode
     *
     * @return - true of instance is a super-node, otherwise return false
     */
    public boolean isSuperNode() {
        return this.superNode;
    }

    /**
     * Getter for currentWeight attributes that represents the weight of
     * the edge when added to our solution
     *
     * @return - current weight
     */
    public int getCurrentWeight() {
        return this.currentWeight;
    }

    /**
     * Getter for contracted edge list
     *
     * @return list of contracted list
     */
    public Set<WeightedEdge> getContractedEdges() { return this.contractedEdges;
    }

    /**
     * Getter for the edge with the largest weight of the cycle
     * @return WeightedEdge object
     */
    public WeightedEdge getMaxEdgeCycle() {
        return this.maxEdgeCycle;
    }

    /**
     * Setter for the attribute maxEdgeCycle
     * @param maxEdgeCycle - WeightedEdge object
     */
    public void setMaxEdgeCycle(WeightedEdge maxEdgeCycle) {
        this.maxEdgeCycle = maxEdgeCycle;
    }

    /**
     * Setter for attribute maxCycleW
      * @param maxCycleW - int
     */
    public void setMaxCycleW(Integer maxCycleW) {
        this.maxCycleW = maxCycleW;
    }

    /**
     * Getter for maxCycleW
     * @return - maxCycleW
     */
    public Integer getMaxCycleW() {
        return this.maxCycleW;
    }

    /**
     * Simple util function to build toString
     *
     * @param node - node
     * @return String
     */
    private String parentUtil(ATNode node) {

        StringBuilder res = new StringBuilder();
        res.append("[Parent ");

        if (node == null) {
            res.append("null");
            res.append( "] ");
            return res.toString();
        }

        WeightedEdge edge = node.weightedEdge;

        if (edge != null) {
            res.append(" Edge: ").append(edge);
        } else {
            res.append(" Edge: Null");
        }

        res.append( "] ");
        return res.toString();
    }


    /**
     * Simple util function to build toString
     *
     * @param edge - edge
     * @return String
     */
    private String edgeUtil(WeightedEdge edge) {
        return edge == null ? "Null" : edge.toString();

    }

    /**
     * Verify if a given node is a root
     * Wost-Case(O(1))
     * @return true if root, false otherwise
     */
    public boolean isRoot() {
        return this.parent == null;
    }

    /**
     * Clear children
     */
    public void clearChildren() {
        this.children.clear();
    }

    /**
     * Add to
     * @param contracted - List of weighted edges
     */
    public void addToContractedEdges(List<WeightedEdge> contracted) {
        this.contractedEdges.addAll(contracted);
    }


    /**
     * Getter for ATNode Comparator based on the edge
     * @return ATNode Comparator
     */
    public static Comparator<ATNode> getATNodeCmp(Comparator<WeightedEdge> cmp) {
        return (o1, o2) -> {
            WeightedEdge e1 = o1.getWeightedEdge();
            WeightedEdge e2 = o2.getWeightedEdge();
            return cmp.compare(e1, e2);
        };
    }

    @Override
    public String toString() {

        ATNode parent = this.getParent();
        StringBuilder builder = new StringBuilder();

        builder.append("ID: ").append(this.id).append(" Super: ").append(this.superNode).append(" Edge: ").
                append(this.edgeUtil(this.weightedEdge)).append(" ").
                append(this.parentUtil(parent)).append("#Contracted edgeList: ").append(getContractedEdges()).
                append("CHILDREN: ").
                append(getChildren());


        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ATNode ATNode = (ATNode) o;
        return parent == ATNode.parent &&
                id == ATNode.id &&
                superNode == ATNode.superNode &&
                Objects.equals(weightedEdge, ATNode.weightedEdge) &&
                Objects.equals(children, ATNode.children);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, weightedEdge, contractedEdges);
        result = 31 * result;
        return result;
    }
}
