package net.phyloviz.dynamic.algo.arborescence;

import net.phyloviz.graph.DirectedGraph;
import net.phyloviz.graph.WeightedEdge;
import net.phyloviz.immutable.algo.arborescence.camerini.SolutionForest;
import net.phyloviz.util.ArboDisjointSet;
import net.phyloviz.util.FasterArboDisjointSet;

import java.util.*;

public class DynamicArborescence implements DynamicArboInterface {

    // class attributes
    private ATree aTree;
    private SolutionForest solutionForest;

    // structures that must be init
    private Comparator<WeightedEdge> cmp;
    private DirectedGraph G;

    private BlackBox blackBoxEdmonds;

    /**
     * Class constructor
     *
     * @param aTree - ATree
     */
    public DynamicArborescence(ATree aTree, Comparator<WeightedEdge> cmp, DirectedGraph G) {
        this.aTree = aTree;
        this.solutionForest = this.aTree.getSolutionForest();
        // reset solution forest in linear time
        this.solutionForest.resetSolutionForest();
        this.cmp = cmp;
        this.G = G;
    }

    /**
     * Add a new Edge
     * Worst-case(|V| + ||p|| log |p|)
     *
     * @param newEdge - new edge to bee added
     * @return Minimum Spanning Arborescence
     */
    @Override
    public List<WeightedEdge> addNewEdge(WeightedEdge newEdge) {

        if (newEdge == null) {
            throw new IllegalArgumentException("Argument newEdge is null");
        }

        // hold the final solution
        List<WeightedEdge> solution;

        // Add the new Edge to the graph O(1)
        this.G.addEdge(newEdge);

        int dst = newEdge.getDest();
        int src = newEdge.getSrc();

        ATNode[] leafs = this.aTree.getPi();
        ATNode nodeDST = leafs[dst];
        ATNode nodeSRC = leafs[src];

        // try to find a candidate node worst-case O(n)
        ATNode candidate = this.findCandidate(nodeDST, newEdge);

        // no candidate was found, newEdge cannot replace any edge in H
        // so find the LCA and add new edge to the contracted edges
        if (candidate == null) {
            //System.out.println("No candidate!");
            ATNode lca = this.LCA(nodeDST, nodeSRC);
            //System.out.println("LCA : " + lca);
            lca.addToContractedEdges(newEdge);
            solution = this.aTree.getSolution();
            return solution;
        }
        // check if new Edge is safe to add

        //System.out.println("REPLACE: " + candidate.getWeightedEdge() + " FOR " + newEdge);

        boolean safe = this.isSafeToAdd(candidate, nodeSRC);
        // new Edge is not safe to be added to our tree
        // find the LCA and add edge to the contracted edges list
        if (!safe) {
            //System.out.println("It is not safe!");
            ATNode lca = this.LCA(nodeDST, nodeSRC);
            lca.addToContractedEdges(newEdge);
            solution = this.aTree.getSolution();
        } else {
            //System.out.println("It is safe!");
            // perform virtual delete or remove edge
            WeightedEdge candidateEdge = candidate.getWeightedEdge();
            solution = this.removeEdgeUtil(true, candidateEdge, newEdge);
        }

        return solution;
    }


    /**
     * Verify if the candidate node is safe to be added to our ATree
     * Worst-case(n)
     *
     * @param candidate - Candidate ATNode
     * @param nodeSRC - Minimum Weight ATNode incident in the source of the edge to be added
     * @return true if nodeSRC is not hanged over candidate, false if it is
     */
    private boolean isSafeToAdd(ATNode candidate, ATNode nodeSRC) {

        boolean isSafe = true;
        // instantiate queue object
        ArrayDeque<ATNode> Q = new ArrayDeque<>();
        // add BFS root element
        Q.add(candidate);
        // perform a BFS checking if nodeSRC is hanged over the candidate node
        while (!Q.isEmpty()) {
            ATNode node = Q.poll();
            // if nodeSRC is hanged over the candidate node
            if (nodeSRC.getWeightedEdge().equals(node.getWeightedEdge())) {
                // set isSafe to false
                isSafe = false;
                // break the BFS search
                break;
            }

            List<ATNode> children = node.getChildren();
            Q.addAll(children);
        }

        return isSafe;
    }


    /**
     * Find the Lowest Common Ancestor
     * Worst-case O(n)
     *
     * @param node1 - ATNode node
     * @param node2 - ATNode node
     * @return LCA of node1 and node2
     */
    private ATNode LCA(ATNode node1, ATNode node2) {
        // create an empty HashSet to verify in constant time if node was visited
        Set<ATNode> visited = new HashSet<>();
        ATNode LCA = null;

        // perform a walk to the root storing the visited nodes in a HashSet
        while (node1 != null) {
            visited.add(node1);
            node1 = node1.getParent();
        }

        // when performing the second walk to the root query the HashSet always verifying
        while (node2 != null) {
            // if the node is in the visited set then the LCA was found
            if (visited.contains(node2)) {
                LCA = node2;
                break;
            }

            node2 = node2.getParent();
        }

        // finally return the LCA
        return LCA;
    }

    /**
     * Find a candidate node
     * Worst-Case O(n)
     *
     * @param parent - first ancestor of the node with dst
     * @param newEdge - new edge
     * @return Candidate Node if it exists, otherwise null
     */
    private ATNode findCandidate(ATNode parent, WeightedEdge newEdge) {

        WeightedEdge parentEdge = parent.getWeightedEdge();
        ATNode candidate = null;

        if (cmp.compare(parentEdge, newEdge) > 0){
            return parent;
        }

        Set<ATNode> ancestors = new HashSet<>();
        ATNode cNode = parent;

        while (cNode != null){
            ancestors.add(cNode);
            cNode = cNode.getParent();
        }

        ArboDisjointSet stronglyConnected = new FasterArboDisjointSet(this.G.size());
        List<ATNode> roots = this.aTree.roots();
        assert roots.size() == 1;

        ATNode root = roots.get(0);

        Stack<ATNode> nodeStack = this.aTree.reverseOrderUtil(root);

        boolean compare = false;

        while (!nodeStack.isEmpty()) {

            ATNode node = nodeStack.pop();

            if (node.getWeightedEdge() == null && node.getParent() == null)
                return null;

            if (ancestors.contains(node)) {
                compare = true;
            }

            ATNode res = this.findCandidateUtil(node, stronglyConnected, newEdge, compare);

            compare = false;

            if (res != null) {
                candidate = res;
                break;
            }
        }

        return candidate;
    }

    /**
     * Util find candidate node function
     *
     * @param node - ATNode object
     * @param stronglyConnected - ArboDisjointSet object
     * @param newEdge - WeightedEdge object
     * @param compare - boolean true compare nodes, false does not compare
     * @return Candidate ATNode object
     */
    private ATNode findCandidateUtil(ATNode node, ArboDisjointSet stronglyConnected , WeightedEdge newEdge, boolean compare){

        WeightedEdge nodeEdge = node.getWeightedEdge();

        List<ATNode> children =  node.getChildren();
        int maxW = node.getMaxCycleW();

        for (ATNode child: children) {
            WeightedEdge e = child.getWeightedEdge();
            int dst = stronglyConnected.findSet(e.getDest());
            int incidentW = e.getOriginalWeight() + stronglyConnected.findWeight(e.getDest());
            int reducedCost = maxW - incidentW;
            stronglyConnected.addWeight(dst, reducedCost);
        }

        WeightedEdge _newEdge = new WeightedEdge(newEdge.getSrc(), newEdge.getDest(),
                newEdge.getOriginalWeight() + stronglyConnected.findWeight(newEdge.getDest()));

        WeightedEdge _nodeWeightedEdge = new WeightedEdge(nodeEdge.getSrc(), nodeEdge.getDest(),
                nodeEdge.getOriginalWeight() + stronglyConnected.findWeight(nodeEdge.getDest()));

        if (compare && cmp.compare(_nodeWeightedEdge, _newEdge) > 0) {
            return node;
        }

        for (ATNode child: children) {
            WeightedEdge e = child.getWeightedEdge();
            int src = e.getSrc();
            int dst = e.getDest();
            stronglyConnected.unionSet(src, dst);
        }

        return null;
    }

    /**
     * Remove an Edge
     * Worst-case(O(|V| + ||p|| log |p|)
     *
     * @param removeEdge - edge to be removed
     * @return Minimum spanning arborescence
     */
    @Override
    public List<WeightedEdge> removeEdge(WeightedEdge removeEdge) {

        return this.removeEdgeUtil(false, removeEdge, null);
    }

    /**
     * Remove an edge from the graph and compute new minimum spanning arborescence
     * Utility function
     * Worst-case(O(|V| + ||p|| log |p|)
     *
     * @param virtual - true do not delete edge from graph and contracted, false delete edge from graph
     * @param removeEdge - edge to be removed
     * @param newEdge - new edge to be added
     * @return New Minimum Spanning Arborescence
     */
    private List<WeightedEdge> removeEdgeUtil(boolean virtual, WeightedEdge removeEdge, WeightedEdge newEdge) {

        // check if the edge to be removed belongs to the final solution in average constant time
        boolean inForest = this.aTree.contains(removeEdge);

        // if the edge to be removed is not in our solution implying that the arborescence remains unaffected
        if (!inForest) {
            //System.out.println("** Remove: " + removeEdge + " not in forest");
            // remove edge from the contracted-edge list
            this.aTree.removeEdgeFromATreeContractedList(removeEdge);
            // return MSA
            return this.aTree.getSolution();
        }

        // whenever virtual parameter is true do not perform deletion
        if (!virtual) {
            // remove edge from the digraph O(E)
            this.G.removeWeightedEdge(removeEdge);
            // remove edge from the contracted-edge list
            this.aTree.removeEdgeFromATreeContractedList(removeEdge);
        }

        // remove the ATNode related with removeEdge
        List<ATNode> removedContracted = this.aTree.decomposeTree(removeEdge);
        // remove EdgeNode related with removeEdge
        this.solutionForest.decomposeTree(removeEdge);

        List<ATNode> atNodeRoots = this.aTree.roots();
        // instantiate Edmonds Algorithm
        this.blackBoxEdmonds = new BlackBox(this.G, this.cmp, this.aTree, atNodeRoots, removedContracted, newEdge, removeEdge);

        // return updated minimum spanning arborescence
        List<WeightedEdge> msa = this.blackBoxEdmonds.getArborescence();
        this.solutionForest.resetSolutionForest();

        return msa;
    }

    /**
     * Dummy update cost function
     *
     * @param edge - original edge
     * @param newCost - new cost regarding the edge parameter
     * @return Minimum spanning arborescence
     */
    @Override
    public List<WeightedEdge> updateCost(WeightedEdge edge, int newCost) {
        this.removeEdge(edge);
        WeightedEdge newEdge = new WeightedEdge(edge.getSrc(), edge.getDest(), newCost);
        return this.addNewEdge(newEdge);
    }

    /**
     * ATree getter
     * @return ATree*/

    public ATree getAtree() {
        return this.aTree;
    }

    public int getE() {return this.blackBoxEdmonds.getE();}
    public int getV() {return this.blackBoxEdmonds.getV();}

}
