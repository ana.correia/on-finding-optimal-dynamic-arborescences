package net.phyloviz.dynamic.queue;


import java.util.*;

public class GenericBinomialHeap<T> implements Heap<T> {


    public class Node {

        private T key;
        private int degree;
        private Node parent, child, sibling;
        private Boolean remove;

        /**
         * Class attributes
         *
         * @param key - key
         */
        @SuppressWarnings("Duplicates")
        private Node(T key) {

            this.key = key;
            this.degree = 0;
            this.parent = null;
            this.child = null;
            this.sibling = null;
            this.remove = false;
        }

        @SuppressWarnings("Duplicates")
        private Node(Node node) {
            this.key = node.key;
            this.degree = node.degree;
            this.parent = node.parent;
            this.child = node.child;
            this.sibling = node.sibling;
            this.remove = node.remove;
        }

        @Override
        @SuppressWarnings("unchecked")
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node node = (Node) o;
            return degree == node.degree &&
                    Objects.equals(key, node.key) &&
                    Objects.equals(parent, node.parent) &&
                    Objects.equals(child, node.child) &&
                    Objects.equals(sibling, node.sibling) &&
                    Objects.equals(remove, node.remove);
        }

        @Override
        public int hashCode() {
            return Objects.hash(key, degree, parent, child, sibling, remove);
        }
    }


    private Node head;
    private Comparator<T> cmp;
    private HashMap<T, Node> map;

    /**
     * Class constructor
     * Worst case O(1)
     *
     * @param cmp Comparator object
     * @throws IllegalArgumentException if cmp is null
     */
    public GenericBinomialHeap(Comparator<T> cmp) {

        if (cmp == null) {
            throw new IllegalArgumentException("The comparators must not be null");
        }

        this.cmp = cmp;
        this.head = null;
        this.map = new HashMap<>();
    }


    /**
     * Class constructor
     * Worst case O(1)
     *
     * @param cmp  Comparator object
     * @param head Head BinomialNode
     * @throws IllegalArgumentException if cmp is null
     */
    public GenericBinomialHeap(Node head, Comparator<T> cmp) {

        if (cmp == null) {
            throw new IllegalArgumentException("The comparator must not be null");
        }

        this.cmp = cmp;
        this.head = head;
        this.map = new HashMap<>();
    }

    public Comparator<T> comparator() {
        return this.cmp;
    }

    @Override
    public void push(T item) {
        Node node = new Node(item);
        GenericBinomialHeap<T> tempHeap = new GenericBinomialHeap<>(node, this.cmp);
        this.map.put(item, node);
        this.head = this.unite(tempHeap);
    }

    /**
     * This method really unites two binomial heaps
     * Worst case is O(log(n))
     *
     * @param heap -  Binomial Heap
     * @return new head
     */
    @SuppressWarnings("Duplicates")
    private Node unite(GenericBinomialHeap<T> heap) {

        Node newHead = this.merge(this, heap);

        this.head = null;
        heap.head = null;

        if (newHead == null) {
            return null;
        }

        Node prev = null;
        Node curr = newHead;
        Node next = newHead.sibling;

        while (next != null) {
            if ( (curr.degree != next.degree) || (next.sibling != null &&
                    next.sibling.degree == curr.degree)) {
                prev = curr;
                curr = next;
            } else {

                if (this.cmp.compare(curr.key, next.key) < 0) {
                    curr.sibling = next.sibling;
                    this.link(next, curr);

                } else {
                    if (prev == null) {
                        newHead = next;
                    } else {
                        prev.sibling = next;
                    }

                    this.link(curr, next);
                    curr = next;
                }
            }

            next = curr.sibling;
        }

        for (Map.Entry<T, Node> entry : heap.map.entrySet()) {
            T key = entry.getKey();
            Node node = entry.getValue();
            this.map.put(key, new Node(node));
        }

        heap.map.clear();

        return newHead;
    }

    /**
     * Links two nodes on the heap
     * Worst case is O(1)
     *
     * @param y node y
     * @param z node z
     */
    private void link(Node y, Node z) {
        y.parent = z;
        y.sibling = z.child;
        z.child = y;
        z.degree++;
    }


    /**
     * This method unites the root list of heap1 and heap2 that is sorted by degree monotonically increasing order
     *
     * @param heap1 Binomial Heap
     * @param heap2 other binomial heap
     * @return new head node
     */
    @SuppressWarnings("Duplicates")
    private Node merge(GenericBinomialHeap<T> heap1, GenericBinomialHeap<T> heap2) {

        if (heap1.head == null) {
            return heap2.head;
        } else if (heap2.head == null) {
            return heap1.head;
        } else {
            Node head;
            Node heap1Next = heap1.head;
            Node heap2Next = heap2.head;

            if (heap1.head.degree <= heap2.head.degree) {
                head = heap1.head;
                heap1Next = heap1Next.sibling;
            } else {
                head = heap2.head;
                heap2Next = heap2Next.sibling;
            }

            Node tail = head;

            while (heap1Next != null && heap2Next != null) {
                if (heap1Next.degree <= heap2Next.degree) {
                    tail.sibling = heap1Next;
                    heap1Next = heap1Next.sibling;
                } else {
                    tail.sibling = heap2Next;
                    heap2Next = heap2Next.sibling;
                }

                tail = tail.sibling;
            }

            if (heap1Next != null) {
                tail.sibling = heap1Next;
            } else {
                tail.sibling = heap2Next;
            }

            return head;
        }
    }

    @Override
    public void update(T idx, T newIdx) {

        Node node = this.keyToNode(idx);

        if (node == null)
            throw new IllegalStateException("Element does not exist");

        this.heapifyUp(node, false);
    }

    private Node keyToNode(T key) {


        if (this.head == null) {
            System.out.println("HEAD: is null");
            return null;
        }

        return this.map.get(key);
    }

    /**
     * This method maintains the min heap property
     * Worst case is O(log(n))
     *
     * @param node - node from where we start bubbling up and swapping
     * @param n    - true if we want to assume decrease key to -infinity, false otherwise
     */
    @SuppressWarnings("Duplicates")
    private Node heapifyUp(Node node, boolean n) {

        Node y = node;
        Node z = node.parent;

        while (z != null && (n || this.cmp.compare(y.key, z.key) < 0)) {

            this.swap(y, z);

            y = z;
            z = z.parent;
        }

        return y;
    }

    /**
     * Auxiliary function that swaps the attributes of two nodes
     *
     * @param y - node y
     * @param z - node z
     */
    @SuppressWarnings("Duplicates")
    private void swap(Node y, Node z) {

        T tmp = y.key;
        boolean b = y.remove;

        //update the mapping
        this.map.replace(y.key, z);
        this.map.replace(z.key, y);

        y.key = z.key;
        y.remove = z.remove;

        z.key = tmp;
        z.remove = b;
    }

    @Override
    @SuppressWarnings("Duplicates")
    public void delete(T idx) {

        if (this.isEmpty()) {
            throw new IllegalStateException("The heap is empty");
        }

        Node node = this.keyToNode(idx);

        System.out.println("DELETE: "  + node.key);

        if (node == null)
            throw new IllegalStateException("Element does not exist");

        node.remove = true;
        this.heapifyUp(node, true);
        this.map.remove(idx);

        this.pop();
    }

    @Override
    public void union(Heap<T> heap) {

        if (!(heap instanceof GenericBinomialHeap)) {
            throw new IllegalArgumentException("Heap must be of type Binomial Heap");
        }

        this.head = this.unite((GenericBinomialHeap<T>) heap);
    }

    /**
     * Returns the minimum value of the root list
     * Worst Case is O(log(n) + 1)
     *
     * @return minimum
     */
    @Override
    @SuppressWarnings("Duplicates")
    public T top() {

        if (this.isEmpty()) {
            throw new IllegalStateException("The heap is empty");
        }

        Node min = this.head;
        Node next = min.sibling;

        while (next != null) {

            if (this.cmp.compare(min.key, next.key) < 0) {
                min = next;
            }

            next = next.sibling;
        }

        return min.key;
    }


    /**
     * Removes the minimum node from the heap
     * Worst case is O(log n)
     *
     * @return Minimum key
     * @throws IllegalStateException if the heap does not have items
     */
    @Override
    @SuppressWarnings("Duplicates")
    public T pop() {

        if (this.head == null) {
            throw new IllegalStateException("The heap is empty");
        }

        Node min = this.head;
        Node next = min.sibling;
        Node nextPrev = min;
        Node minPrev = null;

        while (next != null) {

            /*
            if (next.remove) {
                min = next;
                minPrev = nextPrev;
                break;
            }
             */

            if (this.cmp.compare(next.key, min.key) < 0) {
                min = next;
                minPrev = nextPrev;
            }

            nextPrev = next;
            next = next.sibling;

        }

        this.removeNode(min, minPrev);
        this.map.remove(min.key);
        return min.key;
    }


    /**
     * Method removes root from the root list and reverses the points
     * Worst case is O(log(n))
     *
     * @param root - node to be removed
     * @param prev - previous node to be removed
     */
    @SuppressWarnings("Duplicates")
    private void removeNode(Node root, Node prev) {

        // remove root from the heap
        if (root == this.head) {
            this.head = root.sibling;
        } else {
            prev.sibling = root.sibling;
        }

        //reverse the order of root children
        Node newHead = null;
        Node child = root.child;

        while (child != null) {

            Node next = child.sibling;
            child.sibling = newHead;
            child.parent = null;
            newHead = child;
            child = next;
        }

        GenericBinomialHeap<T> newHeap = new GenericBinomialHeap<T>(newHead, this.cmp);
        this.head = this.unite(newHeap);
    }

    /**
     * Calculates the number of nodes on the heap
     * Just go through the root list and use the property 2^order to calculate the number
     * of nodes on the heap
     * Worst case is O(log(n) + 1)
     *
     * @return number of elements on the heap
     */
    @Override
    @SuppressWarnings("Duplicates")
    public int size() {

        int result = 0;

        Node node = this.head;

        while (node != null) {

            int tmp = 1 << node.degree; //bitwise operations are faster
            result |= tmp;

            node = node.sibling;
        }

        return result;
    }

    /**
     * Verifies if heap is empty
     * Worst case is O(1)
     *
     * @return true if heap is empty, otherwise false
     */
    @Override
    public boolean isEmpty() {
        return this.head == null;
    }

    /**
     * Clears the heap
     * Worst case is O(1)
     */
    public void clear() {
        this.head = null;
    }

    @Override
    public boolean hasKey(T key) {
        return this.map.containsKey(key);
    }

    @Override
    public void setComparator(Comparator<T> cmp) {
        this.cmp = cmp;
    }

    @Override
    public Set<T> getKeySet() {

        return getElements();
        /*
        if (this.isEmpty()){

            return new HashSet<>();
        }

        return this.map.keySet();

         */
    }

    public boolean contains(T idx) {

        return this.map.containsKey(idx);
    }

    public Set<T> getElements(){

        if (this.head == null)
            return null;

        List<Node> nodes = new ArrayList<>();
        nodes.add(this.head);
        Set<T> set = new HashSet<>();
        set.add(this.head.key);

        while (!nodes.isEmpty()) {
            Node curr = nodes.get(0);
            set.add(curr.key);
            nodes.remove(0);

            if (curr.sibling != null) {
                nodes.add(curr.sibling);

            }
            if (curr.child != null) {
                nodes.add(curr.child);
            }
        }

        return set;
    }

    public String toString() {

        return this.map.keySet().toString();
    }
}
