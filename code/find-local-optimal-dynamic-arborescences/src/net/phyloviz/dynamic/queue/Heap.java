package net.phyloviz.dynamic.queue;

import java.util.Comparator;
import java.util.Set;

public interface Heap<T> {

    Comparator<T> comparator();

    void push(T idx);

    void update(T current, T newC);

    void delete(T idx);

    void union(Heap<T> heap);

    T top();

    T pop();

    int size();

    boolean isEmpty();

    boolean hasKey(T key);

    void setComparator(Comparator<T> cmp);

    Set<T> getKeySet();
}
