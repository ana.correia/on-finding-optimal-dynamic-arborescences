package net.phyloviz.dynamic.queue;

import java.util.*;

public class BinomialHeap<T> implements Heap<T> {

    private Node head;
    private Comparator<T> cmp;
    private Map<T, Node> tNodeMap;

    /**
     * Class constructor
     * Worst case O(1)
     *
     * @param cmp Comparator object
     * @throws IllegalArgumentException if cmp is null
     */
    public BinomialHeap(Comparator<T> cmp) {

        if (cmp == null) {
            throw new IllegalArgumentException("The comparators must not be null");
        }

        this.head = null;
        this.cmp = cmp;
        this.tNodeMap = new HashMap<>();
    }


    /**
     * Class constructor
     * Worst case O(1)
     *
     * @param head Head BinomialNode
     */
    public BinomialHeap(Node head) {

        this.head = head;
        this.tNodeMap = new HashMap<>();

        if (head != null) {
            this.tNodeMap.put(head.key, head);
        }
    }

    /**
     * Get the comparator
     * @return Comparator
     */
    @Override
    public Comparator<T> comparator() {
        return this.cmp;
    }

    @Override
    public void push(T idx) {

        Node node = new Node(idx);
        BinomialHeap<T> tempHeap = new BinomialHeap<T>(node);
        this.head = this.unionUtil(tempHeap);
    }

    /**
     * Update the key of a node from heap
     * Worst-case(O(log(n))
     * @param idx - key
     * @param newKey- new key
     */
    @Override
    public void update(T idx, T newKey) {

        Node node = this.keyToNode(idx);
        node.key = newKey;
        this.bubbleUp(node, false);
    }

    /**
     * Delete a node from heap
     * Worst-case(O(log(n))
     * @param idx - key
     */
    @Override
    public void delete(T idx) {

        Node node = this.keyToNode(idx);
        node = this.bubbleUp(node, true);

        if (this.head == node) {
            this.removeTreeRoot(node, null);
        } else {
            Node prev = this.head;
            while (prev.sibling.compareTo(node) != 0) {
                prev = prev.sibling;
            }

            this.removeTreeRoot(node, prev);
        }

        this.tNodeMap.remove(idx);
    }

    /**
     * This method really unites two binomial heaps
     * Worst case is O(log(n))
     *
     * @param heap -  Binomial Heap
     */
    @Override
    public void union(Heap<T> heap) {

        this.head = this.unionUtil((BinomialHeap<T>) heap);
    }

    /**
     * Returns the minimum value of the root list
     * Worst Case is O(log(n) + 1)
     *
     * @return minimum
     */
    @Override
    public T top() {

        if (this.head == null) {
            return null;
        } else {
            Node min = this.head;
            Node next = min.sibling;

            while (next != null) {

                if (next.compareTo(min) < 0) {
                    min = next;
                }

                next = next.sibling;
            }

            return min.key;
        }
    }

    /**
     * Removes the minimum node from the heap
     * Worst case is O(log n)
     *
     * @return Minimum key
     * @throws IllegalStateException if the heap does not have items
     */
    @Override
    public T pop() {

        if (this.head == null) {
            throw new IllegalStateException("The heap is empty");
        }

        Node min = this.head;
        Node minPrev = null;
        Node next = min.sibling;
        Node nextPrev = min;

        while (next != null) {

            if (next.compareTo(min) < 0) {
                min = next;
                minPrev = nextPrev;
            }

            nextPrev = next;
            next = next.sibling;
        }

        this.removeTreeRoot(min, minPrev);
        this.tNodeMap.remove(min.key);
        return min.key;
    }

    /**
     * Calculates the number of nodes on the heap
     * Just go through the root list and use the property 2^order to calculate the number
     * of nodes on the heap
     * Worst case is O(log(n) + 1)
     *
     * @return number of elements on the heap
     */
    @Override
    @SuppressWarnings("Duplicates")
    public int size() {

        int result = 0;

        Node node = this.head;

        while (node != null) {

            int tmp = 1 << node.degree; //bitwise operations are faster
            result |= tmp;

            node = node.sibling;
        }

        return result;
    }

    public boolean isEmpty() {
        return this.head == null;
    }

    @Override
    public boolean hasKey(T key) {

        return this.tNodeMap.containsKey(key);
    }

    public boolean hasKeyLinear(T key) {

        return this.search(key) != null;
    }

    @Override
    public void setComparator(Comparator<T> cmp) {
        this.cmp = cmp;
    }

    @Override
    public Set<T> getKeySet() {

        return tNodeMap.keySet();
        //return getAllElements();
    }

    public Set<T> getAllElements(){

        Set<T> keySet = new HashSet<>();

        if (this.head == null) {
            return keySet;
        }

        LinkedList<Node> nodes = new LinkedList<>();
        nodes.add(this.head);

        while (!nodes.isEmpty()) {
            Node curr = nodes.pop();
            keySet.add(curr.key);
            if (curr.sibling != null) {
                nodes.add(curr.sibling);
            }

            if (curr.child != null) {
                nodes.add(curr.child);
            }
        }

        return keySet;
    }

    /**
     * Clears the heap
     * Worst case is O(1)
     */
    public void clear() {

        this.head = null;
    }


    private Node keyToNode(T key){
        return this.tNodeMap.get(key);
        //return this.search(key);
    }

    /**
     * Find a node with a given key
     * @param key - T
     * @return null if node not exists, otherwise node with key
     */
    private Node search(T key) {

        if (this.head == null)
            return null;

        List<Node> nodes = new ArrayList<>();
        nodes.add(this.head);
        while (!nodes.isEmpty()) {
            Node curr = nodes.get(0);
            nodes.remove(0);
            if (curr.key.equals(key)) {
                return curr;
            }
            if (curr.sibling != null) {
                nodes.add(curr.sibling);
            }
            if (curr.child != null) {
                nodes.add(curr.child);
            }
        }

        return null;
    }

    /**
     * This method maintains the min heap property
     * Worst case is O(log(n))
     *
     * @param node - node from where we start bubbling up and swapping
     * @param toRoot    - true if we want to assume decrease key to -infinity, false otherwise
     */
    private Node bubbleUp(Node node, boolean toRoot) {

        Node parent = node.parent;

        while (parent != null && (toRoot || node.compareTo(parent) < 0)) {
            this.tNodeMap.replace(node.key, parent);
            this.tNodeMap.replace(parent.key, node);
            T temp = node.key;
            node.key = parent.key;
            parent.key = temp;
            node = parent;

            parent = parent.parent;
        }

        return node;
    }

    /**
     * Method removes root from the root list and reverses the points
     * Worst case is O(log(n))
     *
     * @param root - node to be removed
     * @param prev - previous node to be removed
     */
    @SuppressWarnings("Duplicates")
    private void removeTreeRoot(Node root, Node prev) {
        // Remove root from the heap
        if (root == this.head) {
            this.head = root.sibling;
        } else {
            prev.sibling = root.sibling;
        }

        // Reverse the order of root's children and make a new heap
        Node newHead = null;
        Node child = root.child;

        while (child != null) {
            Node next = child.sibling;
            child.sibling = newHead;
            child.parent = null;
            newHead = child;
            child = next;
        }

        BinomialHeap<T> newHeap = new BinomialHeap<T>(newHead);
        // Union the heaps and set its head as this.head
        this.head = this.unionUtil(newHeap);
    }


    /**
     * Merge two binomial trees of the same order
     * Worst-case O(1)
     * @param minNodeTree - Node
     * @param other - Node
     */
    private void linkTree(Node minNodeTree, Node other) {
        other.parent = minNodeTree;
        other.sibling = minNodeTree.child;
        minNodeTree.child = other;
        minNodeTree.degree++;
    }

    /**
     * This method really unites two binomial heaps
     * Worst case is O(log(n))
     *
     * @param heap -  Binomial Heap
     * @return new head
     */
    @SuppressWarnings("Duplicates")
    private Node unionUtil(BinomialHeap<T> heap) {

        Node newHead = this.merge(this, heap);

        this.head = null;
        heap.head = null;

        if (newHead == null) {
            return null;
        }

        Node prev = null;
        Node curr = newHead;
        Node next = newHead.sibling;

        while (next != null) {
            if (curr.degree != next.degree || (next.sibling != null && next.sibling.degree == curr.degree)) {
                prev = curr;
                curr = next;
            } else {
                if (curr.compareTo(next) < 0) {
                    curr.sibling = next.sibling;
                    this.linkTree(curr, next);
                } else {
                    if (prev == null) {
                        newHead = next;
                    } else {
                        prev.sibling = next;
                    }

                    this.linkTree(next, curr);
                    curr = next;
                }
            }

            next = curr.sibling;
        }

        this.tNodeMap.putAll(heap.tNodeMap);
        heap.tNodeMap.clear();

        return newHead;
    }

    /**
     * This method unites the root list of heap1 and heap2 that is sorted by degree monotonically increasing order
     *
     * @param heap1 Binomial Heap
     * @param heap2 other binomial heap
     * @return new head node
     */
    @SuppressWarnings("Duplicates")
    private Node merge(BinomialHeap<T> heap1, BinomialHeap<T> heap2) {

        if (heap1.head == null) {
            return heap2.head;
        } else if (heap2.head == null) {
            return heap1.head;
        } else {
            Node head;
            Node heap1Next = heap1.head;
            Node heap2Next = heap2.head;

            if (heap1.head.degree <= heap2.head.degree) {
                head = heap1.head;
                heap1Next = heap1Next.sibling;
            } else {
                head = heap2.head;
                heap2Next = heap2Next.sibling;
            }

            Node tail = head;

            while (heap1Next != null && heap2Next != null) {
                if (heap1Next.degree <= heap2Next.degree) {
                    tail.sibling = heap1Next;
                    heap1Next = heap1Next.sibling;
                } else {
                    tail.sibling = heap2Next;
                    heap2Next = heap2Next.sibling;
                }

                tail = tail.sibling;
            }

            if (heap1Next != null) {
                tail.sibling = heap1Next;
            } else {
                tail.sibling = heap2Next;
            }

            return head;
        }
    }

    public void print() {
        System.out.println("Binomial heap:");
        if (head != null) {
            head.print(0);
        }
    }

    private class Node {

        public T key;
        public int degree;
        public Node parent;
        public Node child;
        public Node sibling;

        public Node() {
            this.key = null;
        }

        public Node(T key) {
            this.key = key;
        }

        public int compareTo(Node other) {
            return cmp.compare(key, other.key);
        }

        public void print(int level) {
            Node curr = this;
            while (curr != null) {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < level; i++) {
                    sb.append(" ");
                }
                sb.append(curr.key.toString());
                System.out.println(sb.toString());
                if (curr.child != null) {
                    curr.child.print(level + 1);
                }
                curr = curr.sibling;
            }
        }
    }

}