package net.phyloviz.dynamic.queue;

import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


public class HeapTest  {


    private List<Heap<Integer>> heapList;
    private List<Heap<Integer>> heapList2;
    private List<List<Integer>> sequences;

    @Before
    public void setup() {

        Comparator<Integer> cmp = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        };

        int nHeaps = 5;
        Random random = new Random();

        this.heapList = new LinkedList<>();
        this.heapList2 = new LinkedList<>();

        this.sequences  = new LinkedList<>();

        for (int i = 0; i < nHeaps; i++) {

            int hSize = random.nextInt(16)+1;
            Heap<Integer> heap = new BinomialHeap<>(cmp);
            Heap<Integer> heap2 = new GenericBinaryHeap<>(cmp);

            List<Integer> content = new LinkedList<>();

            for (int k = 0 ; k < hSize; k++){
                int v = random.nextInt(55000);
                heap.push(v);
                heap2.push(v);
                content.add(v);
            }

            this.heapList.add(heap);
            this.heapList2.add(heap2);
            this.sequences.add(content);
        }
    }

    @Test
    public void testPop() {

        for (int i = 0; i < heapList.size(); i++) {

            Heap<Integer> h = this.heapList.get(i);
            Heap<Integer> h2 = this.heapList2.get(i);

            while (!h.isEmpty()) {
                Integer min = h.pop();
                Integer min2 = h2.pop();
                assertEquals(min2, min);

            }
        }
    }

    @Test
    public void testUnion(){
        Heap<Integer> heap1 = this.heapList.get(0);
        Heap<Integer> heap2 = this.heapList.get(1);
        heap1.union(heap2);
        int s = this.sequences.get(0).size() + this.sequences.get(1).size();
        assertEquals(heap1.size(), s);
        assertEquals(heap2.size(), 0);


        Heap<Integer> _heap1 = this.heapList2.get(0);
        Heap<Integer> _heap2 = this.heapList2.get(1);
        _heap1.union(_heap2);

        while (!heap1.isEmpty()) {
            Integer min = heap1.pop();
            Integer min2 = _heap1.pop();
            assertEquals(min2, min);
        }

    }

    @Test
    public void testDelete() {

        for (Heap<Integer> heap1 : this.heapList){

            Set <Integer> x = new HashSet<>(heap1.getKeySet());
            BinomialHeap<Integer> h = (BinomialHeap<Integer>) heap1;
            Set<Integer> y = new HashSet<>(h.getAllElements());

            assertEquals(x,y);

            for (Integer i : x) {

                if (i == null) {
                    continue;
                }


                heap1.delete(i);
                assertFalse(heap1.hasKey(i));
                assertFalse(h.hasKeyLinear(i));

            }
        }
    }

    @Test
    public void testUnionDelete(){

        Heap<Integer> h1 = this.heapList.get(0);
        Heap<Integer> h2 = this.heapList.get(1);

        h1.union(h2);
        Set <Integer> x = new HashSet<>(h1.getKeySet());
        BinomialHeap<Integer> _h1 = (BinomialHeap<Integer>) h1;
        Set<Integer> y = _h1.getAllElements();
        assertEquals(x,y);


        for (Integer i : x) {

            if (i == null) {
                continue;
            }


            h1.delete(i);
            assertFalse(h1.hasKey(i));
            assertFalse(_h1.hasKeyLinear(i));

        }



    }



}
